 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		
			<!-- end hero-header -->

			<div class="post-hero bg-light">

				<div class="container">

					<div class="featured-sm-wrapper mt-15 mb-15">

						<div class="row">
							<div class="order-process">
								<div class="col-md-4 col-sm-4">
									<div class="service-item-box style-4 clearfix">
										<a href="#">
											<i class="fa fa-search"></i>
										</a>
										<div class="service-text">
											<h3>Search</h3>
										</div>
									</div>
								</div>
								<!-- end service item -->

								<div class="col-md-4 col-sm-4">
									<div class="service-item-box style-4 clearfix">
										<a href="#">
											<i class="fa fa-car" aria-hidden="true"></i>
										</a>
										<div class="service-text">
											<h3>Reserve</h3>
										</div>
									</div>
								</div>
								<!-- end service item -->

								<div class="col-md-4 col-sm-4">
									<div class="service-item-box style-4 clearfix">
										<a href="#">
											<i class="fa fa-cutlery" aria-hidden="true"></i>
										</a>
										<div class="service-text">
											<h3>Waiting</h3>
										</div>
									</div>
								</div>
								<!-- end service item -->


							</div>

						</div>

					</div>

				</div>

			</div>
			




 <div class="pt-80 pb-80">

 	<div class="container">

 		<div class="row">

 			<div class="col-md-8">

 				<div class="section-title">

 					

 				</div>

 				<div class="restaurant-common-wrapper">

<a href="#" class="restaurant-common-wrapper-item clearfix">
								<div class="GridLex-grid-middle">
									<div class="GridLex-col-6_xs-12">
										<div class="restaurant-type">
											<div class="image">
												<img src="images/brands/02.jpg" alt="image" />
											</div>
											<div class="content">
												<h4>Food Republic</h4>
												<p>Indian</p>
											</div>
										</div>
									</div>
									<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
										<div class="job-location">
											<i class="fa fa-map-marker text-primary"></i> <span> Menlo park, CA </span>
										</div>
									</div>
									<div class="GridLex-col-2_xs-4_xss-12">
										<div class="res-btn label label-danger">
											예약하기
										</div>
									
									</div>
								</div>
							</a>
 				</div>
 			</div>
	
	
					<!-- 인기메뉴 시작 -->
					<div class="col-md-4 mt-50-sm">

						<div class="section-title">

							<h3 class="text-center-sm">예약 많은 가게</h3>

						</div>

						<div class="row gap-20 top-company-wrapper mmt">

							<div class="col-xs-6 col-sm-4 col-md-6">

								<div class="top-company">
									<div class="image">
										<img src="images/brands/01.png" alt="image" />
									</div>
									<h5>Vantage</h5>
									<a href="#">예약하기</a>
								</div>

							</div>


						</div>
						
						
						<!-- 인기메뉴 끝 -->
						

					</div>
 		</div>
 	</div>
 </div>

			<!-- start banner section -->
			<div class="bt-block-home-parallax pt-80 pb-80" style="background-image: url(images/store/bpicture.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="lookbook-product">
								<h2>당신을 위한 동성로 맛집 예약</h2>

							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end banner section -->



 <div class="container pt-80 pb-80">
 	<div class="row">
 		<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
 			<div class="section-title">
 				<h3>신규 맛집</h3>
 			</div>
 		</div>
 	</div>

	<div class="row gap-40">
 		<div class="col-xs-4 col-sm-2 mb-20">
 			<a href="#"><img src="project/images/brands/10.png" alt="image" /></a>
 		</div>
 	</div>
 </div>



 <script>
 	fetch('randomListStore.do')
 		.then(resolve => resolve.json())
 		.then(result => {
 			//console.log(result)

 			for (let i = 0; i < 8; i++) {
 				let template = document.querySelector('.restaurant-common-wrapper').cloneNode(true);
 				//console.log(template.querySelector('h4'))
 				template.querySelector('h4').innerText = result[i].storeName;
 				//console.log(template.querySelector('p'))
 				template.querySelector('p').innerText = result[i].category;
 				template.querySelector('img').setAttribute('src', 'images/store/' + result[i].storeImg);
 				template.querySelector('span').innerText = result[i].storeLocation;
 				
				let atag = template.querySelector('a');
				atag.setAttribute('href', 'reservOnline.do?id=' + result[i].ownerId);
 				
 				document.querySelector('.restaurant-common-wrapper').parentElement.append(template);
 			}
 			document.querySelector('.restaurant-common-wrapper').style.display = 'none';
 		})
 		.catch(reject => console.log(reject))



 	fetch('newStore.do')
 		.then(resolve => resolve.json())
 		.then(result => {

 			for (let i = 0; i < 6; i++) {
 				let template = document.querySelector('.col-xs-4.col-sm-2.mb-20').cloneNode(true);

 				template.querySelector('img').setAttribute('src', 'images/store/' + result[i].storeImg);
				let atag = template.querySelector('a');
				atag.setAttribute('href', 'reservOnline.do?id=' + result[i].ownerId);
 				document.querySelector('.col-xs-4.col-sm-2.mb-20').parentElement.append(template);
 			}
 			document.querySelector('.col-xs-4.col-sm-2.mb-20').style.display = 'none';
 		})
 		.catch(reject => console.log(reject))
 	
 	
 	
 	fetch('topStoreList.do')
	.then(resolve => resolve.json())
	.then(result => {

		for (let i = 0; i < 8; i++) {
			let template = document.querySelector('.col-xs-6.col-sm-4.col-md-6').cloneNode(true);
			console.log(template.querySelector('h5'))
			template.querySelector('h5').innerText = result[i].storeName;
			template.querySelector('img').setAttribute('src', 'images/store/' + result[i].storeImg);
			let atag = template.querySelector('a');
			atag.setAttribute('href', 'reservOnline.do?id=' + result[i].ownerId);
			
			document.querySelector('.col-xs-6.col-sm-4.col-md-6').parentElement.append(template);
		}
		document.querySelector('.col-xs-6.col-sm-4.col-md-6').style.display = 'none';
		
	})
	.catch(reject => console.log(reject))
 	
 	
 </script> 