<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8' />
	<script src="http://code.jquery.com/jquery-latest.js"></script> 
	<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet'/>
	<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
	<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/locales-all.min.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bPopup/0.11.0/jquery.bpopup.js"></script>
    <script>
		document.addEventListener('DOMContentLoaded', function() {
			var calendarEl = document.getElementById('calendar');
			var calendar = new FullCalendar.Calendar(calendarEl, {
				  locale: "ko",
				  initialView: 'dayGridMonth',
				  headerToolbar: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
				  },
				  googleCalendarApiKey: 'AIzaSyCsMYKLjQy-jqeny2zR6UXdD05tUsrGSlY',
				  
				  events: {
				  googleCalendarId: '3fc6f068d02b7cdf095ba38ea74a1924d31a0da7ac932f6182074ccc45aa458d@group.calendar.google.com',
				  className: 'gcal-event' // 팝업창 뜨는 이벤트 
				  },
				  
				  eventClick: function(info) {
					  let start_year = info.event.start.getUTCFullYear();
					  let start_month = info.event.start.getMonth() + 1;
					  let start_date = info.event.start.getUTCDate();
					  let start_hour = info.event.start.getHours();
					  let start_minute = info.event.start.getMinutes();
					  let start_second = info.event.start.getSeconds();
					  let end_hour = info.event.end.getHours();

					  let start = start_year + "-" + start_month + "-" + start_date + " " + start_hour + "시 ~ " + end_hour + "시";

					  let attends = "";
					  info.event.extendedProps.attachments.forEach(function(item) {
						  attends += "<div><a href='"+item.fileUrl+"' target='_blank'>[첨부파일]</a></div>"
					  });

					  if(!info.event.extendedProps.description) {
						  info.event.extendedProps.description = "";
					  }
					  let contents = `
						<div style='font-weight:bold; font-size:20px; margin-bottom:30px; text-align:center'>
							${start}
						</div>
						<div style='font-size:18px; margin-bottom:20px'>
							제목: ${info.event.title}
						</div>
						<div style='width:500px'>
							${info.event.extendedProps.description}
							${attends}
						</div>
					  `;
					  
					 // $("#popup").html(contents);
					 // $("#popup").bPopup({
						//speed: 650,
						//transition: 'slideIn',
					 //	transitionClose: 'slideBack',
					 //	position: [($(document).width()-500)/2, 30] //x, y
					 // });
					 // info.jsEvent.stopPropagation();
					 // info.jsEvent.preventDefault();
					  
					  
					 
				  }
			});
			calendar.render();
		});
		
		
		
		
    </script>
  </head>
  <body>
  
  	<div id='calendar' style="width:800px; height:800px; margin-left : auto ; margin-right : auto; margin-top: 50px; margin-bottom: 50px"></div>
	
  </body>
</html>