<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="container-wrapper">

		

		<!-- start Main Wrapper -->
		<div class="main-wrapper">

			<!-- start hero-header -->
			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list">
						<li><a href="main.do">Home</a></li>
						<li><span>Forgot Password</span></li>
					</ol>
					
				</div>
				
			</div>
			<!-- end hero-header -->

			<div class="login-container-wrapper">	
	
				<div class="container">

					<div class="row">
					
						<div class="col-md-10 col-md-offset-1">
						
							<div class="row">

								<div class="col-sm-6 col-sm-offset-3">
								
									<div class="login-box-wrapper">
							
										<div class="modal-header">
											<h4 class="modal-title text-center">비밀번호 찾기</h4>
										</div>

										<div class="modal-body">
											<div class="row gap-20">
												
												
												<form action="findPw.do" name="myFrm">
												<div class="col-sm-12 col-md-12">
	
													<div class="form-group" id="frm"> 
														<label>아이디를 입력해주세요</label>
														<input class="form-control" name ="id" id="id" placeholder="아이디를 입력해주세요" type="text"> 
													
													
												<div class="modal-footer text-center">
											<button type="submit" class="btn btn-primary">찾기</button>
										</div>
										</div>
										</form>
										</div>
												</div>
													
												
												<div class="col-sm-12 col-md-12">
													<div class="login-box-box-action">
														Return to <a href="login.do">Log-in</a>
													</div>
												</div>
												
											</div>
										</div>
										
										
									</div>

								</div>
							
							</div>
							
						</div>
						
					</div>

				</div>
			
			</div>
	</div>
	</div>
</body>