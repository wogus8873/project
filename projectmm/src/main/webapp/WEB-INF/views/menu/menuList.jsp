<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>메뉴등록</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/store/${storeImg}" alt="image" class="img-circle" />
							</div>

							<h4>${storeName}</h4>
							<p class="user-role">${ownerName}(${ownerId})</p>

						</div>
						
						<div class="admin-user-action text-center">
							<a href="updateStoreForm.do" class="btn btn-primary btn-sm btn-inverse">프로필 수정</a>
						</div>


						<ul class="admin-user-menu clearfix">
							<li><a href="storeProfile.do"><i class="fa fa-user"></i> 프로필</a></li>
							<li class="active"><a href="ownerMyPage.do"><i class="fa fa-key"></i>메뉴등록</a></li>
							<li><a href="char.do"><i class="fa fa-sign-out"></i> 차트</a></li>
							<li><a href="listReserv.do"><i class="fa fa-sign-out"></i> 예약관리</a></li>
							<li><a href="logout.do"><i class="fa fa-sign-out"></i> Logout</a></li>
						</ul>

					</div>

				</div>

				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">

						<div class="admin-section-title">

							<h2>메뉴등록</h2>
							<p>원하시는 메뉴를 입력하세요</p>

						</div>

						<form id = "menuInfo" class="post-form-wrapper">

							<div class="row gap-20">

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<input type="hidden" class="form-control"
											id="ownerId" name="ownerId" value="${ownerId}">
									</div>

								</div>

								<div class="clear"></div>

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>메뉴이름</label> <input type="text" class="form-control"
											id="menuName" name="menuName">
									</div>

								</div>

								<div class="clear"></div>

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>메뉴가격</label> <input type="number" class="form-control"
											id="menuPrice" name="menuPrice">
									</div>

								</div>

								<div class="clear"></div>

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>메뉴이미지</label> <input type="file" class="form-control"
											id="menuImg" name="menuImg">
									</div>

								</div>



								<div class="col-sm-12 mt-10">
									<button class="btn btn-primary" id="addBtn">등록</button>
									<button type="reset" class="btn btn-primary btn-inverse">취소</button>
								</div>

							</div>

						</form>
						<div id="show">
							<table id="tb" class="table">
								<thead>
									<tr>
										<th>메뉴아이디</th>
										<th>점주아이디</th>
										<th>메뉴이름</th>
										<th>가격</th>
										<th>판매량</th>
										<th>삭제</th>
										<th>수정</th>
									</tr>
								</thead>

								<tbody id="body">

								</tbody>
							</table>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>



<script src="js/menuFunctions.js"></script>
<script src="js/menuFetch.js"></script>