<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>메뉴등록 및 조회</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/store/${storeImg}" alt="image"
									class="img-circle" />
							</div>

							<h4>${storeName}</h4>
							<p class="user-role">${ownerName}(${ownerId})</p>

						</div>

						<div class="admin-user-action text-center">
							<a href="updateStoreForm.do"
								class="btn btn-primary btn-sm btn-inverse">프로필 수정</a>
						</div>

						<ul class="admin-user-menu clearfix">
							<li><a href="storeProfile.do"><i class="fa fa-user"></i>프로필</a></li>
							<li><a href="ownerMyPage.do"><i class="fa fa-key"></i>메뉴등록 및 리스트</a></li>
							<li class="active"><a href="char.do"><i class="fa fa-sign-out"></i> 판매량</a></li>
							<li><a href="listReserv.do"><i class="fa fa-sign-out"></i> 예약관리</a></li>
							<li><a href="logout.do"><i class="fa fa-sign-out"></i>Logout</a></li>
						</ul>

					</div>

				</div>

				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">

						<div class="admin-section-title">

							<h2>판매량</h2>
							<p>메뉴별 판매량입니다.</p>

						</div>

						<head>
							<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    						<script type="text/javascript">
    						 google.charts.load("current", {packages:["corechart"]});
    					     google.charts.setOnLoadCallback(drawChart);

    					     function drawChart() {
    					     var data = [['menuName', 'saleCount']]
    						 var options = {
    					          title: '메뉴별 판매량입니다.',
    					          pieHole: 0.4,
    					      };
    						  fetch('charData.do')
    					      .then(resolve=>resolve.json())
    					      .then(result=>{
    						  console.log(result)
    								for(let prop of result){
    									if(prop.ownerId == '${ownerId}'){
    						                let menuName = prop.menuName;//key 
    					    	            let saleCount = prop.saleCount
    					        	        data.push([menuName,saleCount])
    									}
    					            }
    					  			data = google.visualization.arrayToDataTable(data);
    						
    						  var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    					      chart.draw(data, options);
    					      })

    					      }
    						</script>
						</head>
						<body>
					    	<div id="donutchart" style="width: 900px; height: 500px;"></div>
					  	</body>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

