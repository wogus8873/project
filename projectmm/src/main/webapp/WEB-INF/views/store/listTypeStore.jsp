<%@page import="store.vo.StoreVO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!-- start hero-header -->
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="homepage.html">홈</a></li>
			<li><span>카테고리</span></li>
		</ol>

	</div>

</div>
<!-- end hero-header -->

<div class="section sm">

				<div class="container">
				
					<div class="search-restaurant-wrapper mt-0">
					
						<div class="search-restaurant-box">
						
							<div class="row">
							
								<div class="col-sm-4 col-md-3">
								
									<div class="search-restaurant-box-title">

										<h3 class="text-primary text-lowercase mb-0"><a href="#">한식</a></h3>
										<span>Korean</span> 
										
									</div>
									
								</div>
								
								<div class="col-sm-8 col-md-9">
								
									<div class="search-restaurant-box-content">
									
										<div class="search-restaurant-box-inner">
										
											<div class="row gap-20">
											
												<div class="col-sm-6 col-md-4">
												<c:forEach var="vo" items="${storeList }">
													<c:if test="${vo.category eq '한식' }">
													<a href="reservOnline.do?id=${vo.ownerId }">${vo.storeName }</a>
													</c:if>
												</c:forEach>
												</div>
																						
											</div>
										
										</div>
										
									</div>

								</div>
								
							</div>
						
						</div>
						<div class="search-restaurant-box">
						
							<div class="row">
							
								<div class="col-sm-4 col-md-3">
								
									<div class="search-restaurant-box-title">

										<h3 class="text-primary text-lowercase mb-0"><a href="#">중식</a></h3>
										<span>Chinese</span>
										
									</div>
									
								</div>
								
								<div class="col-sm-8 col-md-9">
								
									<div class="search-restaurant-box-content">
									
										<div class="search-restaurant-box-inner">
										
											<div class="row gap-20">
											
												<div class="col-sm-6 col-md-4">
												<c:forEach var="vo" items="${storeList }">
													<c:if test="${vo.category eq '중식' }">
													<a href="reservOnline.do?id=${vo.ownerId }">${vo.storeName }</a>
													</c:if>
												</c:forEach>
												</div>
																						
											</div>
										
										</div>
										
									</div>

								</div>
								
							</div>
						
						</div>
						
						
						<div class="search-restaurant-box">
						
							<div class="row">
							
								<div class="col-sm-4 col-md-3">
								
									<div class="search-restaurant-box-title">

										<h3 class="text-primary text-lowercase mb-0"><a href="#">일식</a></h3>
										<span>Japanese</span>
										
									</div>
									
								</div>
								
								<div class="col-sm-8 col-md-9">
								
									<div class="search-restaurant-box-content">
									
										<div class="search-restaurant-box-inner">
										
											<div class="row gap-20">
											
												<div class="col-sm-6 col-md-4">
												<c:forEach var="vo" items="${storeList }">
													<c:if test="${vo.category eq '일식' }">
													<a href="reservOnline.do?id=${vo.ownerId }">${vo.storeName }</a>
													</c:if>
												</c:forEach>
												</div>
																						
											</div>
										
										</div>
										
									</div>

								</div>
								
							</div>
						
						</div>
						
						<div class="search-restaurant-box">
						
							<div class="row">
							
								<div class="col-sm-4 col-md-3">
								
									<div class="search-restaurant-box-title">

										<h3 class="text-primary text-lowercase mb-0"><a href="#">양식</a></h3>
										<span>Western</span>
										
									</div>
									
								</div>
								
								<div class="col-sm-8 col-md-9">
								
									<div class="search-restaurant-box-content">
									
										<div class="search-restaurant-box-inner">
										
											<div class="row gap-20">
											
												<div class="col-sm-6 col-md-4">
												<c:forEach var="vo" items="${storeList }">
													<c:if test="${vo.category eq '양식' }">
													<a href="reservOnline.do?id=${vo.ownerId }">${vo.storeName }</a>
													</c:if>
												</c:forEach>
												</div>
																						
											</div>
										
										</div>
										
									</div>

								</div>
								
							</div>
						
						</div>
						
						<div class="search-restaurant-box">
						
							<div class="row">
							
								<div class="col-sm-4 col-md-3">
								
									<div class="search-restaurant-box-title">

										<h3 class="text-primary text-lowercase mb-0"><a href="#">디저트</a></h3>
										<span>Dessert</span>
										
									</div>
									
								</div>
								
								<div class="col-sm-8 col-md-9">
								
									<div class="search-restaurant-box-content">
									
										<div class="search-restaurant-box-inner">
										
											<div class="row gap-20">
											
												<div class="col-sm-6 col-md-4">
												<c:forEach var="vo" items="${storeList }">
													<c:if test="${vo.category eq '디저트' }">
													<a href="reservOnline.do?id=${vo.ownerId }">${vo.storeName }</a>
													</c:if>
												</c:forEach>
												</div>
																						
											</div>
										
										</div>
										
									</div>

								</div>
								
							</div>
						
						</div>
						
						
						<div class="search-restaurant-box">
						
							<div class="row">
							
								<div class="col-sm-4 col-md-3">
								
									<div class="search-restaurant-box-title">

										<h3 class="text-primary text-lowercase mb-0"><a href="#">기타</a></h3>
										<span>Etc</span>
										
									</div>
									
								</div>

								<div class="col-sm-8 col-md-9">

									<div class="search-restaurant-box-content">

										<div class="search-restaurant-box-inner">

											<div class="row gap-20">

												<div class="col-sm-6 col-md-4">
												<c:forEach var="vo" items="${storeList }">
													<c:if test="${vo.category eq '기타' }">
													<a href="reservOnline.do?id=${vo.ownerId }">${vo.storeName }</a>
													</c:if>
												</c:forEach>												</div>

												
											</div>

										</div>

									</div>

								</div>
								
							</div>
						
						</div>
						
					</div>

				</div>
				
			</div>
