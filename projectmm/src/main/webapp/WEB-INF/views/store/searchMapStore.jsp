<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- start hero-header -->
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list">
			<li><a href="main.do">홈</a></li>
			<li><span>지도로 찾기</span></li>
		</ol>

	</div>

</div>
<!-- end hero-header -->


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>여러개 마커에 이벤트 등록하기1</title>
<div class="row">

	<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

		<div class="section-title">
			<br>
			<h2>지도에서 가게 찾기</h2>
			<p>가게 마크를 눌러보세요.</p>

		</div>

	</div>

</div>
</head>
<body>
	<div id="map" style="width: 100%; height: 350px;"></div>

	<script type="text/javascript"
		src="//dapi.kakao.com/v2/maps/sdk.js?appkey=efd02bed66037814cc336b7bda4fa87d"></script>
	<script>
		var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
		mapOption = {
			center : new kakao.maps.LatLng(35.867653, 128.596537), // 지도의 중심좌표
			level : 3
		// 지도의 확대 레벨
		};

		var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

		// 마커를 표시할 위치와 내용을 가지고 있는 객체 배열입니다 
		var positions = [ {
			content : '<div>바스코 , 대구광역시 중구 공평동 동성로4길 100</div>',
			latlng : new kakao.maps.LatLng(35.869211, 128.599735)
		}, {
			content : '<div>야끼리아 , 남일동 120-4번지 1층 중구 대구광역시 KR</div>',
			latlng : new kakao.maps.LatLng(35.869328, 128.593901)
		}, {
			content : '<div>오드다이너  , 대구광역시 중구 동성로3길 28-1</div>',
			latlng : new kakao.maps.LatLng(35.866506, 128.597778)
		}, {
			content : '<div>유이쯔  , 대구광역시 중구 동성로3길 28-1</div>',
			latlng : new kakao.maps.LatLng(35.866335, 128.594615)
		}, {
			content : '<div>도쿄다이닝  , 대구광역시 중구 봉산동 4-5 동성로2길</div>',
			latlng : new kakao.maps.LatLng(35.866153, 128.596068)
		}, {
			content : '<div>스시라스또 , 대구광역시 중구 공평동 59-4</div>',
			latlng : new kakao.maps.LatLng(35.868417, 128.598135)
		}, {
			content : '<div>도담 , 대구광역시 중구 삼덕동1가 동성로5길 84</div>',
			latlng : new kakao.maps.LatLng(35.866970, 128.598981)
		}, {
			content : '<div>미란다키친  ,  대구광역시 중구 동성로1길 29-20</div>',
			latlng : new kakao.maps.LatLng(35.866914, 128.595511)
		}, {
			content : '<div>다나 ,  대구광역시 중구 동성로2길 12-24</div>',
			latlng : new kakao.maps.LatLng(35.866288, 128.596468)
		}, {
			content : '<div>맥주공장  ,  대구광역시 중구 삼덕동1가 7-2</div>',
			latlng : new kakao.maps.LatLng(35.867671, 128.596259)
		} ];

		for (var i = 0; i < positions.length; i++) {
			// 마커를 생성합니다
			var marker = new kakao.maps.Marker({
				map : map, // 마커를 표시할 지도
				position : positions[i].latlng
			// 마커의 위치
			});

			// 마커에 표시할 인포윈도우를 생성합니다 
			var infowindow = new kakao.maps.InfoWindow({
				content : positions[i].content
			// 인포윈도우에 표시할 내용
			});

			// 마커에 mouseover 이벤트와 mouseout 이벤트를 등록합니다
			// 이벤트 리스너로는 클로저를 만들어 등록합니다 
			// for문에서 클로저를 만들어 주지 않으면 마지막 마커에만 이벤트가 등록됩니다
			kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(
					map, marker, infowindow));
			kakao.maps.event.addListener(marker, 'mouseout',
					makeOutListener(infowindow));
		}

		// 인포윈도우를 표시하는 클로저를 만드는 함수입니다 
		function makeOverListener(map, marker, infowindow) {
			return function() {
				infowindow.open(map, marker);
			};
		}

		// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
		function makeOutListener(infowindow) {
			return function() {
				infowindow.close();
			};
		}
	</script>
</body>
</html>


<div class="section sm pb-20">

	<div class="container">

		

		<div class="row mb-40 contact-info">
			<div class="col-sm-4">
				<div class="icon icon-default" data-icon="G"></div>
				<h4>New York Office</h4>
				<address>
					<p>Address: 350 Fifth Avenue, 34th floor New York, NY
						10118-3299 USA</p>
				</address>
			</div>
			<div class="col-sm-4">
				<div class="icon icon-default" data-icon="Q"></div>
				<h4>Phones &amp; Email</h4>
				<dl class="dl-horizontal">
					<dt>Phone:</dt>
					<dd>
						<p>
							+370 212 290 4743<br>+370 212 736 1343
						</p>
					</dd>
					<dt>Fax:</dt>
					<dd>
						<p>+370 212 290 4743</p>
					</dd>
					<dt>Email:</dt>
					<dd>
						<p>
							<a href="mailto:info@thefoody.com">info@thefoody.com</a>
						</p>
					</dd>
				</dl>
			</div>
			<div class="col-sm-4">
				<div class="icon icon-default" data-icon="2"></div>
				<h4>Contact Information</h4>
				<p>We work from 8:00 AM till 5:00 PM (UTC+02:00).</p>
			</div>
		</div>

	</div>

</div>

<div class="contact-map">

	<div id="map" data-lat="51.4435746" data-lon="0.1522334"
		style="width: 100%; height: 500px;"></div>

	<div class="infobox-wrapper shorter-infobox contact-infobox">
		<div id="infobox">
			<div class="infobox-address">
				<h6>We Are Here</h6>
			</div>

		</div>
	</div>

</div>




