<%@page import="store.vo.StoreVO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- start Main Wrapper -->
<div class="main-wrapper">

	<div class="second-search-restaurant-wrapper">

		<div class="container">

			<form>

				<div class="second-search-result-inner">
					<span class="labeling">Search a restaurant</span>
					<div class="row">

						<div class="col-xss-12 col-xs-6 col-sm-6 col-md-5">
							<div class="form-group form-lg">
								<input type="text" class="form-control" placeholder="대구 동성로" />
							</div>
						</div>

						<div class="col-xss-12 col-xs-6 col-sm-6 col-md-5">
							<div class="form-group form-lg">
								<input type="text" class="form-control" name="keyword"
									value="${searchvo.keyword }" placeholder="가게이름을 입력하세요" />
							</div>
						</div>

						<div class="col-xss-12 col-xs-6 col-sm-4 col-md-2">
							<button type="button" class="btn btn-block" onclick="list()">검색</button>
						</div>

					</div>
				</div>

			</form>

		</div>

	</div>

	<!-- start hero-header -->
	<div class="breadcrumb-wrapper">

		<div class="container">

			<ol class="breadcrumb-list booking-step">
				<li><a href="main.do">Home</a></li>
				<li><span>가게 목록</span></li>
			</ol>

		</div>

	</div>
	<!-- end hero-header -->

	<div class="section sm">

		<div class="container">

			<div class="sorting-wrappper"></div>

			<div class="restaurant-wrapper">

				<div class="row">

					<div class="col-sm-8 col-md-9 mt-25">

						<div class="restaurant-list-wrapper">
							<div class="restaurant-item-list featured">


								<div class="image">
									<img src="project/images/brands/06.png" alt="image" />
								</div>

								<div class="content">
									<div class="restaurant-item-list-info">
										<div class="row">

											<div class="col-sm-7 col-md-8">
												<input type ="hidden" id = "ownerId" value="">
												<h4 class="heading">Arabiangrill Indiantakeaway</h4>
												<p class="texing">Forget hunting around for paper
													takeaway menus, at thefoody.io you can instantly access a
													wide array of takeaway restaurants in your local
													area.Browse through peer-reviewed takeaway menus that
													include opening times, accepted payment types and special
													offers.</p>
											</div>



											<div class="col-sm-5 col-md-4">
												<ul class="meta-list">
													<li><span>위치:</span> Paris, France</li>
													<li><span>평점:</span> 3.2</li>

												</ul>
											</div>

										</div>

									</div>

									<div class="restaurant-item-list-bottom">

										<div class="row">

											<div class="col-sm-7 col-md-8">
												<div class="sub-category">
													<span> <span style="color: #ED1C3B;"><i
															class="fa fa-star"></i></span> <span style="color: #ED1C3B;"><i
															class="fa fa-star"></i></span> <span style="color: #ED1C3B;"><i
															class="fa fa-star"></i></span> <span style="color: #CCCCCC;"><i
															class="fa fa-star"></i></span> <span style="color: #CCCCCC;"><i
															class="fa fa-star"></i></span> <span class="review"> (<span>35</span>)
													</span>
													</span>
												</div>
											</div>

											<div class="col-sm-5 col-md-4">
												<input type="hidden" class="storeId" id="storeId" value="">
												<a href="summitReserv.do" class="btn btn-primary"
													id="rsvBtn">예약하기</a> <a href="#"
													class="btn btn-primary waiting" id="waiting">대기하기</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>



					</div>

					<!--  사이드바 시작 -->

					<div class="col-sm-4 col-md-3 mt-25">

						<div class="restaurant-list-wrapper">

							<aside class="sidebar with-filter">

								<div class="sidebar-inner">

									<div class="sidebar-module">
										<h4 class="sidebar-title">&emsp;&emsp;&emsp;&emsp;신규 맛집</h4>

										<div class="sub-category" id='sc'>
											<a href="#">Bangkok Lounge</a>

										</div>

									</div>
									<div class="sidebar-module">
										<h4 class="sidebar-title">&emsp;&emsp;&emsp;&emsp;지금 뜨는
											맛집</h4>

										<div class="top-company-2-wrapper">

											<div class="GridLex-gap-10">

												<div class="GridLex-grid-noGutter-equalHeight">

													<div class="GridLex-col-12_sm-12_xs-6_xss-12">

														<div class="top-company-2">
															<a href="#">

																<div class="image">
																	<img src="images/brands/08.png" alt="image" />
																</div>

																<div class="content">
																	<h5 class="heading text-primary font700">Spice
																		Delight</h5>
																	<p class="texting font600">Lorem ipsum dolor sit
																		amet, consectetur and much more...</p>
																	<span class="pull-right icon"><i
																		class="fa fa-long-arrow-right"></i></span>
																	</p>
																</div>

															</a>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>



								</div>

							</aside>

						</div>

					</div>

					<!--  사이드바 끝 -->


					<!-- 페이징...............ㅠ -->
				</div>
				<div class="pager-wrapper">

					<ul class="pager-list">
						<li class="paging-nav"><a href="#"><i
								class="fa fa-angle-double-left"></i></a></li>
						<li class="paging-nav"><a href="#"><i
								class="fa fa-angle-left"></i></a></li>
						<li class="number"><span class="mr-5"><span
								class="font600">page</span></span></li>
						<li class="form">
							<form>
								<input type="text" value="1" class="form-control">
							</form>
						</li>
						<li class="number"><span class="mr-5">/</span> <span
							class="font600">79</span></li>
						<li class="paging-nav"><a href="#">go</a></li>
						<li class="paging-nav"><a href="#"><i
								class="fa fa-angle-right"></i></a></li>
						<li class="paging-nav"><a href="#"><i
								class="fa fa-angle-double-right"></i></a></li>
					</ul>

				</div>
			</div>

		</div>

	</div>


</div>
<!-- end Main Wrapper -->




<script>
list()

function list(){
	
	
	let keyword = document.getElementsByName("keyword")[0].value
	
	fetch(`listStore.do?keyword=\${keyword}`)
		.then(resolve => resolve.json())
		.then(result => {
			console.log(result)
			document.querySelector('.restaurant-list-wrapper').parentElement.innerHTML=`<div class="restaurant-list-wrapper">
				<div class="restaurant-item-list featured">
				

				<div class="image">
					<img src="project/images/brands/06.png" alt="image" />
				</div>

				<div class="content">
					<div class="restaurant-item-list-info">
						<div class="row">
						
							<div class="col-sm-7 col-md-8">
							
								<h4 class="heading">Arabiangrill Indiantakeaway</h4>
								<p class="texing">Forget hunting around for paper
									takeaway menus, at thefoody.io you can instantly access a
									wide array of takeaway restaurants in your local area.Browse
									through peer-reviewed takeaway menus that include opening
									times, accepted payment types and special offers.</p>

							</div>
							
							<input type="hidden" id="memId" value="${id}">
							
							<button class="fa fa-heart text-danger"
								id="jjimBtn">
							</button>

							<div class="col-sm-5 col-md-4">
								<ul class="meta-list">
									<li><span>위치:</span> Paris, France</li>
									<li><span>평점:</span> 3.2</li>

								</ul>
							</div>

						</div>

					</div>

					<div class="restaurant-item-list-bottom">

						<div class="row">

							<div class="col-sm-7 col-md-8">
								<div class="sub-category">
									<span> <span style="color: #ED1C3B;"><i class="fa fa-star"></i></span>
										<span style="color: #ED1C3B;"><i class="fa fa-star"></i></span>
										<span style="color: #ED1C3B;"><i class="fa fa-star"></i></span>
										<span style="color: #CCCCCC;"><i class="fa fa-star"></i></span>
										<span style="color: #CCCCCC;"><i class="fa fa-star"></i></span>
										<span class="review"> (<span>35</span>)
										</span>
									</span>
								</div>
							</div>

							<div class="col-sm-5 col-md-4">
								<input type="hidden" class="storeId" id="storeId" value="">
								<a href="summitReserv.do" class="btn btn-primary" id="rsvBtn">예약하기</a>
								<a href="#" class="btn btn-primary waiting" id="waiting">대기하기</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>`
			
			
			for (let i = 0; i < result.length; i++) {
				let template = document.querySelector('.restaurant-list-wrapper').cloneNode(true);
				template.querySelector('.storeId').value = result[i].ownerId;

				template.querySelector('h4').innerText = result[i].storeName;
				template.querySelector('p').innerText = result[i].storeDesc;
				template.querySelector('img').setAttribute('src', 'images/store/' + result[i].storeImg);
				template.querySelector('li:first-child').innerText = '위치: ' + result[i].storeLocation;
				let atag = template.querySelector('a');
				atag.setAttribute('href', 'reservOnline.do?id=' + result[i].ownerId);
				
				template.querySelector('.btn.btn-primary.waiting').addEventListener('click', goWaitingPage)
				template.querySelector('.text-danger').addEventListener('click', jjimBtn)
				
				//console.log(result[i].ownerId)
				
				document.querySelector('.restaurant-list-wrapper').parentElement.append(template);
			}
			document.querySelector('.restaurant-list-wrapper').style.display = 'none';
				
		})
		.catch(reject => console.log(reject))
	}	


	function jjimBtn() {
		let code = this.parentElement.parentElement.parentElement;
		let storeName = code.children[1].children[0].children[1].querySelector('.storeId').value
		console.log(storeName);
		
		let memid = document.getElementById('memId').value;
		console.log(memid)
		
		const jjim = '.storeId=' + storeName + '&memId=' + memid;
		
		fetch("addjjim.do", {
			method: 'post',
	   		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	   		body: jjim
		})
		.then(resolve => resolve.json())
		.then(result => {
			console.log(result);
			if(result.retCode=="Success"){
	   			alert('찜이 추가되었습니다.');
	   		}else if(result.retCode=="Fail"){
	   			alert('처리중 오류 발생.');
	   		}else if(result.retCode=="Overlap"){
	   			alert('이미 찜이 되어 있습니다.')
	   		}
		})
		
	}
		
		
		
		
		
		
		
		
	

	
	function goWaitingPage(){
			location.href = 'waitingPage.do?ownerId' + ownerId;
		}
	
	fetch('newStore.do')
		.then(resolve => resolve.json())
		.then(result => {

			for (let i = 0; i < 5; i++) {
				let template = document.querySelector('#sc').cloneNode(true);
				template.querySelector('a').innerText = result[i].storeName;
				document.querySelector('#sc').parentElement.append(template);
				let atag = template.querySelector('a');
				atag.setAttribute('href', 'reservOnline.do?id=' + result[i].ownerId);

			}
			document.querySelector('#sc').style.display = 'none';
		})
		.catch(reject => console.log(reject))
	
	fetch('randomListStore.do')
	.then(resolve => resolve.json())
	.then(result => {

		for (let i = 0; i < 3; i++) {
			let template = document.querySelector('.top-company-2-wrapper').cloneNode(true);
			template.querySelector('h5').innerText = result[i].storeName;
			template.querySelector('p').innerText = result[i].storeDesc;
			document.querySelector('.top-company-2-wrapper').parentElement.append(template);
			let atag = template.querySelector('a');
			atag.setAttribute('href', 'reservOnline.do?id=' + result[i].ownerId);
			template.querySelector('img').setAttribute('src', 'images/store/' + result[i].storeImg);

		}
		document.querySelector('.top-company-2-wrapper').style.display = 'none';
	})
	.catch(reject => console.log(reject))
	

	
</script>

<script src="js/goWaiting.js"></script>