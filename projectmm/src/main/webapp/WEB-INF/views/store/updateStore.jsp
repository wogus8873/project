<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>프로필 수정</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/store/${storeImg}" alt="image" class="img-circle" />
							</div>

							<h4>${storeName}</h4>
							<p class="user-role">${ownerName}(${ownerId})</p>

						</div>

						
						<ul class="admin-user-menu clearfix">
							<li><a href="storeProfile.do"><i class="fa fa-user"></i> 프로필</a></li>
							<li><a href="ownerMyPage.do"><i class="fa fa-key"></i>메뉴등록</a></li>
							<li><a href="char.do"><i class="fa fa-sign-out"></i> 판매량</a></li>
							<li><a href="listReserv.do"><i class="fa fa-sign-out"></i> 예약관리</a></li>
							<li><a href="logout.do"><i class="fa fa-sign-out"></i> Logout</a></li>
						</ul>

					</div>

				</div>



				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">

						<div class="admin-section-title">

							<h3>프로필 수정</h3>


						</div>

						<form id = "storeInfo" class="post-form-wrapper" onsubmit="return false" enctype="multipart/form-data" >

							<div class="row gap-20">

								<div class="col-sm-6 col-md-4">
									<div class="form-group">
										<input type="hidden" class="form-control" id="ownerId" value="${ownerId}">
									</div>

									<div class="form-group bootstrap-fileinput-style-01">
										<label>가게 사진</label>
										<input type="file" name="storeImg" class="form-register-photo-2"
											id="storeImg"> 
											<!--<button type="submit" id="updateImg">등록</button>-->
											

									</div>
								
									<div class="form-group">
									    <label>아이디</label> <input type="text" readonly="readonly" class="form-control" 
											name="ownerId" value="${ownerId}">
									</div>
							
							
									<div class="form-group">
										<label>비밀번호</label> <input type="text" class="form-control" id="ownerPw"
											name="ownerPw" value="${ownerPw}">
									</div>

								</div>

								<div class="clear"></div>

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>전화번호</label> <input type="text" class="form-control" id="ownerPhone"
											name="ownerPhone" value="${ownerPhone}">
									</div>

								</div>

								<div class="clear"></div>
								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>가게 이름</label> <input type="text" class="form-control" id="storeName"
											name="storeName" value="${storeName}">
									</div>

								</div>

								<div class="clear"></div>
								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>가게 설명</label> <input type="text" class="form-control" id="storeDesc"
											name="storeDesc" value="${storeDesc}" >
									</div>

								</div>

								<div class="clear"></div>
								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>카테고리</label> <input type="text" class="form-control" id="category"
											name="category" value="${category}">
									</div>

								</div>

								<div class="clear"></div>
								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>위치</label> <input type="text" class="form-control" id="storeLocation"
											name="storeLocation" value="${storeLocation}">
									</div>

								</div>

								<div class="clear"></div>


								<div class="col-sm-6 col-md-4">

									<div class="col-sm-12 mt-10">
										<button class="btn btn-primary" id="modBtn">수정</button>
										<button type="reset" class="btn btn-warning">취소</button>
									</div>
								</div>

						</form>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>


<script src="js/updateStoreFetch.js"></script>

