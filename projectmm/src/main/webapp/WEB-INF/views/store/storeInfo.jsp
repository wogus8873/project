<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>프로필</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/store/${storeImg}" alt="image" class="img-circle" />
							</div>

							<h4>${storeName}</h4>
							<p class="user-role">${ownerName}(${ownerId})</p>

						</div>

						<div class="admin-user-action text-center">
							<a href="updateStoreForm.do" class="btn btn-primary btn-sm btn-inverse">프로필 수정</a>
						</div>

						
						<ul class="admin-user-menu clearfix">
							<li class="active"><a href="storeProfile.do"><i class="fa fa-user"></i> 프로필</a></li>
							<li><a href="ownerMyPage.do"><i class="fa fa-key"></i>메뉴등록</a></li>
							<li><a href="char.do"><i class="fa fa-sign-out"></i> 판매량</a></li>
							<li><a href="listReserv.do"><i class="fa fa-sign-out"></i> 예약관리</a></li>
							<li><a href="logout.do"><i class="fa fa-sign-out"></i> Logout</a></li>
						</ul>

					</div>

				</div>



				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">
<input type="hidden" class="form-control" id="ownerId" value="${ownerId}">
						<div class="admin-section-title">

							<h3>프로필</h3>


						</div>


</p>
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>아이디 </a>: </strong> <span>${ownerId}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>이름 </a>: </strong> <span>${ownerName}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>폰 번호 </a>: </strong> <span>${ownerPhone}</span></li>
                </ul>
              </div>
              
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>가게이름 </a>: </strong> <span>${storeName}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>가게위치 </a>: </strong> <span>${storeLocation}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>가게설명 </a>: </strong> <span>${storeDesc}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>카테고리 </a>: </strong> <span>${category}</span></li>
                </ul>
              </div>
            </div>
            <p>



					</div>

				</div>

			</div>

		</div>

	</div>

</div>


