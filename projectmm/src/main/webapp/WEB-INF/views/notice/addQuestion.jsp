<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<form action="insertQuestion.do" class="comment-form">

	<div class="section sm">

		<div class="container">
			<div class="row">
				<h3>
					<a href="blog-single.html" class="inverse">작성하기</a>
				</h3>
				<div class="col-md-9">
					<div class="form-horizontal">

						<div class="form-group">
							<label class="col-xs-12 col-sm-6 col-md-6">작성자<span
								class="text-danger">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="writer" class="form-control"
									value="${id }" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="col-xs-12 col-sm-6 col-md-6">제목<span
								class="text-danger">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="title" class="form-control"
									value="${vo.questionTitle }">
							</div>
						</div>

						<div class="form-group">
							<label class="col-xs-12 col-sm-6 col-md-6">내용<span
								class="text-danger">*</span></label>
							<div class="col-sm-9">
								<textarea placeholder="내용 입력" rows="3" tabindex="4"
									style="resize: none;" name="subject" class="form-control">${vo.questionContent }</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label hidden-xs"></label>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary pull-right">등록</button>
							</div>
						</div>

					</div>

				</div>
				<!-- Main row end -->
			</div>
		</div>
	</div>

	<!-- END Reservation -->

</form>