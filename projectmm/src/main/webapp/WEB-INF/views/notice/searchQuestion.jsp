<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<br>
<br>
<form name="myFrm">
	<input type="hidden" name="num" value="${vo.questionId }">

	<h3>
		<a href="blog-single.html" class="inverse">&ensp;문의사항</a>
	</h3>

	<table class="table ">

		<tr>
			<th>&emsp;번호</th>
			<td>${vo.questionId }</td>
		</tr>
		<tr>
			<th>&emsp;작성자</th>
			<td>${vo.memId }</td>
		</tr>
		<tr>
			<th>&emsp;제목</th>
			<td><input type="text" class="form-control" style="width: 50%"
				name="title" value="${vo.questionTitle }"></td>
		<tr>
			<th>&emsp;내용</th>
			<td><textarea class="form-control" style="width: 50%" cols=30
					name="subject" rows=5>${vo.questionContent }</textarea> <br> <input
				type="button" value="수정" class="btn btn-primary" onclick="modFunc()">
				<input type="button" value="삭제" class="btn btn-primary"
				onclick="delFunc()"></td>
			<td colspan="2"></td>
		</tr>

	</table>

	<br> <br>
	<h3>
		<a href="blog-single.html" class="inverse">&ensp;댓글</a>
	</h3>

	<div id="show" style="text-align: center">
		<table class="table">
			<thead>
				<tr>
					<th style="width: 8%"><a>&emsp;관리자 | </a></th>
					<th style="width: 10%"><a>내용</a></th>
					<td style=width:>${vo.answerContent }</td>
				</tr>

			</thead>
		</table>
	</div>
</form>

<input type="hidden" id="num" value="${vo.questionId }">
<div class="container">
	<div class="center">

		<table class="table">

			<tr>
				<td><input type="text" id="content" class="form-control"
					placeholder="관리자만 작성이 가능합니다"></td>
				<c:choose>
					<c:when test="${grade == 'M'}">
						<td style="width: 13%" colspan="2"><input type="submit"
							value="등록" class="btn btn-primary" id="addBtn"
							style="padding: 10px">
							<button type="button" class="btn btn-primary" id="delAnswerBtn">삭제</button></td>
					</c:when>

					<c:otherwise>
						<td style="width: 13%" colspan="2"><input type="submit"
							value="등록" class="btn btn-primary" id="addBtn" disabled>
							<button type="button" class="btn btn-primary" id="delAnswerBtn"
								disabled>삭제</button></td>

					</c:otherwise>
				</c:choose>
			</tr>

		</table>


	</div>
</div>

<script>
	function delFunc() {
		let myFrm = document.querySelector('form[name="myFrm"]');
		myFrm.action = "deleteQuestion.do";
		myFrm.submit(); // form submit 이벤트 발생
	}

	function modFunc() {
		console.log("zz")
		let myFrm = document.querySelector('form[name="myFrm"]');
		myFrm.action = "modQuestion.do";
		myFrm.submit(); // form submit 이벤트 발생
	}
</script>

<script src="js/answerAjax.js"></script>
