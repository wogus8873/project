<%@page import="notice.vo.QuestionVO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="section sm">

	<div class="container">

		<div class="row">

			<div class="col-sm-8 col-md-9">

				<div class="blog-wrapper">

					<!--<div class="blog-item">-->

					<!-- <div class="blog-content"> -->
					<h3>
						<a href="blog-single.html" class="inverse">문의사항</a>
					</h3>

					<ul class="blog-meta">
						<li>문의사항 게시판입니다</li>
					</ul>
					<ul class="blog-meta">
						<li>in <a>Tasty Map</a>, <a>Korea</a></li>
					</ul>
					<div class="form-group">
						<form action="listQuestion.do" method="post">
							<select name="searchCondition" class="form-control"
								style="width: 35%">
								<option disabled>:: 선택하세요 ::</option>
								<option value="writer"
									${searchvo.searchCondition == 'writer' ? 'selected' : '' }>작성자</option>
								<option value="title"
									${searchvo.searchCondition == 'title' ? 'selected' : '' }>제목</option>
							</select> <br> <input type="text" class="form-control"
								style="width: 75%" name="keyword" value="${searchvo.keyword }">
							<br> <input type="submit" value="조회" class="btn btn-primary">
						</form>
					</div>
				</div>
				<br>

				<table class="table">
					<thead>
						<tr>
							<th>번호</th>
							<th>작성자</th>
							<th>제목</th>
							<th>작성일</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="vo" items="${listQuestion }">
							<tr>
								<c:choose>
									<c:when test="${id == vo.memId || grade == 'M'}">
										<td><a href="searchQuestion.do?num=${vo.questionId }">${vo.questionId }</a></td>
									</c:when>

									<c:otherwise>
										<td><a href=#>${vo.questionId }</a></td>

									</c:otherwise>
								</c:choose>
								<td>${vo.memId }</td>
								<td>${vo.questionTitle}</td>
								<td>${vo.questionDate }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

				<a href="addQuestion.do">작성하기 <i class="fa fa-long-arrow-right"></i></a>

				<div class="pager-wrapper mmt">

					<nav class="pager-right">
						<ul class="pagination">

							<li><a href="#" aria-label="Previous"> <span
									aria-hidden="true">&laquo;</span>
							</a></li>

							<c:if test="${pageDTO.prev }">
								<li class="active"><a
									href="listQuestion.do?pageNum=${pageDTO.startPage-1 }&searchCondition=${searchvo.searchCondition}&keyword=${searchvo.keyword}">&laquo;</a></li>
							</c:if>

							<c:forEach var="item" begin="${pageDTO.startPage }"
								end="${pageDTO.endPage }">
								<c:choose>
									<c:when test="${pageDTO.cri.pageNum == item }">
										<li><a class="active"
											href="listQuestion.do?pageNum=${item }&searchCondition=${searchvo.searchCondition}&keyword=${searchvo.keyword}">${item }</a></li>
									</c:when>
									<c:otherwise>
										<li><a
											href="listQuestion.do?pageNum=${item }&searchCondition=${searchvo.searchCondition}&keyword=${searchvo.keyword}">${item }</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>

							<c:if test="${pageDTO.next }">
								<li><a
									href="listQuestion.do?pageNum=${pageDTO.endPage+1 }&searchCondition=${searchvo.searchCondition}&keyword=${searchvo.keyword}">&raquo;</a></li>
							</c:if>

						</ul>
					</nav>

				</div>

			</div>

		</div>

	</div>

</div>