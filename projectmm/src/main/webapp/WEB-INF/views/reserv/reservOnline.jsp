<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
HttpSession requestMID = request.getSession(true);
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.dl-horizontal dd {
    margin-left: 165px;
}

#openbtn {
	font-size: 20px;
}

</style>

<!-- start Main Wrapper -->
<div class="main-wrapper scrollspy-container">

	<div id="order-page-title" class="page-title-style2">

		<div class="container inner-img">
			<div class="order-top-section">
				<div class="Page-title">

					<div class="row single-restaurant-top-section">

						<div class="col-md-8">
							<div class="res-short-info">
								<input type="hidden" class="ownerId" id="ownerId" value="">
								<h2>Food Panda Restaurant</h2>
								<ul class="list-unstyled">
									<li><span class="glyphicon glyphicon-cutlery"></span></li>

									<div class="clearfix"></div>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<div class="restaurant-opening-info">

								<div class="res-rating-all">
									<ul class="list-unstyled">

										<li>
											<div class="ui-res-rating">
												<span class="glyphicon glyphicon-star"
													style="color: #E64D64;"></span> <span
													class="glyphicon glyphicon-star" style="color: #E64D64;"></span>
												<span class="glyphicon glyphicon-star"
													style="color: #E64D64;"></span> <span
													class="glyphicon glyphicon-star" style="color: #CCCCCC;"></span>
												<span class="glyphicon glyphicon-star"
													style="color: #CCCCCC;"></span>
											</div>
										</li>
										<li>
											<div class="ui-rating-text">2 Rating</div>
										</li>
										<div class="clearfix"></div>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- start hero-header -->
	<div class="breadcrumb-wrapper">

		<div class="container">


			<ol class="breadcrumb-list booking-step">
				<li><a href="homepage.html">홈</a></li>
				<li><span><a href="#">예약</a></span></li>
				
			</ol>


		</div>

	</div>
	<!-- end hero-header -->


	<div class="section sm">

		<!-- Content Start -->
		<!-- Start order-online-->
		<div id="order-online">
			<div class="container">


				<!-- Nested Row Starts -->
				<div class="row">
					<!-- Mainarea Starts -->
					<div class="col-md-9 col-xs-12">
						<!-- Menu Tabs Starts -->
						<div class="menu-tabs-wrap">
							<!-- Menu Tabs List Starts -->
							<ul class="nav nav-tabs nav-menu-tabs text-center-xs">
								<li class="active"><a href="#menu" data-toggle="tab">메뉴</a></li>
								<li><a href="#offer" data-toggle="tab">가게정보</a></li>
								<li><a href="#reviews" data-toggle="tab">리뷰</a></li>
								<li><a href="#gallery" data-toggle="tab">메뉴사진</a></li>

							</ul>
							<div class="cfo-checkoutarea">
								<a href="#reservInfo" data-toggle="tab">
								<button id="openbtn" class="btn btn-primary btn-block custom-checkout">
									예약정보 입력
								</button></a>
							</div>
							<!-- Menu Tabs List Ends -->
							<!-- Menu Tabs Content Starts -->
							<div class="tab-content">
								<!-- Tab #1 Starts -->
								<div id="menu" class="tab-pane active">
									<!-- Tab #1 Nested Row Starts -->
									<div class="row">
										<!-- Left Column Starts -->
										<div class="col-sm-4 col-xs-12">
											<div class="side-block-1">
												<h4>예약시 유의사항</h4>
												<ul class="list-unstyled list-style-2">
													<li><i
														class="text-primary ion-android-checkmark-circle"></i> 
														가게와 메뉴를 확인해 주세요.</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														날짜와 시간을 확인해 주세요.<br> &nbsp;&nbsp;&nbsp;&nbsp;15분 지연시 노쇼처리 됩니다.</li>
													
												</ul>
											</div>


										</div>
										<!-- Left Column Ends -->
										<!-- Right Column Starts -->
										<div class="col-sm-8 col-xs-12">
											<!-- Order Menu Tab Pane Starts -->
											<div class="order-menu-tab-pane text-center-xs">
											
												<!-- Order Menu List #1 Starts -->
												<h3>메뉴 리스트</h3>
												
												<div class="spacer"></div>
												<div class="order-menu-item clearfix">
													<div class="pull-left">
														<h6>메뉴 이름</h6>
														<p>메뉴 설명</p>
													</div>
													<div class="pull-right">
														<button class="btn btn-primary animation" type='button'
															id="addCartbtn">
															<span class="price-new" id="menu">7.99</span> <i
																class="fa fa-plus-circle"></i>
														</button>
													</div>
												</div>

												<!-- Order Menu List #1 Ends -->
												<!-- Spacer Starts -->
												<div class="spacer big"></div>
												<!-- Spacer Ends -->

											</div>
											<!-- Order Menu Tab Pane Ends -->
											<!-- Pagination Starts -->
											<div class="pagination-block text-right text-center-xs mb-10">
												<ul class="pagination animation">
													<li><a href="#">&laquo;</a></li>
													<li class="active"><a href="#">1</a></li>
													<li><a href="#">2</a></li>
													<li><a href="#">3</a></li>
													<li><a href="#">&raquo;</a></li>
												</ul>
											</div>
											<!-- Pagination Ends -->
											
										</div>
										<!-- Right Column Ends -->
									</div>
									<!-- Tab #1 Nested Row Ends -->
								</div>
								<!-- Tab #1 Ends -->
								<!-- Tab #2 Starts -->
								<div id="offer" class="tab-pane">
									<!-- Tab #2 Nested Row Starts -->
									<div class="row">
										<!-- Left Column Starts -->
										<div class="col-sm-4 col-xs-12">
											<div class="side-block-1">
												<h4>예약시 유의사항</h4>
												<ul class="list-unstyled list-style-2">
													<li><i
														class="text-primary ion-android-checkmark-circle"></i> 
														가게와 메뉴를 확인해 주세요.</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														날짜와 시간을 확인해 주세요.<br> &nbsp;&nbsp;&nbsp;&nbsp;15분 지연시 노쇼처리 됩니다.</li>
													
												</ul>
												
											</div>
										</div>
										<!-- Left Column Ends -->
										<!-- Right Column Starts -->
										<div class="col-sm-8 col-xs-12">
											<!-- Information Tab Pane Starts -->
											<div class="information-tab-pane">
												<p>
													<img src="project/images/banners/banner-discount.png"
														alt="Discount Banner" class="img-responsive">
												</p>
												<!-- Spacer Starts -->
												<div class="spacer big"></div>
												<!-- Spacer Ends -->
												<div class="name">
													<h4>가게이름</h4>
												</div>
												<div class="desc">
													<p>Thefoody is a nice restaurant offering Indian food
														to takeaway or eat in the restaurant. The restaurant
														offers Indian food takeaway.</p>
													<p>Free bottle fo white wine.</p>
													<p>Spend £39.95 or more</p>
													<p>Only available through our online ordering system on
														orders over £24.95 or more.</p>
												</div>
												<div class="loca">
													<h6 i class="fa fa-tag">
														</i>위치
													</h6>
													<br>
													<h5 i class="fa fa-phone">
														</i>번호
													</h5>

													<br>


												</div>




												<hr>
												<!-- Opening Hours Starts -->
												<h5>
													<i class="fa fa-clock-o"></i> Opening Hours
												</h5>
												<ul class="list-unstyled timing-list">
													<li class="clearfix"><span class="pull-left">월요일</span>
														<span class="pull-right text-right mon">10:00 -
															22:00</span></li>
													<li class="clearfix"><span class="pull-left">화요일</span>
														<span class="pull-right text-right tue">10:00 -
															22:00</span></li>
													<li class="clearfix"><span class="pull-left">수요일</span>
														<span class="pull-right text-right wed">10:00 -
															22:00</span></li>
													<li class="clearfix"><span class="pull-left">목요일</span>
														<span class="pull-right text-right thu">10:00 -
															22:00</span></li>
													<li class="clearfix"><span class="pull-left">금요일</span>
														<span class="pull-right text-right fri">10:00 -
															22:00</span></li>
													<li class="clearfix"><span class="pull-left">토요일</span>
														<span class="pull-right text-right sat">10:00 -
															22:00</span></li>
													<li class="clearfix"><span class="pull-left">일요일</span>
														<span class="pull-right text-right sun">10:00 -
															22:00</span></li>
												</ul>
												<!-- Takeway Hours Ends -->
												<hr>
												<!-- Spacer Starts -->
												<div class="spacer"></div>
												<!-- Spacer Ends -->

											</div>
											<!-- Information Tab Pane Ends -->
										</div>
										<!-- Right Column Ends -->
									</div>
									<!-- Tab #1 Nested Row Ends -->
								</div>
								<!-- Tab #2 Ends -->
								<!-- 메뉴 사진 -->
								<div id="gallery" class="tab-pane">
									<!-- Image Gallery Starts -->
									<ul class="row list-unstyled gallery-grid">
										<!-- Gallery Image #1 Starts -->
										<li id="menuImg" class="col-sm-4 col-xs-6">
											<div class="hover-content">
												<img src="images/portfolio/portfolio01.png"
													alt="Gallery Image"
													class="img-responsive img-center-sm img-center-xs">
												<div class="overlay animation">
													<a href="images/portfolio/portfolio01.png"
														class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</li>
										<!-- Gallery Image #1 Ends -->
									</ul>
								</div>
								<!-- 메뉴 사진 끝 -->
								<!-- Tab #4 Starts -->
								<div id="reviews" class="tab-pane">
									<!-- Tab #4 Nested Row Starts -->
									<div class="row">
										<!-- Left Column Starts -->
										<div class="col-sm-4 col-xs-12">
											<div class="side-block-1">
												<h4>리뷰 작성시 유의사항</h4>
												<ul class="list-unstyled list-style-2">
													<li><i
														class="text-primary ion-android-checkmark-circle"></i> 남겨 주시는 모든 리뷰는 <br> &nbsp;&nbsp;&nbsp;&nbsp; 가게 예약시 많은 도움이 됩니다. </li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														욕설 및 비방 등의 내용은 <br> &nbsp;&nbsp;&nbsp;&nbsp;이용 제한이 될 수 있습니다.</li>
													
												</ul>
											</div>
										</div>

										<!-- 리뷰 시작 -->
										<!-- Left Column Ends -->
										<!-- Right Column Starts -->

										<div class="col-sm-8 col-xs-12">
											<!-- Reviews Tab Pane Starts -->
											<div class="reviews-tab-pane">
												<!-- Reviews Form Box Starts -->

												<div class="reviews-form-box">
													<input type="hidden" id=memid value="${id }"> <input
														type="hidden" id=ownerid>

													<h6>리뷰를 남겨주세요</h6>

													<textarea class="form-control" rows="4"
														placeholder="욕설 및 비방 등의 내용은 이용 제한이 될 수 있습니다" id="review"></textarea>
													<div class="clearfix">
														<ul
															class="list-unstyled list-inline rating-star pull-left">

															<li><i class="fa fa-star" id="5-stars"></i></li>
															<li><h6>평점</h6></li>
															<li>
																<div class="input_wrap">
																	<div class="rating_div">
																		<select id="rating" class="form-control">
																			<option value="1">1점</option>
																			<option value="2">2점</option>
																			<option value="3">3점</option>
																			<option value="4">4점</option>
																			<option value="5">5점</option>
																		</select>
																	</div>
																</div>
															</li>
														</ul>
														<button class="btn btn-primary mt-5 pull-right"
															id="addBtn" name="addBtn">등록</button>
													</div>
												</div>

												<!-- Reviews Form Box Ends -->
												<!-- Reviews List Starts -->
												<div class="reviews-box">
													<!-- Review #1 Starts -->
													<div class="review-list">
														<div class="clearfix">
															<div class="pull-left">
																<p class="reviewid" type="text">reviewid</p>
																<h6 class="date">
																	<i class="fa fa-calendar"></i><span>Mar 10, 2016
																	</span>
																</h6>
																<h6 class="memid">By</h6>
																<ul class="list-unstyled list-inline rating-star-list">
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star-o"></i></li>
																	<li><i class="fa fa-star-o"></i></li>
																</ul>
															</div>
														</div>							

																<button class="btn btn-primary mt-5 pull-right del"
																	id="delBtn" name="delBtn">삭제</button>

														<div class="review-list-content">
															<p>The delivery guy said he will be back in 10 mins
																with the missing items, but never came back. when I
																called the restaurant, they took another 30 mins to
																deliver.</p>
														</div>



													</div>

													<!-- Review #1 Ends -->

												</div>
												<!-- Reviews List Ends -->
												<!-- Spacer Starts -->
												<div class="spacer-1 condensed"></div>
												<!-- Spacer Ends -->
												<!-- Banners Starts -->

												<!-- Banners Ends -->
											</div>
											<!-- Reviews Tab Pane Ends -->
										</div>

										<!-- Right Column Ends -->
										<!-- 리뷰 끝 -->
									</div>
									<!-- Tab #4 Nested Row Ends -->
								</div>



								<!-- Tab #4 Ends -->
								<!-- Tab #5 Starts -->
								<div id="reachus" class="tab-pane">
									<!-- Tab #5 Nested Row Starts -->
									<div class="row">
										<!-- Left Column Starts -->
										<div class="col-sm-4 col-xs-12">
											<div class="side-block-1">
												<h6>Delivery Menu</h6>
												<ul class="list-unstyled list-style-2">
													<li><i
														class="text-primary ion-android-checkmark-circle"></i> Top
														Kebab and Pizza</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Bombay Lancer</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Bombay Lancer</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Gillingham Grill</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Chinese Starters</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														North Indian Main Course</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Traditional Telugu Maincourse</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Indian Breads</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Rice, Biryani &amp; Pulao</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Accompaniments</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Desserts &amp; Beverages</li>
													<li><i
														class="text-primary ion-android-checkmark-circle"></i>
														Flamers Pizza Ltd</li>
												</ul>
											</div>
										</div>
										<!-- Left Column Ends -->
										<!-- Right Column Starts -->
										<div class="col-sm-8 col-xs-12">
											<!-- Reach Us Tab Pane Starts -->
											<div class="reachus-tab-pane">
												<!-- Map Starts -->
												<div class="map"></div>
												<!-- Map Ends -->
												<!-- Address Block Starts -->
												<div class="address-block">
													<h5>
														<i class="fa fa-tag"></i> Address
													</h5>
													<ul class="list-unstyled">
														<li># 3453 corn street, Sanford, FL 34232.,</li>
														<li>Cardiff, London.</li>
													</ul>
													<h5>
														<i class="fa fa-phone"></i> Phone Numbers
													</h5>
													<ul class="list-unstyled">
														<li>+0 (123) 456 - 7890, +0 (123) 456 - 7890</li>
													</ul>
													<h5>
														<i class="fa fa-mobile"></i> Can be in touch with Watsapp
													</h5>
													<ul class="list-unstyled">
														<li>+0 (123) 456 - 7890</li>
													</ul>
												</div>
												<!-- Address Block Ends -->
												<!-- Reach Form Starts -->
												<div class="reachus-form">
													<h5>Drop us a mail for Table Reservation</h5>
													<form action="#">
														<div class="form-group">
															<label class="sr-only" for="reachus-name">Name</label> <input
																type="text" class="form-control flat" id="reachus-name"
																placeholder="Name">
														</div>
														<div class="form-group">
															<label class="sr-only" for="reachus-email">E-mail</label>
															<input type="email" class="form-control flat"
																id="reachus-email" placeholder="e-mail">
														</div>
														<div class="form-group">
															<label class="sr-only" for="reachus-mobileno">Mobile
																No</label> <input type="text" class="form-control flat"
																id="reachus-mobileno" placeholder="Mobile No">
														</div>
														<div class="row">
															<div class="col-xs-6">
																<div class="form-group">
																	<div class="input-group">
																		<label class="sr-only" for="reachus-date">Date</label>
																		<input type="text"
																			class="form-control flat datepickerInput"
																			id="reachus-date" placeholder="Date"> <span
																			class="input-group-addon flat"> <span
																			class="fa fa-calendar"></span>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-xs-6">
																<div class="form-group">
																	<label class="sr-only">No.of Persons</label> <select
																		class="form-control flat">
																		<option>No.of Persons</option>
																		<option>1</option>
																		<option>2</option>
																		<option>3</option>
																		<option>4</option>
																		<option>5</option>
																	</select>
																</div>
															</div>

														</div>
														<div class="form-group">
															<label class="sr-only" for="reachus-info">Your
																Information</label>
															<textarea class="form-control flat" id="reachus-info"
																placeholder="Your Information" rows="5"></textarea>

														</div>
														<div class="clearfix">
															<button type="submit"
																class="btn btn-primary mt-5 animation pull-right">Submit</button>
														</div>
													</form>
												</div>
												<!-- Reach Form Ends -->
												<!-- Banners Starts -->
												<div class="row">
													<div class="col-xs-6">
														<img src="project/images/banners/banner-img1.png"
															alt="Banner 1" class="img-responsive img-center-xs">
													</div>
													<div class="col-xs-6">
														<img src="project/images/banners/banner-img2.png"
															alt="Banner 2" class="img-responsive img-center-xs">
													</div>
												</div>
												<!-- Banners Ends -->
											</div>
											<!-- Reach Us Tab Pane Ends -->
										</div>
										<!-- Right Column Ends -->
									</div>
									<!-- Tab #5 Nested Row Ends -->
								</div>
								<!-- Tab #5 Ends -->
								<!-- Tab reservInfo Start -->
								<div id="reservInfo" class="tab-pane">
									<!-- Tab #2 Nested Row Starts -->
									<div class="row">
										<!-- Right Column Starts -->
										<div id="order-process">
											<div class="container">
												<div class="order-inner-content">
													<div class="row">
														<div class="col-md-9">
															<form class="info">
																<div class="order-process-left-side">
																	<div class="order-information row">
																		<div class="col-md-6">
																			<h4 class="section-title pb-10">예약 정보</h4>
																			<ul class="list-unstyled info-list">
																				<li><input class="custom-radio-big"
																					type="radio" name="collection" id="collection"
																					checked=""> <label for="collection">예약
																						시간 15분이 지나면 노쇼처리 됩니다.</label></li>
																				<li>
																					<div class="row">
																						<div class="col-md-6 col-xs-6 col-sm-6">
																							<input class="custom-radio-big" type="radio"
																								name="delivery-priority" id="asap" checked="">
																							<label for="asap">아동포함</label>
																						</div>
																						<div class="col-md-6 col-xs-6 col-sm-6">
																							<input class="custom-radio-big" type="radio"
																								name="delivery-priority" id="later"> <label
																								for="later">아동미포함</label>
																						</div>
																					</div>
																					<div class="row order-time-options">
																						<div class="col-md-6">
																							<select name="reservDate" id="reservDate">
																								<option value="">날짜를 고르세요</option>
																								<option value="01-19"
																									${reservDate == '01-19' ? 'selected' : '' }>2023-01-19</option>
																								<option value="01-20"
																									${reservDate == '01-20' ? 'selected' : '' }>2023-01-20</option>
																								<option value="01-21"
																									${reservDate == '01-21' ? 'selected' : '' }>2023-01-21</option>
																								<option value="01-22"
																									${reservDate == '01-22' ? 'selected' : '' }>2023-01-22</option>
																								<option value="01-23"
																									${reservDate == '01-23' ? 'selected' : '' }>2023-01-23</option>
																								<option value="01-24"
																									${reservDate == '01-24' ? 'selected' : '' }>2023-01-24</option>
																								<option value="01-25"
																									${reservDate == '01-25' ? 'selected' : '' }>2023-01-25</option>
																							</select>
																						</div>
																						<div class="col-md-6">
																							<select name="reservTime" id="reservTime">
																								<option value="">시간을 고르세요</option>
																								<option value="10:30"
																									${reservTime == '10:30' ? 'selected' : '' }>10:30</option>
																								<option value="11:30"
																									${reservTime == '11:30' ? 'selected' : '' }>11:30</option>
																								<option value="12:00"
																									${reservTime == '12:00' ? 'selected' : '' }>12:00</option>
																								<option value="12:30"
																									${reservTime == '12:30' ? 'selected' : '' }>12:30</option>
																								<option value="13:00"
																									${reservTime == '13:15' ? 'selected' : '' }>13:15</option>
																								<option value="14:00"
																									${reservTime == '14:00' ? 'selected' : '' }>14:00</option>
																								<option value="15:00"
																									${reservTime == '15:00' ? 'selected' : '' }>15:00</option>
																								<option value="16:00"
																									${reservTime == '16:00' ? 'selected' : '' }>16:00</option>
																								<option value="17:00"
																									${reservTime == '17:00' ? 'selected' : '' }>17:00</option>
																								<option value="18:00"
																									${reservTime == '18:00' ? 'selected' : '' }>18:00</option>
																								<option value="18:30"
																									${reservTime == '18:30' ? 'selected' : '' }>18:30</option>
																								<option value="19:00"
																									${reservTime == '19:00' ? 'selected' : '' }>19:00</option>
																								<option value="19:30"
																									${reservTime == '19:30' ? 'selected' : '' }>19:30</option>
																								<option value="20:00"
																									${reservTime == '20:00' ? 'selected' : '' }>20:00</option>
																								<option value="20:30"
																									${reservTime == '20:30' ? 'selected' : '' }>20:30</option>
																							</select>
																						</div>
																					</div>
																				</li>
																			</ul>
																		</div>
																		<div class="col-md-6">
																			<h4 class="section-title pb-10 mb-15">요청 사항</h4>
																			<textarea class="special-request"
																				name="special-request" id="special-request"
																				placeholder="300자 이내 입력"></textarea>
																		</div>
																	</div>
																	<div class="row order-delivery-address">
																		<div class="col-md-6">
																			<h4 class="section-title pb-10 mb-15">인원 수</h4>
																			<ul class="list-unstyled">
																				<li><input type="text" class="text"
																					id="personCount" name="personCount"
																					placeholder="인원 수를 입력하세요(숫자만 입력)"></li>
																			</ul>
																		</div>
																		<div class="col-md-6">
																			<h4 class="section-title pb-10 mb-15">주의사항</h4>
																			<div class="postcode">가게에서 적어준 예약시 주의사항</div>
																		</div>
																	</div>
																	<div class="row order-payment-method">
																		<div class="col-md-12">
																			<h4 class="section-title pb-10">결제 수단</h4>
																			<ul class="list-inline payment-method-options">
																				<li><input class="custom-radio-big"
																					type="radio" name="payment-option" id="method-cash">
																					<label for="method-cash">일반카드결제</label></li>
																				<li><input class="custom-radio-big"
																					type="radio" name="payment-option"
																					id="method-paypal"> <label
																					for="method-paypal">카카오톡 결제</label></li>
																			</ul>
																		</div>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- Right Column Ends -->
									</div>
									<!-- Tab #1 Nested Row Ends -->
								</div>
								<!-- Tab reservInfo End -->



							</div>
							<!-- Menu Tabs Content Ends -->
						</div>
						<!-- Menu Tabs Ends -->
					</div>
					<!-- Mainarea Ends -->
					<!-- Sidearea Starts -->
					<div class="col-md-3 col-xs-12">
						<!-- Spacer Starts -->
						<div class="spacer-1 medium hidden-lg hidden-md"></div>
						<!-- Spacer Ends -->
						<!-- Your Order Starts -->
						<div class="side-block-order border-radius-4">
							<!-- Heading Starts -->
							<h5 class="text-center">
								<i class="fa fa-shopping-basket"></i> 예약 메뉴
							</h5>
							<!-- Heading Ends -->
							<!-- Order Content Starts -->
							<div class="side-block-order-content">
								<!-- Order Item List Starts -->
								<!-- 장바구니 -->
								<ul class="list-unstyled order-item-list">
									<!-- 장바구니 메뉴1 -->
									<li class="searchMenu">
										<div class="clearfix">
											<div class="pull-left">
												<div class="update-product">
													<a title="Add a product" class="plusbtn" href="#"
														type='button'> <i class="fa fa-plus-circle"
														onclick="return false;"></i>
													</a> <span class='resultNumber' id='result'>수량</span> <a
														title="Minus a product" class='minusbtn' href="#"
														type='button' onclick="return false;"> <i
														class="fa fa-minus-circle"></i>
													</a>
												</div>
											</div>
											<div class="cart-product-name pull-left" id="menuId">
												선택 메뉴</div>
											<div class="cart-product-price pull-right text-spl-color">
												<input class="mulnumber" id="mul" type="hidden" value="1000">
												<span class="mulresult" id="mulresult">가격</span>
											</div>
											<div class="cart-product-remove">
												<a title="Remove a product" class="trashbtn" href="#"
													type='button' id="remove" onclick="return false;"> <i
													class="fa fa-trash"></i>
												</a>
											</div>
										</div>
									</li>
									<!-- 장바구니 메뉴 -->
								</ul>
								<!-- 장바구니끝 -->
								<!-- Order Item List Ends -->
								<!-- Order Item Total Starts -->
								<dl class="dl-horizontal order-item-total">
									<dt class="text-light">음식 합계 :</dt>
									<dd class="text-spl-color text-right">
										<input type="hidden" id="sumValue" value="총합">0원
									</dd>
									<dt class="text-light">예약금 :</dt>
									<dd class="text-spl-color text-right">2000원</dd>
									<hr>
									<dt class="text-bold">총 합계 :</dt>
									<dd class="text-bold text-spl-color text-right"></dd>
								</dl>
								<!-- Order Item Total Ends -->
								<div class="cfo-checkoutarea">
									<input type="hidden" id="memId" value="${id}">
									<button id="submitbtn" name="submitbtn"
										class="btn btn-primary btn-block custom-checkout">예약
										등록 후 결제</button>
								</div>
							</div>
							<!-- Order Content Ends -->
						</div>
						<!-- Your Order Ends -->


						<!-- start add wrapper -->
						<div id="ad-wrapper">

							<img class="img-responsive"
								src="project/images/add-banner/add-banner.png" alt="">

						</div>
						<!-- end add wrapper -->

					</div>
					<!-- Sidearea Ends -->
				</div>
				<!-- Nested Row Ends -->
			</div>
			<!--.container-->
		</div>
		<!--#order-online-->
		<!--end order-online-->
		<!-- Content End -->

		<!-- start mobile footer nav -->
		<nav
			class="navbar navbar-default navbar-fixed-bottom visible-xs visible-sm mobile-cart-nav">
			<div class="mobile-cart-inner-content">
				<div class="row">
					<div class="col-md-4 col-xs-4">
						<div class="mobile-cart-item">
							<a id="mobileCartToggle" href="#"><i
								class="fa fa-shopping-basket"></i><span id="cart-item">
									10</span></a>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="mobile-total-amount">
							Total: &pound;<span id="total-cart-amount">50</span>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<a href="#" class="btn mobile-btn-checkout">Checkout</a>
					</div>
				</div>
			</div>
		</nav>
		<!-- end mobile footer nav -->

	</div>

</div>
<!-- end Main Wrapper -->


<!-- Start Pages Title  -->




<script type="text/javascript">
	
</script>
<script src="js/reservFetch.js"></script>
<script src="js/menuScripts.js"></script>
<script src="js/reviewFetch.js"></script>
<script src="js/menuScripts.js"></script>
<script src="js/store.js"></script>

