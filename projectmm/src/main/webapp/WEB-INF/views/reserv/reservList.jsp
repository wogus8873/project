<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>메뉴등록 및 조회</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/store/${storeImg}" alt="image" class="img-circle" />
							</div>

							<h4>${storeName}</h4>
							<p class="user-role">${ownerName}(${ownerId})</p>

						</div>
						
						<div class="admin-user-action text-center">
							<a href="updateStoreForm.do" class="btn btn-primary btn-sm btn-inverse">프로필 수정</a>
						</div>


						<ul class="admin-user-menu clearfix">
							<li><a href="storeProfile.do"><i class="fa fa-user"></i> 프로필</a></li>
							<li><a href="ownerMyPage.do"><i class="fa fa-key"></i>메뉴등록</a></li>
							<li><a href="char.do"><i class="fa fa-sign-out"></i> 판매량</a></li>
							<li class="active"><a href="reservList.do"><i class="fa fa-sign-out"></i> 예약관리</a></li>
							<li><a href="logout.do"><i class="fa fa-sign-out"></i> Logout</a></li>
						</ul>

					</div>

				</div>

				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">

						<div class="admin-section-title">

							<h2>예약관리</h2>
							<p>예약을 승인해주세요</p>

						</div>

						<div id="show">
							<table id="tb" class="table">
								<thead>
									<tr>
										<th>주문번호</th>
										<th>회원아이디</th>
										<th>오너아이디</th>
										<th>인원</th>
										<th>예약일</th>
										<th>예약시간</th>
										<th>요청사항</th>
										<th>예약상태</th>
										<th>예약승인</th>
									</tr>
								</thead>

								<tbody id="body">

								</tbody>
							</table>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script>
function reservMod(){
	let tr = this.parentElement.parentElement;
	let orderNum = tr.children[0].textContent;
	let  = tr.children[7].firstChild.value;
	
	fetch("ownerReservAllowMod.do?orderNum="+orderNum,{
		method: 'get'
	})
	.then(resolve=>resolve.json())
	.then(result=>{
		console.log('와이')
		console.log(result)
		if(result.retCode == 'Success'){
			alert('정상적으로 처리 완료');
			console.log(result.reservInfo)
			let newTr = makeTr(result.reservInfo)
			
			tr.replaceWith(newTr);
		}else if(result.retCode == 'Fail'){
			alert('에러발생');
		}
	})
	.catch(reject=>{
		console.log(reject)
	})
}
	let columns = ['orderNum','memId','ownerId','personCount','reserveDate','reserveTime','request','ownerAllow']
	function makeTr(reserv={}){
		
		let tr = document.createElement('tr');
		if(reserv.ownerId == '${ownerId}'){	
			
			for(let column of columns){
				
				let td = document.createElement('td');
				td.innerText = reserv[column];
				tr.append(td);
				console.log(td)
			}
			
			
		//승인버튼
		td = document.createElement('td');
		let btn = document.createElement('button');
		btn.addEventListener('click',reservMod);
		if(reserv['ownerAllow']=='A'){
			btn.innerText='승인완료';
			btn.setAttribute("class","btn btn-primary")	
			btn.setAttribute("disabled","")
		}else{
			btn.innerText='승인대기';
			btn.setAttribute("class","btn btn-primary")
		}
		td.append(btn);//<td><button>삭제</td ...
		tr.append(td);
		}
		return tr; //함수 호출한 영역으로 값을 반환함
	}
	
	
	
	
	fetch('ownerReservList.do') 
	.then(resolve=> resolve.json())
	.then(showReservList)
	.catch(function(reject){
		console.log(reject);
	})
	
	function showReservList(result){
		console.log(result);
		//table 생성
			let tbody = document.querySelector('#body')
			result.forEach(function(item,idx,ary){
				
					console.log(item.ownerId)
					let tr = makeTr(item)
					tbody.append(tr)
				
			})
	}
</script>