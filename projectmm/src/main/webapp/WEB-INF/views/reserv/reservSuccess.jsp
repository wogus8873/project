<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
.section.sm {
	margin-top: 80px;
	margin-bottom: 80px;
}

</style>
    		<!-- 여기서부터 -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start hero-header -->
			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list booking-step">
						<li><a href="homepage.html">홈</a></li>
						<li><span>예약 성공</span></li>
					</ol>
					
				</div>
				
			</div>
			<!-- end hero-header -->
			
			<div class="section sm">

				<div class="cfo-area">
					<div class="container">
						<div class="row">

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="contact-details thankyou text-center clearfix">
									<span><i class="fa fa-check" aria-hidden="true"></i></span>
									<h2>🥂예약해 주셔서 감사합니다.🥂</h2>
									<div class="font-icon-list col-md-2 col-sm-3 col-xs-6 col-xs-6">

									</div>
									<div class="reservInfo" id="reservInfo">
									<br>
										<input type = "hidden" id ="memId" value = "${id}">
										<input type = "hidden" id ="ownerId" value = "${ownerIid}">
										<b><span class="storeName" id="storeName">가게이름</span></b>
										<span> 예약 되었습니다.</span>
										<br>
										<span>예약날짜: </span>
										<b><span class="reservDate" id="reservDate">예약날짜</span></b>
										<br>
										<span>예약시간: </span>
										<b><span class="reservTime" id="reservTime">예약시간</span></b>
										<br>
										<span>인원수: </span>
										<b><span class="personCount" id="personCount">인원수</span></b>
									</div>
									<br>
									<div class="clearfix">
									<a href="memberProfile.do" class="btn btn-primary animation"type='button' id="reservDetail">
									<span class="price-new" id="menu">상세 정보 조회</span></a>
									<a href="main.do" class="btn btn-primary animation"type='button' id="addCartbtn">
									<span class="price-new" id="menu">홈으로</span></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<!-- END OF CONTACT DETAILS SECTION -->

							</div>
							<!-- End of CFO order list -->


						</div>
					</div>
				</div>
			
			</div>

		</div>
		<!-- 여기까지 -->
		
<script src="js/reservSuccessfetch.js"></script>