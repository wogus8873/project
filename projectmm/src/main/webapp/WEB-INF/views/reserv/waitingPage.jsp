<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
.section.sm {
	margin-top: 80px;
	margin-bottom: 80px;
}

.cancle {
    background: #fff;
    border-color: #e7604a;
    color: #000 !important;
}
</style>
    		<!-- 여기서부터 -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start hero-header -->
			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list booking-step">
						<li><a href="homepage.html">홈</a></li>
						<li><span>대기번호 발급</span></li>
					</ol>
					
				</div>
				
			</div>
			<!-- end hero-header -->
			
			<div class="section sm">

				<div class="cfo-area">
					<div class="container">
						<div class="row">

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="contact-details thankyou text-center clearfix">
									<span><i class="fa fa-check" aria-hidden="true"></i></span>
									<h2>대기하실 인원을 선택하세요</h2>
									<div class="font-icon-list col-md-2 col-sm-3 col-xs-6 col-xs-6">
									</div>
									<h4>인원</h4>
									<div class="reservInfo" id="reservInfo">
										<input type="hidden" class="memId" id="memId" value="${id}">
										<a title="Add a product" id="plusbtn" href="#" type='button' onclick="return false;">
										<i class="fa fa-plus-circle"></i>
										</a>
										<b><span class='peopleNumber' id='result'>1</span></b>
										<a title="Minus a product" id='minusbtn' href="#" type='button' onclick="return false;">
										<i class="fa fa-minus-circle"></i>
										</a>
										<br>
									</div>
									<br>
									<div class="clearfix">
									<a href="listStoreForm.do" class="btn btn-primary cancle animation"type='button' id="addCartbtn">
									<span class="price-new" id="menu">취소</span></a>
									<a href="#" class="btn btn-primary animation"type='button' id="addWaitingbtn">
									<span class="price-new" id="menu">대기신청하기</span></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<!-- END OF CONTACT DETAILS SECTION -->

							</div>
							<!-- End of CFO order list -->


						</div>
					</div>
				</div>
			
			</div>

		</div>
		<!-- 여기까지 -->
		
<script src="js/waitingPage.js"></script>