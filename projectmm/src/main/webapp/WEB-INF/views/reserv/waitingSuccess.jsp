<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
.section.sm {
	margin-top: 80px;
	margin-bottom: 80px;
}

</style>
    		<!-- 여기서부터 -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start hero-header -->
			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list booking-step">
						<li><a href="homepage.html">홈</a></li>
						<li><span>예약 성공</span></li>
					</ol>
					
				</div>
				
			</div>
			<!-- end hero-header -->
			
			<div class="section sm">

				<div class="cfo-area">
					<div class="container">
						<div class="row">

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="contact-details thankyou text-center clearfix">
									<span><i class="fa fa-check" aria-hidden="true"></i></span>
									<h2>🌻대기열에 등록 되었습니다.🌻</h2>
									<h1 class = "waitingnumber" id="waitingnumber">3</h1>
									<div class="font-icon-list col-md-2 col-sm-3 col-xs-6 col-xs-6">
									</div>
									<div class="reservInfo" id="reservInfo">
									<br>
										<input type = "hidden" id ="memId" value = "${id}">
										<span> 마이페이지에서 실시간으로 대기 순서를 조회할 수 있습니다.</span>
										<br>
										<b><span>입장 순서가 되었을 때 매장 앞에 계시지 않을 경우 웨이팅이 취소 될 수 있습니다.</span></b>
										<br>
										<span>가게에 입장하신 이후 직원의 확인이 필요합니다.</span>
									</div>
									<br>
									<div class="clearfix">
									<a href="memberProfile.do" class="btn btn-primary animation"type='button' id="addCartbtn">
									<span class="price-new" id="menu">마이페이지</span></a>
									<a href="main.do" class="btn btn-primary animation"type='button' id="addCartbtn">
									<span class="price-new" id="menu">홈으로</span></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<!-- END OF CONTACT DETAILS SECTION -->

							</div>
							<!-- End of CFO order list -->


						</div>
					</div>
				</div>
			
			</div>

		</div>
		<!-- 여기까지 -->
		
<script src="js/waitingSuccessfetch.js"></script>