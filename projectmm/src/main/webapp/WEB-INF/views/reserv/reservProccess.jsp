<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<div class="section sm">

	<!-- Content Start -->
	<!-- Start order-online-->
	<div id="order-process">
		<div class="container">
			<div class="order-inner-content">
				<div class="row">
					<div class="col-md-9">
						<div class="order-process-left-side">
							<div class="order-information row">
								<div class="col-md-6">
									<h4 class="section-title pb-10">예약 정보</h4>
									<ul class="list-unstyled info-list">
										<li><input class="custom-radio-big" type="radio"
											name="collection" id="collection" checked=""> <label
											for="collection">예약 시간 15분이 지나면 노쇼처리 됩니다.</label></li>
										<li>
											<div class="row">
												<div class="col-md-6 col-xs-6 col-sm-6">
													<input class="custom-radio-big" type="radio"
														name="delivery-priority" id="asap" checked=""> <label
														for="asap">아동포함</label>
												</div>
												<div class="col-md-6 col-xs-6 col-sm-6">
													<input class="custom-radio-big" type="radio"
														name="delivery-priority" id="later"> <label
														for="later">아동미포함</label>
												</div>
											</div>
											<div class="row order-time-options">
												<div class="col-md-6">
													<select name="reservDate" id="order-delivery-day">
														<option value="">날짜를 고르세요</option>
														<option value="day1">2023-01-12</option>
														<option value="day2">2023-01-13</option>
														<option value="day3">2023-01-14</option>
														<option value="day4">2023-01-15</option>
														<option value="day5">2023-01-16</option>
														<option value="day6">2023-01-17</option>
														<option value="day7">2023-01-18</option>
													</select>
												</div>
												<div class="col-md-6">
													<select name="reservTime" id="order-delivery-time">
														<option value="">시간을 고르세요</option>
														<option value="10:30"
															${vo.reservTime == '10:30' ? 'selected' : '' }>10:30</option>
														<option value="11:30"
															${vo.reservTime == '11:30' ? 'selected' : '' }>11:30</option>
														<option value="12:00"
															${vo.reservTime == '12:00' ? 'selected' : '' }>12:00</option>
														<option value="12:30"
															${vo.reservTime == '12:30' ? 'selected' : '' }>12:30</option>
														<option value="13:00"
															${vo.reservTime == '13:15' ? 'selected' : '' }>13:15</option>
														<option value="14:00"
															${vo.reservTime == '14:00' ? 'selected' : '' }>14:00</option>
														<option value="15:00"
															${vo.reservTime == '15:00' ? 'selected' : '' }>15:00</option>
														<option value="16:00"
															${vo.reservTime == '16:00' ? 'selected' : '' }>16:00</option>
														<option value="17:00">17:00</option>
														<option value="18:00">18:00</option>
														<option value="18:30">18:30</option>
														<option value="19:00">19:00</option>
														<option value="19:30">19:30</option>
														<option value="20:00">20:00</option>
														<option value="20:30">20:30</option>
													</select>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<h4 class="section-title pb-10 mb-15">요청 사항</h4>
									<textarea class="special-request" name="special-request"
										id="special-request" placeholder="300자 이내 입력"></textarea>
								</div>
							</div>
							<div class="row order-delivery-address">
								<div class="col-md-6">
									<h4 class="section-title pb-10 mb-15">인원 수</h4>
									<ul class="list-unstyled">
										<li><input type="text" class="text" name="personCount"
											value="${personCount}" placeholder="인원 수를 입력하세요(숫자만 입력)"></li>
									</ul>
								</div>
								<div class="col-md-6">
									<h4 class="section-title pb-10 mb-15">추가사항</h4>
									<div class="postcode"></div>
								</div>
							</div>
							<div class="row order-payment-method">
								<div class="col-md-12">
									<h4 class="section-title pb-10">결제 수단</h4>
									<ul class="list-inline payment-method-options">
										<li><input class="custom-radio-big" type="radio"
											name="payment-option" id="method-cash"> <label
											for="method-cash">일반카드결제</label></li>
										<li><input class="custom-radio-big" type="radio"
											name="payment-option" id="method-paypal"> <label
											for="method-paypal">카카오톡 결제</label></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--.container-->
</div>
<!--#order-online-->
<!--end order-online-->
<!-- Content End -->



<script src="js/reservFetch.js"></script>