<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.admin-content-wrapper {
	margin-top: 80px;
	margin-bottom: 80px;
}

.storeName {
	font-size: 20px;

}

.btn.btn-primary.animation {
	margin-bottom: 5px;
}
</style>
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>회원정보</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/member/${img}" alt="image" class="img-circle" />
							</div>

							<h4>${name}</h4>
							<p class="user-role">${id}</p>

						</div>

						<div class="admin-user-action text-center">
							<a href="updateMember.do"
								class="btn btn-primary btn-sm btn-inverse">정보수정</a>
						</div>

						<ul class="admin-user-menu clearfix">
							<li class="active"><a href="memberProfile.do"><i
									class="fa fa-user"></i> 회원정보</a></li>
							<li><a href="waitingReport.do"><i class="fa fa-key"></i>
									대기내역</a></li>
							<li><a href="reservReport.do"><i class="fa fa-bookmark"></i>
									예약내역</a></li>
							<li><a href="favoriteStore.do"><i
									class="fa fa-tachometer"></i> 찜목록</a></li>
							<li><a onclick="withdrawal()"><i class="fa fa-sign-out"></i>
									회원탈퇴</a></li>
						</ul>

					</div>

				</div>

				<script>
					function withdrawal() {
						if (!confirm("정말 탈퇴 하시겠습니까?")) {
							alert("취소하셨습니다. 함께해주셔서 감사합니다 ^^ ");
						} else {

							location.href = "withdrawal.do";
							alert("탈퇴완료.");

						}
					}
				</script>
				<div class="GridLex-col-9_sm-8_xs-12">
					<div class="admin-content-wrapper">
						<div class="cfo-area">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="contact-details thankyou text-center clearfix">
									<span><i class="fa fa-check" aria-hidden="true"></i></span>
									<h2>🌻대기 조회🌻</h2>
									<h1 class = "waitingnumber" id="waitingnumber">3</h1>
									<div class="font-icon-list col-md-2 col-sm-3 col-xs-6 col-xs-6">
									</div>
									<div class="reservInfo" id="reservInfo">
									<br>
										<input type = "hidden" id ="memId" value = "${id}">
										<span class ="storeName" id ="storeName"> 가게 이름 </span>
										<br>
										<span id = "storeLocation">가게 위치</span>
										<br>
										<br>
										<b><span>입장 순서가 되었을 때 매장 앞에 계시지 않을 경우 웨이팅이 취소 될 수 있습니다.</span></b>
										<br>
										<span>가게에 입장하신 이후 직원의 확인이 필요합니다.</span>
										<span>새로고침 버튼으로 실시간 번호 조회가 가능합니다.</span>

									</div>
									<br>
									<div class="clearfix">
									<a href="#" class="btn btn-primary animation" id="reload" onclick="return false">
									<span class="price-new" id="reload">새로고침</span></a>
									<a href="#" class="btn btn-primary animation">
									<span class="price-new" id="waitingCancel">대기 취소</span></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<!-- END OF CONTACT DETAILS SECTION -->

							</div>
							<!-- End of CFO order list -->


							</div>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>
</div>
<script src="js/waitingDetailFetch.js"></script>