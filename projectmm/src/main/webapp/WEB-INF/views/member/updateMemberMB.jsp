<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="breadcrumb-wrapper">



	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>회원정보수정</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/member/${img}" alt="image"
									class="img-circle" />
							</div>

							<h4>${name}</h4>
							<p class="user-role">${id}</p>

						</div>


						
						<ul class="admin-user-menu clearfix">
							<li><a href="memberProfile.do"><i
									class="fa fa-user"></i> 회원정보</a></li>
							<li class="active"><a href="updateMember.do"><i
									class="fa fa-user"></i> 회원정보수정</a></li>
							<li><a href="waitingReport.do"><i class="fa fa-key"></i>
									대기내역</a></li>
							<li><a href="reservReport.do"><i class="fa fa-bookmark"></i>
									예약내역</a></li>
							<li><a href="favoriteStore.do"><i
									class="fa fa-tachometer"></i> 찜목록</a></li>
							<li><a onclick="withdrawal()"><i class="fa fa-sign-out"></i>
									회원탈퇴</a></li>
						</ul>

					</div>

				</div>



				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">

						<div class="admin-section-title">

							<h2>회원정보 수정</h2>


						</div>

						<form id = "memInfo" class="post-form-wrapper" onsubmit="return false" enctype="multipart/form-data">

							<div class="row gap-20">

							

								<div class="col-sm-6 col-md-4">
									<div class="form-group">
										<input type="hidden" class="form-control" id="memId"
											value="${id}" >
									</div>

									<div class="form-group bootstrap-fileinput-style-01">
										<label for="form-register-photo-2">프로필 사진</label> <input
											type="file" name="form-register-photo-2"
											class="form-register-photo-2" id="storeImage">

									</div>
									<div class="form-group">
									    <label>아이디</label> <input type="text" readonly="readonly" class="form-control" 
											name ="id" value="${id}">
									</div>
									
									<div class="form-group">
										<label>비밀번호</label> <input type="text"
											class="form-control" id="memPw" name="memPw" value="${pw}" >
									</div>

								</div>

								<div class="clear"></div>

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>전화번호</label> <input type="text" class="form-control"
											id="memPhone" name="memPhone" value="${phone}" >
									</div>

								</div>

								<div class="clear"></div>

								<div class="col-sm-6 col-md-4">

									<div class="form-group">
										<label>카드번호</label> <input type="text" class="form-control"
											id="creditNum" name="creditNum" value="${credit}">
									</div>

								</div>

								
							
								<div class="clear"></div>
								

								<div class="col-sm-6 col-md-4">





									<div class="col-sm-12 mt-10">
										<button class="btn btn-primary" id="modBtn">수정</button>
										<button type="reset" class="btn btn-warning">취소</button>
									</div>
					

								</div>
						</form>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script src="js/updateMemberFetch.js"></script>
