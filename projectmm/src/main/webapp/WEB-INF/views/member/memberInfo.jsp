<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>프로필</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/member/${img}" alt="image"
									class="img-circle" />
							</div>

							<h4>${name}</h4>
							<p class="user-role">${id}</p>

						</div>

						<div class="admin-user-action text-center">
							<a href="updateMember.do" class="btn btn-primary btn-sm btn-inverse">프로필 수정</a>
						</div>
						
						<ul class="admin-user-menu clearfix">
							<li class="active"><a href="memberProfile.do"><i
									class="fa fa-user"></i> 프로필</a></li>
							<li><a href="waitingReport.do"><i class="fa fa-key"></i>
									대기내역</a></li>
							<li><a href="reservReport.do"><i class="fa fa-bookmark"></i>
									예약내역</a></li>
							<li><a href="favoriteStore.do"><i
									class="fa fa-tachometer"></i> 찜목록</a></li>
							<li><a onclick="withdrawal()"><i class="fa fa-sign-out"></i>
									회원탈퇴</a></li>
						</ul>
					</div>

				</div>

				<script>
					
					function withdrawal(){
						if(!confirm("정말 탈퇴 하시겠습니까?")){
				           alert("취소하셨습니다. 함께해주셔서 감사합니다 ^^ ");
				         }else {
				            
				        	 location.href="withdrawal.do";
				             alert("탈퇴완료.");
				        
				        }
					 }
					
					
					
					</script>
				


				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">
<input type="hidden" class="form-control" id="memId"
											value="${id}" >
						<div class="admin-section-title">

							<h2>회원정보</h2>


						</div>


</p>
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>아이디</a> : </strong> <span>${id}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>이름</a> : </strong> <span>${name}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>휴대폰 번호</a> : </strong> <span>${phone}</span></li>
                  <br>
                  <li><i class="bi bi-chevron-right"></i> <strong><a>카드 번호</a> : </strong> <span>${credit}</span></li>
                </ul>
                
              </div>
              
            </div>
            <p>



					</div>

				</div>

			</div>

		</div>

	</div>

</div>


