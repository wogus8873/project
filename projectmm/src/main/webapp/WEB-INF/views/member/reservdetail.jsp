<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.admin-content-wrapper {
	margin-top: 80px;
	margin-bottom: 80px;
}

.btn.btn-primary.animation {
	margin-bottom: 5px;
}

</style>
<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>회원정보</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/member/${img}" alt="image" class="img-circle" />
							</div>

							<h4>${name}</h4>
							<p class="user-role">${id}</p>

						</div>

						<div class="admin-user-action text-center">
							<a href="updateMember.do"
								class="btn btn-primary btn-sm btn-inverse">정보수정</a>
						</div>

						<ul class="admin-user-menu clearfix">
							<li class="active"><a href="memberProfile.do"><i
									class="fa fa-user"></i> 회원정보</a></li>
							<li><a href="waitingReport.do"><i class="fa fa-key"></i>
									대기내역</a></li>
							<li><a href="reservReport.do"><i class="fa fa-bookmark"></i>
									예약내역</a></li>
							<li><a href="favoriteStore.do"><i
									class="fa fa-tachometer"></i> 찜목록</a></li>
							<li><a onclick="withdrawal()"><i class="fa fa-sign-out"></i>
									회원탈퇴</a></li>
						</ul>

					</div>

				</div>

				<script>
					function withdrawal() {
						if (!confirm("정말 탈퇴 하시겠습니까?")) {
							alert("취소하셨습니다. 함께해주셔서 감사합니다 ^^ ");
						} else {

							location.href = "withdrawal.do";
							alert("탈퇴완료.");

						}
					}
				</script>



				<div class="GridLex-col-9_sm-8_xs-12">
					<div class="admin-content-wrapper">
						<div class="cfo-area">
							<div class="row">

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="contact-details thankyou text-center clearfix">
										<span><i class="fa fa-check" aria-hidden="true"></i></span>
										<h2>🥂예약 정보🥂</h2>
										<div class="font-icon-list col-md-2 col-sm-3col-xs-6col-xs-6">
										</div>
										<div class="reservInfo" id="reservInfo">
											<br> <input type="hidden" id="memId" value="${id}">
											<input type="hidden" id="ownerId" value="${ownerIid}">
											<b> <span class="storeName" id="storeName">가게이름</span></b>
											<span>예약 되었습니다.</span>
											<br>
											<span>예약날짜: </span>
											<b><span class="reservDate" id="reservDate">예약날짜</span></b>
											<br>
											<span>예약시간:</span> 
											<b><span class="reservTime" id="reservTime">예약시간</span></b>
											<br>
											<span>인원수: </span>
											<b><span class="personCount"id="personCount">인원수</span></b>
											<span>명</span>
										</div>
										<br>
										<h2></h2>
										<div class="menuListparent">
										<h2>메뉴 리스트</h2>
											<div class="menuList">
												<b><span id="menuName" class="menuName"> 메뉴이름 </span></b>
												<b><span id="menuCount" class="menuCount"> 수량 </span></b><span>개</span>
												<b><span id="totalMenu" class="totalMenu"> 총 가격 </span></b><span>원</span>
											</div>
										</div>
										<br>
										<div id="totalPrice">
											<h4 id="totalMenu" class="totalMenu"> 합계: <b><span id="ttprice" class="ttprice"> 합계 </span></b><span>원</span></h4>
										</div>
									</div>
									<div class="clearfix"></div>
									<!-- END OF CONTACT DETAILS SECTION -->

								</div>
								<!-- End of CFO order list -->


							</div>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>
</div>
<script src="js/reservDetailFetch.js"></script>