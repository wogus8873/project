<%@page import="jjim.vo.JjimVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>찜목록</title>
</head>
<body>
	<div class="main-wrapper">

		<!-- start breadcrumb -->
		<div class="breadcrumb-wrapper">

			<div class="container">

				<ol class="breadcrumb-list booking-step">
					<li><a href="index.html">홈</a></li>
					<li><span>찜목록</span></li>
				</ol>

			</div>

		</div>
		<!-- end breadcrumb -->
		<%
		String id = (String) session.getAttribute("id");
		String pw = (String) session.getAttribute("pw");
		String grade = (String) session.getAttribute("grade");
		String name = (String) session.getAttribute("name");
		String img = (String) session.getAttribute("img");
		String phone = (String) session.getAttribute("phone");
		%>


	</div>

	<div class="admin-container-wrapper">

		<div class="container">

			<div class="GridLex-gap-15-wrappper">

				<div class="GridLex-grid-noGutter-equalHeight">

					<div class="GridLex-col-3_sm-4_xs-12">

						<div class="admin-sidebar">

							<div class="admin-user-item">

								<div class="image">
									<img src="images/member/${img}" alt="image" class="img-circle" />
								</div>

								<h4>${name}</h4>
								<p class="user-role">${id}</p>

							</div>

							<div class="admin-user-action text-center">
								<a href="updateMember.do"
									class="btn btn-primary btn-sm btn-inverse">프로필 수정</a>
							</div>

							<ul class="admin-user-menu clearfix">

								<li><a href="memberProfile.do"><i class="fa fa-user"></i>
										프로필</a></li>
								<li><a href="waitingReport.do"><i class="fa fa-key"></i>
										대기내역</a></li>
								<li><a href="reservReport.do"><i class="fa fa-bookmark"></i>
										예약내역</a></li>
								<li class="active"><a href="favoriteStore.do"><i
										class="fa fa-tachometer"></i> 찜목록</a></li>
								<li><a onclick="withdrawal()"><i class="fa fa-sign-out"></i>
										회원탈퇴</a></li>
							</ul>

						</div>

					</div>


					<div class="GridLex-col-9_sm-8_xs-12">

						<div class="admin-content-wrapper">

							<div class="admin-section-title">

								<h2>찜목록</h2>

							</div>

							<div class="restaurant-item-grid-wrapper">

								<div class="GridLex-gap-30">

									<div class="GridLex-grid-noGutter-equalHeight">
										
										<script type="text/javascript">
										console.log(${listJjim })
										</script>
										
										<!-- 시작 -->
										
										<c:forEach var="vo" items="${listJjim }">
										<div class="GridLex-col-4_sm-6_xs-6_xss-12">

											<div class="restaurant-item-grid">

												<div class="fav-save">

													<a href="#"><i class="fa fa-heart text-danger"></i></a>

												</div>
												<div class="labeling">
													<span class="label label-success">Get Offer</span>
												</div>

													<div class="content">
														<!-- 가게이름 -->
														<h4 class="heading">${vo.STORE_NAME}</h4>
														<!-- 장소 -->
														<p class="location">
															<i class="fa fa-map-marker text-primary"></i> <strong
																class="text-primary">${vo.STORE_LOCATION}</strong> - Korea
														</p>
														<p class="date text-muted font12 font-italic"></p>
													</div>

												<div class="content-bottom">
													<div class="sub-category">
														<a href="#">Place order</a>
													</div>
												</div>

											</div>

										</div>
										</c:forEach>
										<!-- 끝 -->

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</body>
</html>