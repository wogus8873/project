<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="error-page-wrapper">
		
				<div class="container">

					<div class="row">
					
							<!-- login container -->

							<div class="login-container">
								<!-- Combined Form Content -->
								<div class="login-container-content">
									<ul class="nav nav-tabs nav-justified">
										<li class="active link-one"><a href="#login-block" data-toggle="tab"><i class="fa fa-sign-in"></i><br>로그인</a></li>
										<li class="link-two"><a href="#register-block" data-toggle="tab"><i class="fa fa-pencil"></i>손님<br>회원가입</a></li>
										<li class="link-three"><a href="#contact-block" data-toggle="tab"><i class="fa fa-pencil"></i>점주<br>회원가입</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active fade in" id="login-block">
											<!-- Login Block Form -->
											<div class="login-block-form">
												<!-- Heading -->
												<h4>아이디와 비밀번호를 입력하세요.</h4>
												<!-- Border -->
												<div class="bor bg-orange"></div>
												
												
												
												
												
												
												
												<!-- Form -->
												<form action="loginCheck.do" onsubmit="return checkForm()" name="loginfm" class="form" role="form" >
													<!-- Form Group -->
													<div class="form-group">
														<!-- Label -->
														<label class="control-label">아이디</label>
														<!-- Input -->
														<input name="id" type="text" class="form-control" placeholder="아이디를 입력하세요.">
													</div>
													<div class="form-group">
														<label class="control-label">비밀번호</label>
														<input name="pw" type="password" class="form-control" placeholder="비밀번호를 입력하세요.">
													</div>
													<div class="form-group">
														<div class="checkbox">
															<label>
																<input type="checkbox"> 기억하시겠습니까?
															</label>
														</div>
													</div>
													<div class="form-group">
														<!-- Button -->
														<button type="submit" class="btn btn-primary">로그인</button>
													
													</div>
													<div class="form-group">
														<a href="findPwForm.do" class="black">아이디 / 비밀번호 찾기</a>
													</div>
												</form>
												
												<script>
												function checkForm(){
													let id = document.loginfm.id;
													let pw = document.loginfm.pw;
													
													if(!id.value && !pw.value) {
														alert("아이디와 비밀번호를 입력하세요.");
														id.focus();
														return false;
													}else if(!id.value){
														alert("아이디를 입력하세요");
														id.focus();
														return false;	
													}else if(!pw.value){
														alert("비밀번호를 입력하세요")
														pw.focus();
														return false;
													}
														submit();	
												}
												
											</script>	
											
											
											</div>
										</div>
										<div  class="tab-pane fade" id="register-block">
											<div class="register-block-form">
												<!-- Heading -->
												<h4>Create the New Account</h4>
												<!-- Border -->
												<div class="bor bg-orange"></div>
												
												
												<!-- 유저 회원가입 Form
												
												onsubmit="return submitCheck()"  -->
												
												
												
												<form action="submitMember.do"  onsubmit="return submitMemForm()" name="userfm" class="form" role="form">
													<!-- Form Group -->
													<div class="form-group">
														<!-- Label -->
														<label class="control-label">아이디</label>
														<!-- Input -->
														<div style="position: relative;">
														

														<input type="text" name="userid" class="form-control" onkeydown="inputIdChk()" placeholder="아이디를 입력하세요.">
														<input value="중복확인"  onclick="openidCheck()" type="button"  style="position: absolute; right: 7px; top:3px;">
														<input name="idDuplication" type="hidden" value="idUncheck">

									
														
														</div>
														
														</div>
													<div class="form-group">
														<label class="control-label">비밀번호</label>
														<input name="userpw" type="password" class="form-control" placeholder="비밀번호를 입력하세요">
													</div>
													<div class="form-group">
														<input name="userpwcheck" type="password" class="form-control" placeholder="비밀번호를 한번 더 입력하세요">
													</div>
													<div class="form-group">
														<label class="control-label">이름</label>
														<input name="username" type="text" class="form-control" placeholder="이름을 입력하세요">
													</div>
													<div class="form-group">
														<label class="control-label">전화번호</label>
														<input name="userphone" type="text" class="form-control" placeholder="전화번호를 입력하세요">
													</div>
													
													<div class="form-group">

														<label class="control-label">카드번호</label>
														<input name="creditnum" type="text" class="form-control" placeholder="카드번호를 입력하세요">

													</div>
													
													<div class="form-group">
														<!-- Checkbox -->
														<div class="checkbox">
															<label>
																<input type="checkbox"> By register, I read & accept  <a href="#">the terms</a>
															</label>
														</div>
													</div>
													<div class="form-group">
														<!-- Button -->
														<button type="submit" class="btn btn-primary">회원가입</button>
													</div>
												</form>
												</div>
												</div>
												
												<script>
													function openidCheck(){
														
														window.name="parentForm";
														var popupWidth = 500;
														var popupHeight = 500;

														var popupX = (window.screen.width / 2) - (popupWidth / 2);
														// 만들 팝업창 width 크기의 1/2 만큼 보정값으로 빼주었음

														var popupY= (window.screen.height / 2) - (popupHeight / 2);

														window.open('idCheckForm.do','chkForm' ,'status=no, height=' + popupHeight  + ', width=' + popupWidth  + ', left='+ popupX + ', top='+ popupY);
													 }
													
													
													 function inputIdChk(){
												            document.userfm.idDuplication.value ="idUncheck";
												      }
													 	
													 
													 function submitMemForm(){
														 
														 let form = document.userfm;
														 
														 if(!form.userid.value){
															 alert("아이디를 입력 해주세요");
															 form.userid.focus();
															 return false;
															 }
														 
														 if(!form.userpw.value){
															 alert("비밀번호를 입력해주세요");
															 form.userpw.focus();
															 return false;
															 
														 }
														 if(!form.userpwcheck.value){
															 alert("비밀번호 확인을 해 주세요");
															 form.userpwcheck.focus();
															 return false;
														 }
														 
														 if(form.userpw.value != form.userpwcheck.value){
															 alert("비밀번호를 동일하게 입력해주세요");
															 form.uwerpw.focus();
															 return false;
														 }
														
														 if(!form.username.value){
															 alert("이름을 입력해주세요");
															 form.username.focus();
															 return false;
														 } 
														 if(!form.userphone.value){
															 alert("전화번호를 입력해주세요");
															 form.userphone.focus();
															 return false;
														 }
														 if(isNaN(form.userphone.value)){
															 alert("전화번호는 숫자만 입력해주세요.");
															 form.userphone.focus();
															 return false;
														 }
														 if(!form.creditnum.value){
															 alert("카드번호를 입력해주세요");
															 form.creditnum.focus();
															 return false;															 
														 }
													 }
												
													
													 </script>
												
												
												
												
												
											<div class="tab-pane fade" id="contact-block">
												<!-- Contact Block Form -->
												<div class="register-block-form">
												<!-- Heading -->
												<h4>Create the New Account</h4>
												<!-- Border -->
												<div class="bor bg-orange"></div>
												
												
												
												
												
												<!-- Form -->
												<form action="submitStoreMember.do" onsubmit="return submitOwnerForm()" class="form" role="form" name="storefm">
													
													
													
													<!-- Form Group -->
													<div class="form-group">
														<!-- Label -->
														<label class="control-label">아이디</label>
														<!-- Input -->
														<div style="position: relative;">
														
														<input name="ownerid" type="text" class="form-control"  placeholder="아이디를 입력하세요">
														
														
														<input value="중복확인"  onclick="openOwnerIdCheck()" type="button"  style="position: absolute; right: 7px; top:3px;">
														<input name="idDuplication2" type="hidden" value="idUncheck">
														</div>
														
														</div>
													<div class="form-group">
														<label class="control-label">비밀번호</label>
														<input name="ownerpw" type="text" class="form-control" placeholder="비밀번호를 입력하세요">
													</div>
													<div class="form-group">
														<label class="control-label">이름</label>
														<input name = "ownername" type="text" class="form-control" placeholder="이름을 입력하세요">
													</div>
													<div class="form-group">
														<label class="control-label">핸드폰번호</label>
														<input name= "ownerphone" type="text" class="form-control" placeholder="핸드폰번호를 입력하세요">
													</div>
													<div class="form-group">
														<label class="control-label">사업자번호</label>
														<input name= "biznum" type="text" class="form-control" placeholder="사업자번호 입력하세요">
													</div>
													 <div class="form-group">
													<label class="control-label">영업카테고리를 골라주세요</label>
													<td valign="top">
														<select class="form-control" name="category">
															<option value="한식">한식</option>
															<option value="중식">중식</option>
															<option value="일식">일식</option>
															<option value="양식">양식</option>
															<option value="기타">기타</option>
														</select>
													</td>
													</div>
													<div class="form-group">
														<label class="control-label">가게이름</label>
														<input name= "storename" type="text" class="form-control" placeholder="가게이름을 입력하세요">
													</div>
												    <div class="form-group">
														<label class="control-label">가게위치</label>
														<input name= "storelocation" id="storelocation" type="text" class="form-control" placeholder="가게위치를 입력하세요">
													</div>
													<div class="form-group">
														<label class="control-label">영업 시작시간</label>
														<input name= "storetime" type="text" class="form-control" placeholder="영업시작시간을 입력하세요(00:00~00:00)">
													</div>
													<div class="form-group">
														<label class="control-label">상세설명</label>
														<input name= "storedesc" type="text" class="form-control" placeholder="상세설명을 입력하세요">
													</div>
													
													<div class="form-group">
														
													</div>
													<div class="form-group">
														<!-- Buton -->
														<button type="submit" class="btn btn-primary">회원가입</button>&nbsp;
													<!-- 	<button type="submit" class="btn btn-primary btn-inverse">Reset</button> -->
													</div>
													
													
													
												</form>
												
											</div>
										</div>
										
										<script>
										
										
										
										
										function openOwnerIdCheck(){
											 	window.name="parentForm2";
												var popupWidth = 500;
												var popupHeight = 500;

												var popupX = (window.screen.width / 2) - (popupWidth / 2);
												// 만들 팝업창 width 크기의 1/2 만큼 보정값으로 빼주었음

												var popupY= (window.screen.height / 2) - (popupHeight / 2);

												window.open('ownerIdCheckForm.do','ownerchkForm' ,'status=no, height=' + popupHeight  + ', width=' + popupWidth  + ', left='+ popupX + ', top='+ popupY); 
										}
										 
										
										
										
										 function submitOwnerForm(){
											 
											 let form = document.storefm;
											 
											 if(!form.ownerid.value){
												 alert("아이디를 입력해주세요");
												 form.ownerid.focus();
												 return false;
												 }
											 if(!form.ownerpw.value){
												 alert("비밀번호를 입력해주세요.");
												 form.ownerpw.focus();
												 return false;
											 }
											 if(!form.ownername.value){
												 alert("성함을 입력해주세요.");
												 form.ownername.focus();
												 return false;
											 }
											 if(!form.ownerphone.value){
												 alert("핸드폰번호를 입력해주세요.");
												 form.ownerphone.focus();
												 return false;
											 }
											 if(isNaN(form.ownerphone.value)){
												 alert("핸드폰번호는 숫자만 가능합니다.");
												 form.ownerphone.focus();
												 return false;
											 }
											 
											 if(!form.biznum.value){
												 alert("사업자번호를 입력해주세요.");
												 form.biznum.focus();
												 return false;
											 }
											 if(!form.storename.value){
												 alert("가게이름을 입력해주세요.");
												 form.storename.focus();
												 return false;
											 }
											if(!form.storelocation.value){
												alert("가게 위치를 알려주세요.");
												form.storelocation.focus();
												return false;
											}
											if(!form.storetime.value){
												alert("영업시간을 적어주세요.");
												form.storetime.focus();
												return false;
											}
											if(!form.storedesc.value){
												alert("가게 상세설명을 적어주세요.");
												form.storedesc.focus();
												return false;
											}
											
											submit();	
									
											 
											 
										 }
										
										 
										 
										
										
										
										</script>
										
										
									</div>
								</div>
							</div>
							

					
					</div>
				
				</div>
			
			</div>
			<script src=""></script>