<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="breadcrumb-wrapper">

	<div class="container">

		<ol class="breadcrumb-list booking-step">
			<li><a href="main.do">홈</a></li>
			<li><span>회원목록</span></li>
		</ol>

	</div>

</div>

<div class="admin-container-wrapper">

	<div class="container">

		<div class="GridLex-gap-15-wrappper">

			<div class="GridLex-grid-noGutter-equalHeight">

				<div class="GridLex-col-3_sm-4_xs-12">

					<div class="admin-sidebar">

						<div class="admin-user-item">

							<div class="image">
								<img src="images/member/${img}" alt="image"
									class="img-circle" />
							</div>

							<h4>${name}</h4>
							<p class="user-role">${id}</p>

						</div>



						<ul class="admin-user-menu clearfix">
							<li class="active"><a href="admin.do"><i class="fa fa-user"></i> 회원목록</a></li>
							<li><a href="adminStore.do"><i class="fa fa-key"></i>가게목록</a></li>
							<li><a href="logout.do"><i class="fa fa-sign-out"></i>Logout</a></li>
						</ul>

					</div>

				</div>

				<div class="GridLex-col-9_sm-8_xs-12">

					<div class="admin-content-wrapper">

						<div class="admin-section-title">

							<h2>회원 목록</h2>
							<p>회원 목록 입니다.</p>

						</div>


						<div id="show">
							<table id="tb" class="table">
								<thead>
									<tr>
										<th>아이디</th>
										<th>비밀번호</th>
										<th>이름</th>
										<th>전화번호</th>
										<th>카드번호</th>
										<th>결제횟수</th>
										<th>등급</th>
										<th>삭제</th>
									</tr>
								</thead>

								<tbody id="body">

								</tbody>
							</table>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<script>
/*	
	let btn = this;
	function deleteStore(memId){
		console.log(memId)
		fetch("deleteStore.do?ownerId="+ownerId,{
			method:'get'
		})
		.then(resolve=>resolve.json())
		.then(result=>{
			if(result.retCode == 'Success'){
				alert('정상적으로 처리 완료');
				btn.parentElement.parentElement.remove();//화면에서 바로제거
			}else if(result.retCode == 'Fail'){
				alert('처리중 오류 발생');
			}
		})
		
	}
	*/
	function memberDeleteAjax(){
		console.log(this.parentElement.parentElement.firstChild.innerText); 
		let memId = this.parentElement.parentElement.firstChild.innerText;  //tr 지우려고 자식의 부모의 부모 
		let btn = this;
		//db에서 삭제. 화면에서 제외
		
		fetch("deleteMember.do?memId="+memId,{
			method:'get'
		})
		.then(resolve=>resolve.json())
		.then(result=>{
			if(result.retCode == 'Success'){
				alert('정상적으로 처리 완료');
				btn.parentElement.parentElement.remove();//화면에서 바로제거
			}else if(result.retCode == 'Fail'){
				alert('처리중 오류 발생');
			}
		})
		.catch(reject=>{
			console.log(reject)
		})
	}
	
	
	let columns = ['memId','memPw','memName','memPhone','creditNum','payCount','grade']
	function makeTr(member={}){
		
		let tr = document.createElement('tr');
				
		
			for(let column of columns){
				let td = document.createElement('td');
				td.innerText = member[column];
				tr.append(td);
				console.log(td)
			}
			
		//삭제버튼
		td = document.createElement('td');
		let btn = document.createElement('button');
		btn.addEventListener('click',memberDeleteAjax);
		if(member['grade']=='M'){
			btn.innerText='삭제';
			btn.setAttribute("class","btn btn-primary")	
			btn.setAttribute("disabled","")
		}else{
			btn.innerText='삭제';
			btn.setAttribute("class","btn btn-primary")
		}
		td.append(btn);//<td><button>삭제</td ...
		tr.append(td);
		
		return tr; //함수 호출한 영역으로 값을 반환함
	}
	
	fetch('adminMemberList.do') 
	.then(resolve=> resolve.json())
	.then(showMemberList)
	.catch(function(reject){
		console.log(reject);
	})
	
	function showMemberList(result){
		console.log(result);
		//table 생성
			let tbody = document.querySelector('#body')
			result.forEach(function(item,idx,ary){
				console.log(item)
				let tr = makeTr(item)
				tbody.append(tr)
			})
	}
</script>
 

