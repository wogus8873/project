<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!doctype html>
<html lang="en">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title Of Site -->
	<title>믐믐 - 홈페이지</title>
	<meta name="description" content="TheFoody is a food ordering platform which brings restaurants and food lovers together. Food ordering online is easier than any other platforms.">
	<meta name="keywords" content="food, order online, restaurant, reservation, book a table, foodies, cafe, recipes, menu, dishes, chefs and cooking experts ">
	<meta name="author" content="iglyphic">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="project/images/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="project/images/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="project/images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="project/images/ico/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="project/images/ico/favicon.png">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="project/bootstrap/css/bootstrap.min.css" media="screen">	
	<link href="project/css/animate.css" rel="stylesheet">
	<link href="project/css/main.css" rel="stylesheet">
	<link href="project/css/component.css" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="project/icons/linearicons/style.css">
	<link rel="stylesheet" href="project/icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="project/icons/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" href="project/icons/ionicons/css/ionicons.css">
	<link rel="stylesheet" href="project/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="project/icons/rivolicons/style.css">
	<link rel="stylesheet" href="project/icons/flaticon-line-icon-set/flaticon-line-icon-set.css">
	<link rel="stylesheet" href="project/icons/flaticon-streamline-outline/flaticon-streamline-outline.css">
	<link rel="stylesheet" href="project/icons/flaticon-thick-icons/flaticon-thick.css">
	<link rel="stylesheet" href="project/cons/flaticon-ventures/flaticon-ventures.css">

	<!-- CSS Custom -->
	<link href="project/css/style.css" rel="stylesheet">
	<link href="css/order-online.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>


<body class="home">

	<div id="introLoader" class="introLoading"></div>
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		<header id="header">
			<!-- start Navbar (Header) -->
			<tiles:insertAttribute name="navbar"></tiles:insertAttribute>
			<!-- end Navbar (Header) -->
		</header>
		<!-- end Header -->

		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start hero-header -->
			<tiles:insertAttribute name="hero-header"></tiles:insertAttribute>
			<!-- end hero-header -->

			
			
			

			<!-- start banner section -->
			
			<!-- end banner section -->
			<tiles:insertAttribute name="body"></tiles:insertAttribute>
			

			<!-- Download App Start -->
			
			<!-- Download App End -->


			<!-- start footer -->
			<tiles:insertAttribute name="footer"></tiles:insertAttribute>
			<!-- end footer -->	
			
		</div>
		<!-- end Main Wrapper -->

	</div> <!-- / .wrapper -->
	<!-- end Container Wrapper -->
 
 
<!-- start Back To Top -->
<tiles:insertAttribute name="backToTop"></tiles:insertAttribute>
<!-- end Back To Top -->


<!-- JS -->
<script type="text/javascript" src="project/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="project/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="project/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="project/js/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="project/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="project/js/smoothscroll.js"></script>
<script type="text/javascript" src="project/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="project/js/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="project/js/wow.min.js"></script>
<script type="text/javascript" src="project/js/jquery.slicknav.min.js"></script>
<script type="text/javascript" src="project/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="project/js/bootstrap-tokenfield.js"></script>
<script type="text/javascript" src="project/js/typeahead.bundle.min.js"></script>
<script type="text/javascript" src="project/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="project/js/jquery-filestyle.min.js"></script>
<script type="text/javascript" src="project/js/bootstrap-select.js"></script>
<script type="text/javascript" src="project/js/ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="project/js/handlebars.min.js"></script>
<script type="text/javascript" src="project/js/jquery.countimator.js"></script>
<script type="text/javascript" src="project/js/jquery.countimator.wheel.js"></script>
<script type="text/javascript" src="project/js/slick.min.js"></script>
<script type="text/javascript" src="project/js/easy-ticker.js"></script>
<script type="text/javascript" src="project/js/jquery.introLoader.min.js"></script>
<script type="text/javascript" src="project/js/jquery.responsivegrid.js"></script>
<script type="text/javascript" src="project/js/customs.js"></script>


</body>



</html>