<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<%
String grade = null;
if (session.getAttribute("grade") != null) {
	grade = (String) session.getAttribute("grade");
}

String ownerGrade = null;
if (session.getAttribute("ownerGrade") != null) {
	ownerGrade = (String) session.getAttribute("ownerGrade");
}



if (ownerGrade == null && grade == null) {
	
	
%>


<!-- 로그인 안했을 때  -->



<nav
	class="navbar navbar-default navbar-fixed-top navbar-sticky-function">

	<div class="container">

		<div class="logo-wrapper">
			<div class="logo">
				<a href="main.do"><img src="images/store/flogo.png" alt="Logo" style="width: 135px; height: 50px; margin-right: 0px;"></a>
			</div>
		</div>

		<div id="navbar" class="navbar-nav-wrapper navbar-arrow">
			<!--상단바 시작-->
			<ul class="nav navbar-nav" id="responsive-menu">

				<li><a href="main.do">홈으로</a></li>

				<li><a href="listStoreForm.do">가게</a>
					<ul>
						<li><a href="listStoreForm.do">가게 목록</a></li>
						<li><a href="listTypeStoreForm.do">카테고리</a></li>
						<li><a href="searchMapStore.do">지도에서 가게 찾기</a></li>
					</ul></li>

				<li><a href="calendar.do">이벤트</a></li>
				<li><a href="listQuestion.do">문의사항</a></li>
			</ul>
			<!--상단바 끝-->
		</div>
		<!--/.nav-collapse -->
		<div class="nav-mini-wrapper">
			<ul class="nav-mini sign-in">
				<li><a href="login.do">로그인</a></li>

			</ul>
		</div>
	</div>
</nav>


<%
} else {

if (ownerGrade != null) {
%>

<!-- 점주 로그인 후  -->


<nav
	class="navbar navbar-default navbar-fixed-top navbar-sticky-function">

	<div class="container">

		<div class="logo-wrapper">
			<div class="logo">
				<a href="main.do"><img src="images/store/flogo.png" alt="Logo" style="width: 135px; height: 50px; margin-right: 0px;"></a>
			</div>
		</div>

		<div id="navbar" class="navbar-nav-wrapper navbar-arrow">
			<!--상단바 시작-->
			<ul class="nav navbar-nav" id="responsive-menu">

				<li><a href="main.do">홈으로</a></li>

				<li><a href="listStoreForm.do">가게</a>
					<ul>
						<li><a href="listStoreForm.do">가게 목록</a></li>
						<li><a href="listTypeStoreForm.do">카테고리</a></li>
						<li><a href="searchMapStore.do">지도에서 가게 찾기</a></li>
					</ul></li>

				<li><a href="calendar.do">이벤트</a></li>
				<li><a href="listQuestion.do">문의사항</a></li>
				<li><a href="storeProfile.do">마이페이지</a>
					<ul>
						<li><a href="storeProfile.do">프로필</a></li>
						<li><a href="ownerMyPage.do">메뉴등록</a></li>
						<li><a href="char.do">판매량</a></li>
						<li><a href="listReserv.do">예약관리</a></li>
					</ul></li>


			</ul>
			



		</div>
		<!--/.nav-collapse -->


		<ul class="nav-mini sign-in">
			<li><a href="logout.do">로그아웃</a></li>
		</ul>
	</div>

</nav>




<%
} else if (grade != null) {

if (grade.equals("M")) {
	
	
%>
<!-- 관리자 로그인 후  -->

<nav
	class="navbar navbar-default navbar-fixed-top navbar-sticky-function">

	<div class="container">

		<div class="logo-wrapper">
			<div class="logo">
				<a href="main.do"><img src="images/store/flogo.png" alt="Logo" style="width: 135px; height: 50px; margin-right: 0px;"></a>
			</div>
		</div>

		<div id="navbar" class="navbar-nav-wrapper navbar-arrow">
			<!--상단바 시작-->
			<ul class="nav navbar-nav" id="responsive-menu">

				<li><a href="main.do">홈으로</a></li>

				<li><a href="listStoreForm.do">가게</a>
					<ul>
						<li><a href="listStoreForm.do">가게 목록</a></li>
						<li><a href="listTypeStoreForm.do">카테고리</a></li>
						<li><a href="searchMapStore.do">지도에서 가게 찾기</a></li>
					</ul></li>

				<li><a href="calendar.do">이벤트</a></li>
				<li><a href="listQuestion.do">문의사항</a></li>
				<li><a href="admin.do">관리자</a>
					<ul>
						<li><a href="admin.do">회원목록</a></li>
						<li><a href="adminStore.do">가게목록</a></li>
					</ul></li>


			</ul>
		</div>
		<!--/.nav-collapse -->
		<div class="nav-mini-wrapper">
			<ul class="nav-mini sign-in">
				<li><a href="logout.do">로그아웃</a></li>
			</ul>
		</div>
	</div>
</nav>




<%
} else {
%>



<!-- 일반회원 로그인 후  -->



<nav
	class="navbar navbar-default navbar-fixed-top navbar-sticky-function">

	<div class="container">

		<div class="logo-wrapper">
			<div class="logo">
				<a href="main.do"><img src="images/store/flogo.png" alt="Logo" style="width: 135px; height: 50px; margin-right: 0px;"></a>
			</div>
		</div>

		<div id="navbar" class="navbar-nav-wrapper navbar-arrow">
			<!--상단바 시작-->
			<ul class="nav navbar-nav" id="responsive-menu">

				<li><a href="main.do">홈으로</a></li>

				<li><a href="listStoreForm.do">가게</a>
					<ul>
						<li><a href="listStoreForm.do">가게 목록</a></li>
						<li><a href="listTypeStoreForm.do">카테고리</a></li>
						<li><a href="searchMapStore.do">지도에서 가게 찾기</a></li>
					</ul></li>

				<li><a href="calendar.do">이벤트</a></li>
				<li><a href="listQuestion.do">문의사항</a></li>
				
				<li><a href="memberProfile.do">마이페이지</a>
					<ul>
						<li><a href="memberProfile.do">프로필</a></li>
						<li><a href="sale.html">대기내역</a></li>
						<li><a href="sale.html">예약내역</a></li>
						<li><a href="favoriteStore.do">찜목록</a></li>
					</ul></li>


			</ul>
		</div>
		<!--/.nav-collapse -->
		<div class="nav-mini-wrapper">
			<ul class="nav-mini sign-in">
				<li><a href="logout.do">로그아웃</a></li>
			</ul>
		</div>
	</div>
</nav>



<%


				}

		}
}
%>









