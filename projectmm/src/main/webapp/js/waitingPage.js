


//수량 더하기
function plusbtnFnc() {
	let template = document.querySelector('.reservInfo')
	let number = template.querySelector('.peopleNumber').innerText

	number = parseInt(number) + 1;

	template.querySelector('.peopleNumber').innerText = number;
}

//수량 빼기
function minusbtnFnc() {
	let template = document.querySelector('.reservInfo')
	let number = template.querySelector('.peopleNumber').innerText

	if (number > 1) {
		number = parseInt(number) - 1;
	} else if (number == 1) {
		alert('한 명부터 대기가 가능합니다.')
	}

	template.querySelector('.peopleNumber').innerText = number;
}

//db등록
function insertWaitingFnc() {
	let url = location.search
	let id = url.split('=')
	let owner = id[1];
	let memId = document.getElementById('memId').value
	let ownerId = owner
	let template = document.querySelector('.reservInfo')
	let number = template.querySelector('.peopleNumber').innerText
	let res = null;

	if (memId == null || memId == "") {
		alert('로그인 후 이용이 가능합니다.')
	} else {
		fetch('searchWaiting.do?memId=' + memId + '&ownerId=' + ownerId)
			.then(resolve => resolve.json())
			.then(result => {
				if (result != null) {
					alert("이미 대기열에 등록 되어 있습니다.")
				} else {
					fetch('insertWaiting.do?memId=' + memId + '&ownerId=' + ownerId + '&personCount=' + number)
						.then(resolve => resolve.json())
						.then(result => {
							console.log(result)
							if (result.retCode == 'Success') {
								alert('대기 등록 완료')
								console.log(result.retCode)
								location.href = 'waitingSuccess.do?ownerId=' + ownerId
							} else if (result.retCode == 'Fail') {
								alert('대기 등록 실패')
								console.log(result.retCode)
							}
						})
						.catch(reject => { console.log(reject)})

				}
			})
			.catch(reject => { console.log(reject)})

	}
}

function searchWaitingFnc() {
	let url = location.search
	let id = url.split('=')
	let owner = id[1];
	let memId = document.getElementById('memId').value
	let ownerId = owner
	console.log(memId)
	console.log(ownerId)

	fetch('searchWaiting.do?memId=' + memId + '&ownerId=' + ownerId)
		.then(resolve => resolve.json())
		.then(result => {

			let res = result.MEM_ID
			console.log(res)
		})
		.catch(reject => { console.log(reject)})


}


document.getElementById('plusbtn').addEventListener('click', plusbtnFnc);
document.getElementById('minusbtn').addEventListener('click', minusbtnFnc);
document.getElementById('addWaitingbtn').addEventListener('click', insertWaitingFnc);