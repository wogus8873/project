/**
 * 
 */
fetch('menuList.do')
	.then(resolve => resolve.json())
	.then(result => {
		let url = location.search
		let id = url.split('=')
		let owner = id[1];
		document.getElementById('ownerId').value = owner;
		console.log(document.getElementById('ownerId'))
		result.forEach(function(res, idx) {
			if (owner == res.ownerId) {
				let template = document.querySelector('.order-menu-item.clearfix').cloneNode(true);
				//이름
				template.querySelector('h6').innerText = res.menuName;
				//가격
				template.querySelector('.price-new').innerText = res.menuPrice + '원';

				template.querySelector('.price-new').id = res.menuId;

				template.querySelector('.btn.btn-primary.animation').addEventListener('click', addCartFnc);

				document.querySelector('.order-menu-item.clearfix').parentElement.append(template);
			}
		})
		//해당테그에 클릭 이벤트를 건다
		//그러면 this가 걸린 addCartFnc가 걸린 .price-new를 가지고와서 실행한다 this를 하면 이벤트가 걸린 거 전부 가지고옴 
		document.querySelector('.order-menu-item.clearfix').style.display = 'none';

	})
	.catch(reject => console.log(reject))


fetch('menuList.do')
	.then(resolve => resolve.json())
	.then(result => {
		let url = location.href
		let id = url.indexOf('id=') + 3
		let owner = url.substring(id)

		result.forEach(function(res, idx) {
			if (owner == res.ownerId) {
				let template = document.querySelector('#menuImg').cloneNode(true);
				console.log(template)

				//이미지
				template.querySelector('img').setAttribute('src', 'images/store/' + res.menuImg)

				document.querySelector('#menuImg').parentElement.append(template);
			}
		})
		document.querySelector('#menuImg').style.display = 'none';
	})
	.catch(reject => console.log(reject))