
var menuArr = [];

//예약 등록
function addReservFnc() {

	let memId = document.getElementById('memId').value;
	let ownerId = document.getElementById('ownerId').value;
	let personCount = document.getElementById('personCount').value;
	let reservDate = document.getElementById('reservDate').value;
	let reservTime = document.getElementById('reservTime').value;
	let request = document.getElementById('special-request').value;

	console.log(menuArr.length)
	if (memId == "" || memId == null) {
		alert('로그인 후 예약이 가능합니다.')
	} else {

		//예약 정보 기입 확인
		if (personCount == "" || reservDate == "" || reservTime == "" ) {
			alert('예약 정보를 입력 하세요.')
		} else {

			//예약 불가 확인
			fetch('searchReserv.do?memId=' + memId + '&ownerId=' + ownerId)
				.then(resolve => resolve.json())
				.then(result => {
					console.log(result)
					if (result == null) {

						const reserv = 'memId=' + memId + '&ownerId=' + ownerId + '&personCount=' + personCount + '&reservDate=' + reservDate + '&reservTime=' + reservTime + '&request=' + request;

						fetch("submitReserv.do", {
							method: "post",
							headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
							body: reserv
						})
							.then(resolve => resolve.json())
							.then(result => {
								if (result.retCode == 'Success') {
									alert('예약 등록')
									reservNumFnc();
									console.log(result.retCode)
								} else if (result.retCode == 'Fail') {
									alert('에러 발생')
									console.log(result.retCode)
								}
							})
							.catch(reject => {
								console.log(reject)
							})
					} else {
						alert('가게에서 이미 신청하신 예약을 확인 중 입니다.')
					}


				})
		}
		//여기서 끝
	}

}




function reservNumFnc() {
	//예약번호 가지고 오기
	let memId = document.getElementById('memId').value;
	let ownerId = document.getElementById('ownerId').value;

	fetch('searchReserv.do?memId=' + memId + '&ownerId=' + ownerId)
		.then(resolve => resolve.json())
		.then(result => {
			let ordernum = result.ORDER_NUM


			let sum = document.querySelector('.list-unstyled.order-item-list')

			for (let i = 1; i < sum.children.length; i++) {
				var menu = new Array(4);
				let menuId = sum.children[i].querySelector('.cart-product-name.pull-left').id;
				let quantity = sum.children[i].querySelector('.resultNumber').innerText;
				let totalPrice = sum.children[i].querySelector('.mulresult').innerText;


				menu[0] = ordernum;
				menu[1] = menuId;
				menu[2] = quantity;
				menu[3] = totalPrice;

				menuArr.push(menu)
			}

			insertReservMenuFnc()
		})
		.catch(reject => console.log(reject))

}



function insertReservMenuFnc() {

	let sum = document.querySelector('.list-unstyled.order-item-list')
	//메뉴 디비 등록
	for (let i = 0; i < sum.children.length - 1; i++) {

		let orderNum = menuArr[i][0];
		let menuId = menuArr[i][1];
		let quantity = menuArr[i][2];
		let menuSum = menuArr[i][3];


		const menulist = 'orderNum=' + orderNum + '&menuId=' + menuId + '&quantity=' + quantity + '&menuSum=' + menuSum;

		fetch("insertReservMenu.do", {
			method: "post",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			body: menulist
		})
			.then(resolve => resolve.json())
			.then(result => {
				if (result.retCode == 'Success') {
					console.log(result.retCode)
					nextPageFnc();
				} else if (result.retCode == 'Fail') {
					console.log(result.retCode)
					alert('오류 발생')
				}
			})
			.catch(reject => {
				console.log(reject)
			})
	}

}


//수량 더하기
function plusbtnFnc() {
	let number = this.parentElement.children[1].innerText;
	let num = this.parentElement.parentElement.parentElement.children[2].children[0].value;
	//let result = this.parentElement.parentElement.parentElement.children[2].children[1].innerText;


	if (number >= 1) {
		number = parseInt(number) + 1;
	}

	this.parentElement.children[1].innerText = number

	//곱하기 하기
	this.parentElement.parentElement.parentElement.children[2].children[1].innerText = parseInt(num) * number
	sumFnc();
}

//수량 빼기
function minusbtnFnc() {
	let number = this.parentElement.children[1].innerText;
	let num = this.parentElement.parentElement.parentElement.children[2].children[0].value;


	if (number > 1) {
		number = parseInt(number) - 1;
	}else if (number == 1){
		alert("메뉴 수량 1개 이상부터 가능");
	}

	this.parentElement.children[1].innerText = number

	//곱하기 하기
	this.parentElement.parentElement.parentElement.children[2].children[1].innerText = parseInt(num) * number
	
	sumFnc();
}


//쓰레기버튼
function trashbtnFnc() {
	let code = this.parentElement.parentElement.parentElement;
	code.remove();
	sumFnc();

}


//장바구니 추가
function addCartFnc() {
	let menuId = this.children[0].id;
	let forId = document.querySelector('.list-unstyled.order-item-list');
	let ownerId = document.getElementById('ownerId').value;
	console.log(ownerId)
	let r = 0;
	if (forId.children.length > 1) {
		for (let i = 1; i < forId.children.length; i++) {
			id = forId.children[i].querySelector('.cart-product-name.pull-left').id
			if (menuId == id) {
				//최종 마지막꺼가 다르면 -1이 나오니까 두번 못잡아줌
				r = -1;
				break;
			} else {
				r = 1;
			}
			console.log(r);
		}

		if (r == -1) {
			alert("이미 장바구니에 있습니다.")
		} else if (r == 1) {
			fetch('searchMenu.do?menuId=' + menuId)
				.then(resolve => resolve.json())
				.then(result => {
					let template = document.querySelector('.searchMenu').cloneNode(true);
					//plus plus 버튼
					template.querySelector('.plusbtn').addEventListener('click', plusbtnFnc)
					template.querySelector('.resultNumber').innerText = "1";
					template.querySelector('.minusbtn').addEventListener('click', minusbtnFnc)

					//
					//음식 이름
					template.querySelector('.cart-product-name.pull-left').innerText = result.menuName;
					template.querySelector('.cart-product-name.pull-left').id = result.menuId;
					//음식 가격
					template.querySelector(".mulnumber").value = result.menuPrice;
					template.querySelector(".mulresult").innerText = result.menuPrice;

					//쓰레기 버튼
					template.querySelector('.trashbtn').addEventListener('click', trashbtnFnc)

					//붙이기
					document.querySelector('.searchMenu').parentElement.append(template);

					sumFnc();
					//document.querySelector('.searchMenu').style.display = 'none'
				})
				.catch(reject => console.log(reject))
		}
	} else if (forId.children.length == 1) {
		fetch('searchMenu.do?menuId=' + menuId)
			.then(resolve => resolve.json())
			.then(result => {

				let template = document.querySelector('.searchMenu').cloneNode(true);

				//plus plus 버튼
				template.querySelector('.plusbtn').addEventListener('click', plusbtnFnc)
				template.querySelector('.resultNumber').innerText = "1";
				template.querySelector('.minusbtn').addEventListener('click', minusbtnFnc)

				//
				//음식 이름
				template.querySelector('.cart-product-name.pull-left').innerText = result.menuName;
				template.querySelector('.cart-product-name.pull-left').id = result.menuId;
				//음식 가격
				template.querySelector(".mulnumber").value = result.menuPrice;
				template.querySelector(".mulresult").innerText = result.menuPrice;

				//쓰레기 버튼
				template.querySelector('.trashbtn').addEventListener('click', trashbtnFnc)

				//붙이기
				document.querySelector('.searchMenu').parentElement.append(template);

				//document.querySelector('.searchMenu').style.display = 'none'
				sumFnc();
			})
			.catch(reject => console.log(reject))
	}



}

sumFnc();

function nextPageFnc() {
	location.href="reservSuccess.do";
}

function sumFnc() {
	console.log("더하기 시작!")
	//합계 출력
	let sum = document.querySelector('.list-unstyled.order-item-list')
	let re = 0;
	console.log(sum.children)
	console.log(sum.children.length)
	for (let i = 1; i < sum.children.length; i++) {
		let sumplus = sum.children[i].querySelector('.mulresult').innerText
		console.log(sumplus)
		re = re + parseInt(sumplus);
	}

	document.querySelector('.text-spl-color.text-right').innerText = re + "원";

}


document.getElementById('submitbtn').addEventListener('click', addReservFnc);
