/**
 * 
 */

let columns = ['menuId','ownerId','menuName','menuPrice','saleCount'];
//member값을 던져주면 tr생성해주는 함수
function makeTr(menu={}){

	console.log(menu)
	let tr = document.createElement('tr');
			
	
		for(let column of columns){
			let td = document.createElement('td');
			td.innerText = menu[column];
			tr.append(td);
			console.log(td)
		}
		
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',menuDeleteAjax);
	btn.innerText='삭제';
	btn.setAttribute("class","btn btn-primary btn-inverse")
	td.append(btn);//<td><button>삭제</td ...
	tr.append(td);
	
	//수정버튼
	td = document.createElement('td');
	btn = document.createElement('button');
	btn.addEventListener('click',menuModifyForm);
	btn.innerText='수정';
	btn.setAttribute("class","btn btn-primary")
	td.append(btn);//<td><button>수정</td ...
	tr.append(td);
	
	
	return tr; //함수 호출한 영역으로 값을 반환함
}


function showMenuList(result){
	console.log(result);
	//table 생성
		let tbody = document.querySelector('#body')
		result.forEach(function(item,idx,ary){
			console.log(item['ownerId']);   //위에 makeTr의 매개값에 item 넣어서
			let owner = document.getElementById('ownerId').value;
			if(item['ownerId'] == owner){
			let tr = makeTr(item) 
			tbody.append(tr);
			}
		})
}


function addMenuFnc(e){
	let menuImg = document.getElementById('menuImg').value;
	let ownerId = document.getElementById('ownerId').value;
	let menuName = document.getElementById('menuName').value;
	let menuPrice = document.getElementById('menuPrice').value;
	/*
	const obj = {
		
		ownerId: ownerId,
		menuName: menuName,
		menuPrice: menuPrice,
		saleCount: saleCount,
	}
	*/
	const menu = 'menuImg='+menuImg+'&menuName='+menuName+'&menuPrice='+menuPrice+'&ownerId='+ownerId;
	
	let myForm = document.getElementById('menuInfo');
	let formData = new FormData(myForm);
	console.log(formData);
	//post 요청 . content-type 
	fetch("addMenu.do",{
		method: 'post',
		//headers: {'Content-Type':'multipart/form-data'},
		body: formData
	})
	.then(resolve=>resolve.json())
	.then(result=>{
		console.log(result)
		if(result.retCode == 'Success'){
			//makeTr(obj)
			alert('됨ㅋㅋ')
			console.log(result.retCode)
		}else if(result.retCode == 'Fail'){
			alert('처리중 에러.......')
			console.log(result.retCode)
		}
	})
	.catch(reject=>{
		console.log(reject)
	})
	
	e.stopPropagation();
}




fetch('menuList.do') 
.then(resolve=> resolve.json())
.then(showMenuList)
.catch(function(reject){
	console.log(reject);
})

document.getElementById('addBtn').addEventListener('click',addMenuFnc);
