/**
 js/answerAjax.js
 */

function addAnswerFnc() {
	let num = document.getElementById('num').value;
	console.log(num)
	let content = document.getElementById('content').value;

	const obj = {
		questionId: num,
		answerContent: content,
	}

	console.log(num)
	console.log(content)

	const answer = 'num=' + num + '&content=' + content;

	fetch("addAnswerAjax.do", {
		method: 'post',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
		body: answer
	})
		.then(resolve => resolve.json())
		.then(result => {
			if (result.retCode == 'Success') {
				alert("정상적으로 등록되었습니다.");
				window.location.reload();
			} else if (result.retCode == 'Fail') {
				alert("정상적으로 등록하지 못했습니다.");
			}
		})
		.catch(reject => {
			console.log(reject);
		});
}

function delAnswerFunc() {
	let num = document.getElementById('num').value;
	console.log(num)
	
	const answer = 'num=' + num;
	
		fetch("deleteAnswer.do", {
		method: 'post',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
		body: answer
	})
		.then(resolve => resolve.json())
		.then(result => {
			if (result.retCode == 'Success') {
				alert("정상적으로 삭제되었습니다.");
				window.location.reload();
			} else if (result.retCode == 'Fail') {
				alert("정상적으로 삭제하지 못했습니다.");
			}
		})
		.catch(reject => {
			console.log(reject);
		});
	
	
	
	
}

document.getElementById('delAnswerBtn').addEventListener('click', delAnswerFunc);
document.getElementById('addBtn').addEventListener('click', addAnswerFnc);
