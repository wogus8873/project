let memId = document.getElementById('memId').value
console.log(memId)

fetch('waitingList.do?memId=' + memId)
	.then(resolve => resolve.json())
	.then(result => {
		console.log(result)
		for (i = 0; i < result.length; i++) {
			let template = document.querySelector('#waitingtemplate').firstElementChild.cloneNode(true);
			
			template.querySelector('.ownerId').value = result[i].OWNER_ID;
			template.querySelector('.storeName').innerText = result[i].STORE_NAME;
			template.querySelector('.btn.btn-primary.animation.storeInfo').addEventListener('click', waitingInfo);
			template.querySelector('.btn.btn-primary.animation.reservCancle').addEventListener('click', waitingCancle);

			
			document.querySelector(".waitingInfo").append(template);
			
		}
	})

.catch(reject => console.log(reject))

function waitingInfo() {
	let ownerId = this.parentElement.children[1].value;
	location.href = 'waitingDetail.do?memId=' + memId + '&ownerId=' + ownerId;
}

function waitingCancle() {
	console.log(this.parentElement)
	let tem = this.parentElement
	let ownerId = tem.querySelector('.ownerId').value;
	let res = confirm("대기열을 삭제하시겠습니까?");
	if(res == true){
		tem.remove();
		fetch('removeWaiting.do?memId='+memId+'&ownerId='+ownerId)
			.then(resolve => resolve.json())
			.then(result => {
				console.log(result);
				alert("삭제 되었습니다.")
	
			})
	.catch(reject => console.log(reject))		
	}else{
		alert("삭제가 불가능 합니다.")
	}
	

}
