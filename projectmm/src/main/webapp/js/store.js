/**
 * 
 */
fetch('listStore.do')
.then(resolve=>resolve.json())
.then(result=>{
	let url = location.href
	let id = url.indexOf('id=')+3
	let ownerId = url.substring(id)

	result.forEach(function(res, idx){
		if(ownerId == res.ownerId) {
			//console.log(res.ownerId)
		let template = document.querySelector('.information-tab-pane').cloneNode(true);
		template.querySelector('img').setAttribute('src', 'images/store/' + res.storeImg);
		template.querySelector('h4').innerText = res.storeName;
		template.querySelector('h6').innerText = res.storeLocation;
		template.querySelector('h5').innerText = res.ownerPhone;
		template.querySelector('.desc').innerText = res.storeDesc;
		template.querySelector('.pull-right.text-right.mon').innerText = res.storeTime;
		template.querySelector('.pull-right.text-right.tue').innerText = res.storeTime;
		template.querySelector('.pull-right.text-right.wed').innerText = res.storeTime;
		template.querySelector('.pull-right.text-right.thu').innerText = res.storeTime;
		template.querySelector('.pull-right.text-right.fri').innerText = res.storeTime;
		template.querySelector('.pull-right.text-right.sat').innerText = res.storeTime;
		template.querySelector('.pull-right.text-right.sun').innerText = res.storeTime;
		document.querySelector('.information-tab-pane').parentElement.append(template);
		}
	})
		document.querySelector('.information-tab-pane').style.display = 'none';
	
})
.catch(reject => console.log(reject))


fetch('listStore.do')
.then(resolve=>resolve.json())
.then(result=>{
	let url = location.href
	let id = url.indexOf('id=')+3
	let ownerId = url.substring(id)

	result.forEach(function(res, idx){
		if(ownerId == res.ownerId) {
			
		let template = document.querySelector('.row.single-restaurant-top-section').cloneNode(true);
		
		template.querySelector('h2').innerText = res.storeName;
		template.querySelector('span').innerText = res.category;
		
		
		document.querySelector('.row.single-restaurant-top-section').parentElement.append(template);
		}
	})
		document.querySelector('.row.single-restaurant-top-section').style.display = 'none';
	
})
.catch(reject => console.log(reject))



