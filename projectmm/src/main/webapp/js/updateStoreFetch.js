/**
 * 
 */

function updateStoreFnc() {

	let ownerPw = document.getElementById("ownerPw").value
	let ownerPhone = document.getElementById("ownerPhone").value
	let storeName = document.getElementById("storeName").value
	let storeDesc = document.getElementById("storeDesc").value
	let category = document.getElementById("category").value
	let storeLocation = document.getElementById("storeLocation").value

	const store = 'ownerPw=' + ownerPw + '&ownerPhone=' + ownerPhone + '&storeName=' + storeName + '&storeDesc=' + storeDesc + '&category=' + category + '&storeLocation=' + storeLocation;

	let myForm = document.getElementById('storeInfo');
	let formData = new FormData(myForm);



	fetch('updateStore.do', {
		method: 'post',
		//headers: { 'Content-Type': 'multipart/form-data' },
		body: formData
	})
		.then(resolve => resolve.json())
		.then(result => {
			if (result.retCode == 'Success') {

				alert('수정 완료');
				location.href = "storeProfile.do"


			} else if (result.retCode == 'Fail') {
				alert('수정 실패');
			}
		})
		.catch(reject => {
		})

}

document.getElementById('modBtn').addEventListener('click', updateStoreFnc);

