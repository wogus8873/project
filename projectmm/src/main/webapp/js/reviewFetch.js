fetch('listReview.do')
	.then(resolve => resolve.json())
	.then(result => {
		let url = location.href;
		let id = url.indexOf('id=') + 3;
		let owner = url.substring(id);

		result.forEach(function(res, idx) {

			if (owner == res.ownerId) {

				let template = document.querySelector('.review-list').cloneNode(true);
				
				template.querySelector('.reviewid').innerText = res.reviewId;

				template.querySelector('.date span').innerText = new Date(res.createdAt).toDateString();

				template.querySelector('h6:nth-of-type(2)').innerText = res.memId;

				template.querySelector('.del').addEventListener('click', delReviewBtn);
				
				if (memid.value == res.memId) {
						template.querySelector('.del').disabled = false;
				} else {
					
					template.querySelector('.del').disabled = true;
				}

				template.querySelector('.review-list-content').innerText = res.review;
				
				// 평점
				let sItem = template.querySelector('ul.list-unstyled.list-inline.rating-star-list');

				for (let j = 0; j < 5; j++) {
					let star = document.createElement('li');
					let star1 = document.createElement('i');
					star.append(star1);

					if (j < Math.floor(res.rating)) {
						star1.className = 'fa fa-star';
					} else if (j < res.rating) {
						star1.className = 'fa fa-star-o';
					} else {
						star1.className = 'fa fa-star-o';
					}
					sItem.children[j].replaceWith(star);
				}

				document.querySelector('.review-list').parentElement.append(template);

			}
		})
		document.querySelector('.review-list').style.display = 'none';
	})

function addReviewBtn() {
	let url = location.href
	let id = url.indexOf('id=') + 3
	let ownerid = url.substring(id);
	let memid = document.getElementById('memid').value;
	let review = document.getElementById('review').value;
	let rating = document.getElementById('rating').value;

	const reviews = 'ownerid=' + ownerid + '&review=' + review + '&memid=' + memid + '&rating=' + rating;

	fetch('insertReview.do', {
		method: 'post',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
		body: reviews
	})
		.then(resolve => resolve.json())
		.then(result => {
			if (result.retCode == 'Success') {
				alert("리뷰가 등록되었습니다.");
				window.location.reload();
			} else if (result.retCode == 'Fail') {
				alert("리뷰가 등록되지 못했습니다.");
			}
		})
		.catch(reject => {
			console.log(reject);
		})
}

function delReviewBtn() {
	console.log(this)
	let code = this.parentElement;
	console.log(code)
	code.remove();

	let memid = document.getElementById('memid').value;
	console.log(memid)

	fetch('delReview.do?id=' + memid)
		.then(resolve => resolve.json())
		.then(result => {
			if (result.retCode == 'Success') {
				alert("리뷰가 삭제되었습니다.");
				window.location.reload();
			} else if (result.retCode == 'Fail') {
				alert("리뷰가 삭제되지 못했습니다.");
			}
		})
		.catch(reject => {
			console.log(reject);
		})
}

document.getElementById('addBtn').addEventListener('click', addReviewBtn);
//document.getElementById('delBtn').addEventListener('click', delReviewBtn);