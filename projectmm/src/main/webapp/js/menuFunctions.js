/**
 * 
 */
//삭제버튼 누르면 시작되는 이벤트 
	function menuDeleteAjax(){
		console.log(this.parentElement.parentElement.firstChild.innerText); 
		let menu = this.parentElement.parentElement.firstChild.innerText;  //tr 지우려고 자식의 부모의 부모 
		let btn = this;
		//db에서 삭제. 화면에서 제외
		fetch("deleteMenu.do?menuId="+menu,{
			method:'get'
		})
		.then(resolve=>resolve.json())
		.then(result=>{
			if(result.retCode == 'Success'){
				alert('정상적으로 처리 완료');
				btn.parentElement.parentElement.remove();//화면에서 바로제거
			}else if(result.retCode == 'Fail'){
				alert('처리중 오류 발생');
			}
		})
		.catch(reject=>{
			console.log(reject)
		})
			
		
	}
	
	function menuModifyForm(){
	let tr = this.parentElement.parentElement;
	let menuId = tr.children[0].textContent;
	let menuName = tr.children[2].textContent;
	let menuPrice = tr.children[3].textContent;
	let ownerId = tr.children[1].textContent;
	let saleCount = tr.children[4].textContent;
	console.log(menuId)
	console.log(menuName)
	console.log(menuPrice)
	const menu = {
			menuId: menuId,
			menuName: menuName,
			menuPrice: menuPrice,
			ownerId: ownerId,
			saleCount: saleCount
		};
	let newTr = document.createElement('tr');
	for(let column of columns){
		let td = document.createElement('td');
		if(column == 'menuName' || column == 'menuPrice'){
			let inp = document.createElement('input');
			inp.value = menu[column];
			td.append(inp);
			
		}else {
			td.innerText = menu[column];
			console.log(menu[column])
		}
		newTr.append(td);
	}
		
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',menuDeleteAjax);
	btn.innerText='삭제';
	btn.setAttribute("class","btn btn-primary btn-inverse")
	btn.disabled = true;
	td.append(btn);//<td><button>삭제</td ...
	newTr.append(td);
	
	//수정버튼
	td = document.createElement('td');
	btn = document.createElement('button');
	btn.addEventListener('click',menuUpdateAjax);
	btn.innerText='변경';
	btn.setAttribute("class","btn btn-primary")
	td.append(btn);//<td><button>수정</td ...
	newTr.append(td);
	
	//tr요소를 newTr요소로 대체	
	tr.replaceWith(newTr);
	
}
function menuUpdateAjax(){
	let tr = this.parentElement.parentElement;
	let menuId = tr.children[0].textContent;
	let menuName = tr.children[2].firstChild.value;
	let menuPrice = tr.children[3].firstChild.value;
	console.log(menuId)
	console.log(menuName)
	console.log(menuPrice)
	fetch("modMenu.do",{
		method: 'post',
		headers: {'Content-Type':'application/x-www-form-urlencoded'},		
		body: 'menuId='+menuId+'&menuName='+menuName+'&menuPrice='+menuPrice
	})
	.then(resolve=>resolve.json())
	.then(result=>{
		if(result.retCode == 'Success'){
			alert('정상적으로 처리 완료');
			console.log(result.menuInfo)
			let newTr = makeTr(result.menuInfo)
			
			tr.replaceWith(newTr);
		}else if(result.retCode == 'Fail'){
			alert('에러발생');
		}
	})
	.catch(reject=>{
		console.log(reject)
	})
		
		
	
}
	