package store.command;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class TopStoreList implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		StoreService service = new StoreServiceImplMybatis();
		List<StoreVO> list = service.topStoreList();
		
		
		ObjectMapper mapper = new ObjectMapper();

		try {
			String json = mapper.writeValueAsString(list);
			return json + ".ajax";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return null;
	}

}
