package store.command;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;



import common.Command;
import menu.service.MenuService;
import menu.service.impl.MenuServiceMybatis;
import menu.vo.MenuVo;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class UpdateStoreImg implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String savePath = req.getServletContext().getRealPath("/project/images"); //제일 상위 경로에서 /upload 경로 지정
		MultipartRequest multi = new MultipartRequest(req, savePath, 1024*1024*10, "utf-8", new DefaultFileRenamePolicy());
		
		//System.out.println(savePath);

		
		String ownerId = (String) ((HttpServletRequest) multi).getSession().getAttribute("ownerId");
		String storeImg = multi.getParameter("storeImg");
		
		Enumeration<?> files = multi.getFileNames();
		while(files.hasMoreElements()) {
			String file = (String) files.nextElement();
			storeImg = multi.getFilesystemName(file);
		}
		
		StoreVO vo = new StoreVO();
		
		vo.setStoreImg(storeImg);
		vo.setOwnerId(ownerId);
		
		StoreService service = new StoreServiceImplMybatis();
		int r = service.updateImg(vo);
		//System.out.println(r);
		String json = "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		
			
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		 
		 
		 
		return json+".ajax";
	}		
}


