package store.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.SearchVO;
import common.Command;

import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class ListStoreDo implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		

		String keyword = req.getParameter("keyword");
		SearchVO svo = new SearchVO();
		svo.setKeyword(keyword);
		
		req.setAttribute("searchvo", svo);
		
		
		
		
		StoreService service = new StoreServiceImplMybatis();
		List<StoreVO> list = service.listStore(svo);
		req.setAttribute("listStore", list);
		
		
		ObjectMapper mapper = new ObjectMapper();

		try {
			String json = mapper.writeValueAsString(list);
			return json + ".ajax";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return null;
		
	}

}
