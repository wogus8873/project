package store.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import reserv.vo.ReservVO;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;

public class OwnerReservAllowMod implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		String num = req.getParameter("orderNum");
		int orderNum = Integer.parseInt(num);
		StoreService service = new StoreServiceImplMybatis();
		
		int r = service.ownerReservAllowMod(orderNum);
		
		Map<String, Object> resultMap = new HashMap<>();
		Map<String, Object> vo = service.getReserv(orderNum);
		if(r>0) {
			vo = service.getReserv(orderNum);
			
			resultMap.put("retCode", "Success");
			resultMap.put("reservInfo", vo);
		}else {
			resultMap.put("retCode", "Fail");
		}
		
		String json = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(resultMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json+".ajax";
	}

}
