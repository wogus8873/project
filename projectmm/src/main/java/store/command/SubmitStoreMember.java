package store.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import common.ScriptUtils;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class SubmitStoreMember implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
		StoreVO vo = new StoreVO();
		
		String biznum = req.getParameter("biznum");
		String id = req.getParameter("ownerid");
		String pw = req.getParameter("ownerpw");
		String phone = req.getParameter("ownerphone");
		String name = req.getParameter("ownername");
		String storename = req.getParameter("storename");
		String category = req.getParameter("category");
		String location = req.getParameter("storelocation");
		String desc = req.getParameter("storedesc");
		String time = req.getParameter("storetime");
		
		vo.setBizNum(biznum);
		vo.setOwnerId(id);
		vo.setOwnerPw(pw);
		vo.setOwnerPhone(phone);
		vo.setOwnerName(name);
		vo.setStoreName(storename);
		vo.setCategory(category);
		vo.setStoreLocation(location);
		vo.setStoreDesc(desc);
		vo.setStoreTime(time);
		
		
		StoreService service = new StoreServiceImplMybatis();
		
		service.submitStoreMember(vo);
		ScriptUtils.alertAndMovePage(resp,"회원가입 되셨습니다. 로그인 해주세요.", "login.do");
		
		return"login.do";
		
	}

}
