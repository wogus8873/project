package store.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;

public class DeleteStore implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		String ownerId = req.getParameter("ownerId");
		StoreService service = new StoreServiceImplMybatis();
		int r = service.deleteStore(ownerId);
		String json = "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		
		return json+".ajax";
	}

}
