package store.command;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.tools.view.WebappUberspector.SetAttributeExecutor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import common.Command;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class SearchStore implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String ownerId = req.getParameter("ownerId");
		
		HttpSession session = req.getSession(false);
		
		StoreService service = new StoreServiceImplMybatis();
		StoreVO vo  = service.searchStore(ownerId);	
		String json = "";
		ObjectMapper mapper = new ObjectMapper();
		
		vo.setOwnerId(ownerId);
		
		
		session.setAttribute("ownerIId", vo.getOwnerId());
		
		
		try {
			json = mapper.writeValueAsString(vo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json + ".ajax";
	}

}
