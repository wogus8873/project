package store.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class ListTypeStoreForm implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		StoreService service = new StoreServiceImplMybatis();
		List<StoreVO> list = service.listStore(null);
		req.setAttribute("storeList", list);
		return "store/listTypeStore.tiles";
	}

}
