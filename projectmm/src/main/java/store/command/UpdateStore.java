package store.command;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import common.Command;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;


public class UpdateStore implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse resp) throws IOException {
		String savePath = request.getServletContext().getRealPath("/images/store"); //헬로우앱 제일 상위 경로에서 /upload 경로 지정
		MultipartRequest multi = new MultipartRequest(request, savePath, 1024*1024*10, "utf-8", new DefaultFileRenamePolicy() ); // 멀티파트ㅡ리퀘스트 생성
		
		
		

		
		//String storeImg = request.getParameter("storeImg");
		
		String ownerId = (String)  request.getSession().getAttribute("ownerId");
		System.out.println(ownerId);
		String ownerPw = multi.getParameter("ownerPw");
		String ownerPhone = multi.getParameter("ownerPhone");
		String storeName = multi.getParameter("storeName");
		String storeDesc = multi.getParameter("storeDesc");
		String category = multi.getParameter("category");
		String storeLocation = multi.getParameter("storeLocation");
		String storeImg = multi.getParameter("storeImg");
		
		// file객체 가져오기
		Enumeration<?> files = multi.getFileNames();   
		while(files.hasMoreElements()) {
			String file = (String) files.nextElement(); // 하나하나씩 불러옴
			System.out.println(file);
			storeImg = multi.getFilesystemName(file); // getFilesystemName 메소드에 파일이름 매개값으로 넣어서
			// 똑같은 이름 있으면 리네임 정책에의해 이름 바껴서 가져옴
		}
		
		
		
		
//		String ownerId = (String) request.getSession().getAttribute("ownerId");
//		String ownerPw = request.getParameter("ownerPw");
//		String ownerPhone = request.getParameter("ownerPhone");
//		String storeName = request.getParameter("storeName");
//		String storeDesc = request.getParameter("storeDesc");
//		String category = request.getParameter("category");
//		String storeLocation = request.getParameter("storeLocation");
		
		
		
		StoreVO vo = new StoreVO();
		vo.setOwnerId(ownerId);
		vo.setOwnerPw(ownerPw);
		vo.setOwnerPhone(ownerPhone);
		vo.setStoreName(storeName);
		vo.setStoreDesc(storeDesc);
		vo.setCategory(category);
		vo.setStoreLocation(storeLocation);
		vo.setStoreImg(storeImg);

		
		
		

		Map<String,Object> resultMap = new HashMap<>();
		StoreService service = new StoreServiceImplMybatis();
		int r = service.updateStore(vo);
		if(r > 0) {
			vo = service.searchStore(ownerId);
			resultMap.put("retCode", "Success");
			resultMap.put("storeInfo", vo);
			
			HttpSession session = request.getSession(true);
			session.setAttribute("ownerId",vo.getOwnerId());
			session.setAttribute("ownerPw",vo.getOwnerPw());
			session.setAttribute("ownerName", vo.getOwnerName());
			session.setAttribute("ownerPhone", vo.getOwnerPhone());
			session.setAttribute("storeName", vo.getStoreName());
			session.setAttribute("category", vo.getCategory());
			session.setAttribute("storeTime", vo.getStoreTime());
			session.setAttribute("storeLocation", vo.getStoreLocation());
			session.setAttribute("storeImg", vo.getStoreImg());
			session.setAttribute("ownerGrade", vo.getGrade());
			session.setAttribute("storeDesc", vo.getStoreDesc());
		} else {
			resultMap.put("retCode", "Fail");
		}
		
		// json 포맷 생성.
		String json ="";
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(resultMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
				
		return json + ".ajax";

	}

}
