package store.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class AdminStore implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		StoreService service = new StoreServiceImplMybatis();
		List<StoreVO> list = service.listStore(null);
		
		req.setAttribute("storeList", list);
		return "store/adminStore.tiles";
	}

}
