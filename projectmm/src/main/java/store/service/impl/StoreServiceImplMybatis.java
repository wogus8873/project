package store.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.DataSource;
import common.SearchVO;
import menu.vo.MenuVo;
import reserv.vo.ReservVO;
import store.mapper.StoreMapper;
import store.service.StoreService;
import store.vo.StoreVO;

public class StoreServiceImplMybatis implements StoreService {
	SqlSession session = DataSource.getInstance().openSession(true);  // true 오토커밋
	StoreMapper mapper = session.getMapper(StoreMapper.class);
	
	@Override
	public List<StoreVO> listStore(SearchVO svo) {
		return mapper.listStore(svo);
	}

	@Override
	public List<StoreVO> listTypeStore(String category) {
		return mapper.listTypeStore(category);
	}

	@Override
	public StoreVO searchStore(String ownerId) {
		return mapper.searchStore(ownerId);
	}

	@Override
	public List<StoreVO> newStore() {
		return mapper.newStore();
	}

	@Override
	public int updateStore(StoreVO vo) {
		return mapper.updateStore(vo);
	}



	@Override
	public int updateImg(StoreVO vo) {
		return mapper.updateImg(vo);
	}

	public int deleteStore(String ownerId) {
		// TODO Auto-generated method stub
		return mapper.deleteStore(ownerId);
	}
	public int submitStoreMember(StoreVO vo) {
		// TODO Auto-generated method stub
		return mapper.submitStoreMember(vo);
	}

	@Override
	public List<Map<String, Object>> adminStoreList() {
		
		return mapper.adminStoreList();
	}

	@Override
	public List<Map<String, Object>> ownerReservList() {
		// TODO Auto-generated method stub
		return mapper.ownerReservList();
	}

	@Override
	public int ownerReservAllowMod(int orderNum) {
		// TODO Auto-generated method stub
		return mapper.ownerReservAllowMod(orderNum);
	}

	@Override
	public Map<String, Object> getReserv(int orderNum) {
		// TODO Auto-generated method stub
		return mapper.getReserv(orderNum);
	}

	public List<StoreVO> topStoreList() {
		// TODO Auto-generated method stub
		return mapper.topStoreList();
	}

	

}
