package store.service;

import java.util.List;
import java.util.Map;

import common.SearchVO;
import reserv.vo.ReservVO;
import store.vo.StoreVO;


public interface StoreService {
	
	// 가게리스트
	public List<StoreVO> listStore(SearchVO svo);
	// 카테고리별 가게리스트
	public List<StoreVO> listTypeStore(String category);
	// 가게 상세조회
	public StoreVO searchStore (String ownerId);
	// 신상가게
	public List<StoreVO> newStore();	
	// 가게수정
	public int updateStore(StoreVO vo);
	// 가게삭제
	public int deleteStore(String ownerId);
	
	public int updateImg(StoreVO vo);

	public int submitStoreMember(StoreVO vo);//점주회원가입
	
	public List<StoreVO> topStoreList();
	
	//점주가게조회
	public List<Map<String,Object>> adminStoreList();
	//점주예약조회
	public List<Map<String,Object>> ownerReservList();
	public int ownerReservAllowMod(int orderNum);
	//점주예약승인
	public Map<String,Object> getReserv(int orderNum);
	
}
