package store.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class StoreDAO {
	Connection conn;
	ResultSet rs;
	PreparedStatement psmt;
	
	// 싱글톤 방식으로 인스턴스 생성
	private static StoreDAO instance = new StoreDAO();
	private StoreDAO() {}
	public static StoreDAO getInstance() {
		return instance;
	}
	//리소스 반환
	public void close() {
		try {
			//else if 쓰면 다음을 만족했을 때 다음으로 넘어가지 않기 때문에
			//각각 if를 써줌.
			if(rs != null) {
			   rs.close();
			}
			if(conn != null) {
			   conn.close();
			}		
			if(psmt != null) {
			   psmt.close();
			}		
 		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
}
