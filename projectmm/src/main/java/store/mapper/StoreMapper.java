package store.mapper;

import java.util.List;
import java.util.Map;

import common.Criteria;
import common.SearchVO;
import reserv.vo.ReservVO;
import store.vo.StoreVO;
import common.SearchVO;

public interface StoreMapper {
	public List<StoreVO> listStore(SearchVO svo);

	public List<StoreVO> listTypeStore(String category);

	public StoreVO searchStore(String ownerId);

	public List<StoreVO> newStore();

	public int updateStore(StoreVO vo);
	
	
	public StoreVO searchKey(SearchVO svo);

	public List<StoreVO> storeListPaging(Criteria cri);

	public int updateImg(StoreVO vo);
	
	public int deleteStore(String ownerId);

	public int submitStoreMember(StoreVO vo);//점주회원가입
	
	//점주가게조회
	public List<Map<String, Object>> adminStoreList();
	//점주예약조회
	public List<Map<String, Object>> ownerReservList();
	//예약승인
	public int ownerReservAllowMod(int orderNum);
	//예약승인후
	public Map<String,Object> getReserv(int orderNum);

	public List<StoreVO> topStoreList();


 }

	


