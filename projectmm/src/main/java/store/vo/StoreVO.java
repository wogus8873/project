package store.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StoreVO {
	
	private String bizNum;
	private String ownerId;	
	private String ownerPw;
	private String ownerName;
	private String ownerPhone;
	private String storeName;
	private String category;
	private String storeLocation;
	private String storeTime;
	private String storeImg;
	private String storeDesc;
	private String grade;
}
