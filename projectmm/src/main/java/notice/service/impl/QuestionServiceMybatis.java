package notice.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import common.Criteria;
import common.DataSource;
import common.SearchVO;
import notice.mapper.QuestionMapper;
import notice.service.QuestionService;
import notice.vo.QuestionVO;

public class QuestionServiceMybatis implements QuestionService {
	
	SqlSession session = DataSource.getInstance().openSession(true);
	QuestionMapper mapper = session.getMapper(QuestionMapper.class);

	@Override
	public List<QuestionVO> questionList() {
		return mapper.questionList();
	}
	
	
	@Override
	public List<QuestionVO> noticeListPaging(int pageNum, SearchVO svo) {
		// mybatis로 사용하기 위해서 parameter로 Criteria 활용
		Criteria cri = new Criteria(pageNum, 10);
		cri.setSearchCondition(svo.getSearchCondition());
		cri.setKeyword(svo.getKeyword());
		
		return mapper.noticeListPaging(cri); // notice list가 여러 건 넘어오겠지만 cri에 다 담겨져 있음
	}

	@Override
	public int noticeListPagingTotalCnt(SearchVO svo) {
		return mapper.noticeListPagingTotalCnt(svo);
	}
	
	

	@Override
	public int insertQuestion(QuestionVO vo) {
		return mapper.insertQuestion(vo);
	}

	@Override
	public QuestionVO serachQuestion(int questionId) {
		return mapper.searchQuestion(questionId);
	}

	@Override
	public int deleteQuestion(int questionId) {
		return mapper.deleteQuestion(questionId);
	}

	@Override
	public int addAnswer(QuestionVO vo) {
		return mapper.addAnswer(vo);
	}


	@Override
	public int deleteAnswer(QuestionVO vo) {
		// TODO Auto-generated method stub
		return mapper.deleteAnswer(vo);
	}


	@Override
	public int modQuestion(QuestionVO vo) {
		return mapper.modQuestion(vo);
		
	}






}
