package notice.service;

import java.util.List;

import common.SearchVO;
import notice.vo.QuestionVO;

public interface QuestionService {
	
	// 건의사항 전체 목록
	public List<QuestionVO> questionList();
	
	// 페이지에 따른 '페이징 리스트'를 가져오기 위함(아래랑 같이 움직임)
	public List<QuestionVO> noticeListPaging(int pageNum, SearchVO svo);

	// 페이지 리스트를 계산하기 위해 전체 건수를 확인하기 위함(위랑 같이 움직임)
	public int noticeListPagingTotalCnt(SearchVO svo);

	// 건의사항 등록
	public int insertQuestion(QuestionVO vo);
	
	// 건의사항 단건 조회
	public QuestionVO serachQuestion(int questionId);
	
	// 건의사항 삭제
	public int deleteQuestion(int questionId);

	// 댓글 등록
	public int addAnswer(QuestionVO vo);
	
	// 댓글 삭제
	public int deleteAnswer(QuestionVO vo);

	// 건의사항 수정
	public int modQuestion(QuestionVO vo);
	
}
