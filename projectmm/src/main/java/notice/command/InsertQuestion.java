package notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;
import notice.vo.QuestionVO;

public class InsertQuestion implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String writer = req.getParameter("writer");
		String title = req.getParameter("title");
		String subject = req.getParameter("subject");
		System.out.println(writer);
		
		req.setAttribute("writer", writer);
		req.setAttribute("title", title);
		req.setAttribute("subject", subject);
		
		QuestionVO vo = new QuestionVO();
		
		vo.setMemId(writer);
		vo.setQuestionTitle(title);
		vo.setQuestionContent(subject);
		
		QuestionService service = new QuestionServiceMybatis();
		
		service.insertQuestion(vo);
		
		return "listQuestion.do";
	}

}
