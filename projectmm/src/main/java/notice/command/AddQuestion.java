
package notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;

public class AddQuestion implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		return "notice/addQuestion.tiles";
	}

}



