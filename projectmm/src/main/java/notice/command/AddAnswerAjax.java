package notice.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;
import notice.vo.QuestionVO;

public class AddAnswerAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String num = req.getParameter("num");
		String content = req.getParameter("content");
		
		System.out.println(num);
		System.out.println(content);

		// update 구현
		QuestionVO vo = new QuestionVO();

		vo.setQuestionId(Integer.parseInt(num));
		vo.setAnswerContent(content);
		
		QuestionService service = new QuestionServiceMybatis();
		
		int r = service.addAnswer(vo);
		
		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
