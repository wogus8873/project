package notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;
import notice.vo.QuestionVO;

public class searchQuestion implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

		String num = req.getParameter("num");

		QuestionService service = new QuestionServiceMybatis();

		QuestionVO vo = service.serachQuestion(Integer.parseInt(num));

		req.setAttribute("vo", vo);

		return "notice/searchQuestion.tiles";
	}

}
