package notice.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;
import notice.vo.QuestionVO;

public class DeleteAnswer implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String nid = req.getParameter("num");

		System.out.println(nid);
		QuestionVO vo = new QuestionVO();

		vo.setQuestionId(Integer.parseInt(nid));

		QuestionService service = new QuestionServiceMybatis();
		int r = service.deleteAnswer(vo);

		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}
}
