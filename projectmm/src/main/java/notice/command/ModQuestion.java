package notice.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;
import notice.vo.QuestionVO;

public class ModQuestion implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String nid = req.getParameter("num");
		String title = req.getParameter("title");
		String subject = req.getParameter("subject");

		QuestionVO vo = new QuestionVO();

		vo.setQuestionId(Integer.parseInt(nid));
		vo.setQuestionTitle(title);
		vo.setQuestionContent(subject);

		QuestionService service = new QuestionServiceMybatis();

		service.modQuestion(vo);

		return "searchQuestion.do?=num";

	}

}
