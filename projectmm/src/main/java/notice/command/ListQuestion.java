package notice.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Criteria;
import common.PageDTO;
import common.SearchVO;
import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;
import notice.vo.QuestionVO;

public class ListQuestion implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

		String searchCondition = req.getParameter("searchCondition");
		String keyword = req.getParameter("keyword");
		
		SearchVO svo = new SearchVO();
		
		svo.setSearchCondition(searchCondition);
		svo.setKeyword(keyword);
		
		String pageNum = req.getParameter("pageNum");
		pageNum = pageNum == null ? "1" : pageNum;

		int pageNumInt = Integer.parseInt(pageNum);
		
		QuestionService service = new QuestionServiceMybatis();
		
		int total = service.noticeListPagingTotalCnt(svo);
		
		
		List<QuestionVO> list = service.noticeListPaging(pageNumInt, svo);

		//list = service.questionList();

		req.setAttribute("listQuestion", list);
		
		// 시작 페이지, 끝 페이지 기능
		// 현재 페이지, 페이지당 건수, 전체 데이터 건수를 계산하는 PageDTO를 NoticeList.java에서 읽어들이도록 해야 함
		Criteria cri = new Criteria(pageNumInt, 10); // 현재 페이지, 한 페이지당 보일 건수가 10건
		PageDTO dto = new PageDTO(cri, total); // 전체 건수
		req.setAttribute("pageDTO", dto);

		req.setAttribute("searchvo", svo);
		System.out.println(svo);
		
		
		

		return "notice/listQuestion.tiles";
	}

}
