package notice.command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import notice.service.QuestionService;
import notice.service.impl.QuestionServiceMybatis;

public class DeleteQuestion implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String nid = req.getParameter("num");
		
		QuestionService service = new QuestionServiceMybatis();
		service.deleteQuestion(Integer.parseInt(nid));
		
		return "listQuestion.do";
	}

}

