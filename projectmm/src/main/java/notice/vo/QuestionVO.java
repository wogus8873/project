package notice.vo;

import java.sql.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestionVO {
	/*
QUESTION_ID      NOT NULL NUMBER(6)      
MEM_ID           NOT NULL VARCHAR2(30)   
QUESTION_TITLE   NOT NULL VARCHAR2(30)   
QUESTION_CONTENT NOT NULL VARCHAR2(1000) 
QUESTION_DATE    NOT NULL DATE           
ANSWER_CONTENT           VARCHAR2(500)  
	*/
	
	private int questionId;
	private String memId;
	private String questionTitle;
	private String questionContent;
	private Date questionDate;
	private String answerContent;
}
