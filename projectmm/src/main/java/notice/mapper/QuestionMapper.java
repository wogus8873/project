package notice.mapper;

import java.util.List;

import common.Criteria;
import common.SearchVO;
import jjim.vo.JjimVO;
import notice.vo.QuestionVO;
import review.vo.ReviewVO;

public interface QuestionMapper {
	
	// 건의사항 전체 목록
	public List<QuestionVO> questionList();

	public int insertQuestion(QuestionVO vo);

	public QuestionVO searchQuestion(int questionId);

	public int deleteQuestion(int questionId);

	public int addAnswer(QuestionVO vo);

	// 페이징	
	public int noticeListPagingTotalCnt(SearchVO svo);
	public List<QuestionVO> noticeListPaging(Criteria cri);

	public int deleteAnswer(QuestionVO vo);

	public int modQuestion(QuestionVO vo);

	


}
