package jjim.command;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import jjim.service.JjimService;
import jjim.service.impl.JjimServiceMybatis;
import jjim.vo.JjimVO;

public class FavoriteStore implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String ownerid = req.getParameter(".storeId");
		String memid = req.getParameter("memId");
		
		req.setAttribute(".storeId", ownerid);
		req.setAttribute("memId", memid);
		
		JjimService service = new JjimServiceMybatis();
		List<Map<String, Object>> list = service.listJjim();
		System.out.println(list.size());
		
		req.setAttribute("listJjim", list);
		return "member/favoriteStore.tiles";
	}

}
