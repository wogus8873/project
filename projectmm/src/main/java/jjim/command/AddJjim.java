package jjim.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import jjim.service.JjimService;
import jjim.service.impl.JjimServiceMybatis;
import jjim.vo.JjimVO;

public class AddJjim implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String ownerid = req.getParameter(".storeId");
		String memid = req.getParameter("memId");
		
		System.out.println(ownerid);
		System.out.println(memid);
		
		req.setAttribute(".storeId", ownerid);
		req.setAttribute("memId", memid);
		
		JjimVO vo = new JjimVO();
		
		vo.setOwnerId(ownerid);
		vo.setMemId(memid);
		
		JjimService service = new JjimServiceMybatis();
		
		int r = service.addJjim(vo);
		
		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
