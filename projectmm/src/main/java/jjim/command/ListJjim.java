package jjim.command;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import jjim.service.JjimService;
import jjim.service.impl.JjimServiceMybatis;
import jjim.vo.JjimVO;

public class ListJjim implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String ownerid = req.getParameter(".storeId");
		String memid = req.getParameter("memId");
		
		req.setAttribute(".storeId", ownerid);
		req.setAttribute("memId", memid);
		
		JjimService service = new JjimServiceMybatis();
		List<Map<String, Object>> list = service.listJjim();
		
		req.setAttribute("listJjim", list);
		
		ObjectMapper mapper = new ObjectMapper();
		String json = "";

		try {
			json = mapper.writeValueAsString(list);
			return json + ".ajax";
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

}
