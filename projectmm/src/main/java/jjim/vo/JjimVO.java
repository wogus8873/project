package jjim.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JjimVO {

	/*
	MEM_ID   NOT NULL VARCHAR2(30) 
	OWNER_ID NOT NULL VARCHAR2(30) 
	 */
	
	private String memId;
	private String ownerId;

}
