package jjim.mapper;

import java.util.List;
import java.util.Map;

import jjim.vo.JjimVO;

public interface JjimMapper {

	public int addJjim(JjimVO vo);

	public List<Map<String, Object>> listJjim();

}
