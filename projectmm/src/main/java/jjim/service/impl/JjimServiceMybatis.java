package jjim.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.DataSource;
import jjim.service.JjimService;
import jjim.vo.JjimVO;
import jjim.mapper.JjimMapper;

public class JjimServiceMybatis implements JjimService{
	
	SqlSession session = DataSource.getInstance().openSession(true);
	JjimMapper mapper = session.getMapper(JjimMapper.class);

	@Override
	public int addJjim(JjimVO vo) {
		return mapper.addJjim(vo);
	}

	@Override
	public List<Map<String, Object>> listJjim() {
		return mapper.listJjim();
	}

}
