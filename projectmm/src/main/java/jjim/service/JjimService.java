package jjim.service;

import java.util.List;
import java.util.Map;

import jjim.vo.JjimVO;

public interface JjimService {

	public int addJjim(JjimVO vo);

	public List<Map<String, Object>> listJjim();

}
