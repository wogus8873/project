package menu.mapper;

import java.util.List;
import java.util.Map;

import common.Criteria;
import common.SearchVO;
import menu.vo.MenuVo;

public interface MenuMapper {
	//page에 따른 목록
		public List<MenuVo> menuListPaging(Criteria cri);
		//page 목록 계산을 위한 건수
		public int menuListPagingTotalCnt(SearchVO svo);
		//메뉴리스트
		public List<Map<String,Object>> menuList();
		//메뉴추가
		public int addMenu(MenuVo vo);
		//메뉴삭제
		public int deleteMenu(String menuId);
		//메뉴수정
		public int modMenu(MenuVo vo);
		//메뉴검색
		public Map<String, Object> searchMenu(int menuId);
		//판매량 차트
		public List<Map<String, Integer>> charData();
}
