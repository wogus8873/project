package menu.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.Criteria;
import common.DataSource;
import common.SearchVO;
import menu.mapper.MenuMapper;
import menu.service.MenuService;
import menu.vo.MenuVo;

public class MenuServiceMybatis implements MenuService{
	SqlSession session = DataSource.getInstance().openSession(true);
	MenuMapper mapper = session.getMapper(MenuMapper.class);
	
	
	@Override
	public List<MenuVo> menuListPaging(int pageNum, SearchVO svo) {
		Criteria cri = new Criteria(pageNum,10);
		cri.setSearchCondition(svo.getSearchCondition());
		cri.setKeyword(svo.getKeyword());
		return mapper.menuListPaging(cri);
	}

	@Override
	public int menuListPagingTotalCnt(SearchVO svo) {
		// TODO Auto-generated method stub
		return mapper.menuListPagingTotalCnt(svo);
	}

	@Override
	public List<Map<String, Object>> menuList() {
		// TODO Auto-generated method stub
		return mapper.menuList();
	}

	@Override
	public int addMenu(MenuVo vo) {
		// TODO Auto-generated method stub
		return mapper.addMenu(vo);
	}

	@Override
	public int deleteMenu(String menuId) {
		// TODO Auto-generated method stub
		return mapper.deleteMenu(menuId);
	}

	@Override
	public int modMenu(MenuVo vo) {
		// TODO Auto-generated method stub
		return mapper.modMenu(vo);
	}

	@Override
	public Map<String, Object> searchMenu(int menuId) {
		return mapper.searchMenu(menuId);
	}

	@Override
	public List<Map<String, Integer>> charData() {
		// TODO Auto-generated method stub
		return mapper.charData();
	}

	

	
}
