package menu.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MenuVo {
//	MENU_ID    NOT NULL NUMBER(6)     
//	OWNER_ID   NOT NULL VARCHAR2(30)  
//	MENU_NAME  NOT NULL VARCHAR2(100) 
//	MENU_PRICE NOT NULL NUMBER(10)    
//	SALE_COUNT          NUMBER(10)    
//	MENU_IMG   NOT NULL VARCHAR2(300) 
	public int menuId;
	public String ownerId;
	public String menuName;
	public int menuPrice;
	public int saleCount;
	public String menuImg;
	
	
}
