package menu.command;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import menu.service.MenuService;
import menu.service.impl.MenuServiceMybatis;

public class SearchMenu implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		int menuId = Integer.parseInt(req.getParameter("menuId"));
		System.out.println(menuId);
		
		
		
		MenuService service = new MenuServiceMybatis();
		Map<String, Object> sm = service.searchMenu(menuId);
		
		ObjectMapper mapper = new ObjectMapper();
			
		String json = "";
		
		try {
			json = mapper.writeValueAsString(sm);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
		return json + ".ajax";

	}

}
