package menu.command;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import menu.service.MenuService;
import menu.service.impl.MenuServiceMybatis;
import menu.vo.MenuVo;

public class ModMenu implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String id = req.getParameter("menuId");
		String menuName = req.getParameter("menuName");
		String price = req.getParameter("menuPrice");
		int menuId = Integer.parseInt(id);
		int menuPrice = Integer.parseInt(price);
		
		System.out.println(menuId);
		System.out.println(menuName);
		System.out.println(menuPrice);
		
		
		MenuVo vo = new MenuVo();
		vo.setMenuId(menuId);
		vo.setMenuName(menuName);
		vo.setMenuPrice(menuPrice);
		
		Map<String,Object> resultMap = new HashMap<>();
		MenuService service = new MenuServiceMybatis();
		int r = service.modMenu(vo);
		System.out.println(r);
		if(r>0) {
			Map<String,Object> menuvo = service.searchMenu(menuId);
			resultMap.put("retCode", "Success");
			resultMap.put("menuInfo", menuvo);
			System.out.println(menuvo);
		}else {
			resultMap.put("retCode", "Fail");
		}
		
		String json = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(resultMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json+".ajax";
	}

}
