package menu.command;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import common.Command;
import menu.service.MenuService;
import menu.service.impl.MenuServiceMybatis;
import menu.vo.MenuVo;

public class AddMenu implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String savePath = req.getServletContext().getRealPath("/project/images");
		System.out.println(savePath);
		MultipartRequest multi = new MultipartRequest(req,savePath,1024*1024*10,"utf-8",new DefaultFileRenamePolicy());
		
		String ownerId = multi.getParameter("ownerId");
		String menuName = multi.getParameter("menuName");
		String menuPrice = multi.getParameter("menuPrice");
		String menuImg = multi.getParameter("menuImg");
		int price = Integer.parseInt(menuPrice);
		
		Enumeration<?> files = multi.getFileNames();
		while(files.hasMoreElements()) {
			String file = (String) files.nextElement();
			System.out.println(file);
			menuImg = multi.getFilesystemName(file);
		}
		
		
		MenuVo vo = new MenuVo();
		
		vo.setOwnerId(ownerId);
		vo.setMenuName(menuName);
		vo.setMenuPrice(price);
		vo.setMenuImg(menuImg);
		
		MenuService service = new MenuServiceMybatis();
		int r = service.addMenu(vo);
		System.out.println(r);
		String json = "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		 
		 
		 
		return json+".ajax";
	}

}
