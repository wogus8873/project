package menu.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import menu.service.MenuService;
import menu.service.impl.MenuServiceMybatis;

public class DeleteMenu implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String menuId = req.getParameter("menuId");
		MenuService service = new MenuServiceMybatis();
		int r = service.deleteMenu(menuId);
		String json = "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		
		return json+".ajax";
	}

}
