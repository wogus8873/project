package menu.command;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import menu.service.MenuService;
import menu.service.impl.MenuServiceMybatis;

public class MenuList implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		MenuService service = new MenuServiceMybatis();
		List<Map<String,Object>> list = service.menuList();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		
		try {
			json = mapper.writeValueAsString(list);
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json+".ajax";
	}

}
