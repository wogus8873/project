package reserv.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class ReservVO {
	/*
	 * 	ORDER_NUM    NOT NULL NUMBER(6)    
		MEM_ID                VARCHAR2(30) 
		OWNER_ID              VARCHAR2(30) 
		PERSON_COUNT NOT NULL NUMBER(2)    
		RESERVE_DATE NOT NULL DATE         
		RESERVE_TIME NOT NULL DATE   
	 */
	
	private int orderNum;
	private String memId;
	private String ownerId;
	private int personCount;
	private String reservDate;
	private String reservTime;
	private String request;
	
	
}
