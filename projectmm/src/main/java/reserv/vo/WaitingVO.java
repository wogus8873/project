package reserv.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class WaitingVO {
	/*mem_id	varchar2(30)	NOT NULL,
	owner_id	varchar2(30)	NOT NULL,
	person_count	number(2)	NOT NULL,
	submit_date	date DEFAULT SYSDATE*/
	
	private String memId;
	private String ownerId;
	private int personCount;
	private String submitDate;
	
	
}
