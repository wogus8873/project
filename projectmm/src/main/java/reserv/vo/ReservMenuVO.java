package reserv.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class ReservMenuVO {
	/*	order_num	number(6),
	  	menu_id	number(6),
		quantity	number(6),
		menu_sum	number(10) */
	
	private int orderNum;
	private String menuId;
	private int quantity;
	private int menuSum;
	
}
