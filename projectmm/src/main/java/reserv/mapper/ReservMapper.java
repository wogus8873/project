package reserv.mapper;

import java.util.List;
import java.util.Map;

import reserv.vo.ReservMenuVO;
import reserv.vo.ReservVO;
import reserv.vo.WaitingVO;

public interface ReservMapper {
	//예약 추가
	public int submitReserv(ReservVO vo);
	//예약 검색
	public Map<String, Object> searchReserv(ReservVO vo);
	//예약메뉴 추가
	public int insertReservMenu(ReservMenuVO vo);
	//가게이름
	public Map<String, Object> storeName(String ownerId);
	//대기 추가
	public int insertWaiting(WaitingVO vo);
	//대기 검색
	public Map<String, Object> searchWaiting(WaitingVO vo);
	//대기 번호 검색
	public Map<String, Object> rownumWaiting(ReservVO vo);
	//예약 리스트
	public List<ReservVO> reservList(String memId);
	//개인 메뉴 조회
	public List<ReservVO> memMenuList(ReservVO vo);
	//대기 조회
	public List<Map<String, Object>> waitingList(String memId);
	//대기 삭제
	public int removeWaiting(WaitingVO vo);
	
}
