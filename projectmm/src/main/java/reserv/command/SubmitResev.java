package reserv.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;
import reserv.vo.ReservVO;

public class SubmitResev implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		HttpSession session = req.getSession(true);
		
		String memId = req.getParameter("memId");
		String ownerId = req.getParameter("ownerId");
		int personCount = Integer.parseInt(req.getParameter("personCount"));
		String reservDate = req.getParameter("reservDate");
		String reservTime = req.getParameter("reservTime");
		String request = req.getParameter("request");
		
		
		
		
		ReservVO vo = new ReservVO();

		vo.setMemId(memId);
		vo.setOwnerId(ownerId);
		vo.setPersonCount(personCount);
		vo.setReservDate(reservDate);
		vo.setReservTime(reservTime);
		vo.setRequest(request);
	
		
		ReservService service = new ReservServiceImplMybatis();
		int r = service.submitReserv(vo);
		System.out.println(r);
		
		session.setAttribute("ownerIid", vo.getOwnerId());
		
		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
		
	}

}		
		
		


