package reserv.command;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;

public class WaitingList implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String memId = req.getParameter("memId");
		
		ReservService service = new ReservServiceImplMybatis();
		List<Map<String, Object>> list = service.waitingList(memId);
		
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		
		try {
			json = mapper.writeValueAsString(list);
			System.out.println(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json+".ajax";
	}

}
