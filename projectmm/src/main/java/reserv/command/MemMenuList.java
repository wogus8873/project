package reserv.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;
import reserv.vo.ReservVO;

public class MemMenuList implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String memId = req.getParameter("memId");
		String ownerId = req.getParameter("ownerId");
		
		ReservVO vo = new ReservVO();
		vo.setMemId(memId);
		vo.setOwnerId(ownerId);
				
		ReservService service = new ReservServiceImplMybatis();
		List<ReservVO> list = service.memMenuList(vo);
		System.out.println(list);
		ObjectMapper mapper = new ObjectMapper();

		try {
			String json = mapper.writeValueAsString(list);
			return json + ".ajax";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return null;
	}

}
