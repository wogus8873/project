package reserv.command;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;
import reserv.vo.ReservVO;

public class SearchReserv implements Command{

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String memId = req.getParameter("memId");
		String onwerId = req.getParameter("ownerId");
		System.out.println(memId);
		
		ReservVO vo = new ReservVO();
		vo.setMemId(memId);
		vo.setOwnerId(onwerId);
		
		ReservService service = new ReservServiceImplMybatis();
		Map<String, Object> sr = service.searchReserv(vo);
		
		ObjectMapper mapper = new ObjectMapper();
		
		String json = "";
		
		try {
			json = mapper.writeValueAsString(sr);
			System.out.println(sr);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
		return json + ".ajax";
		
	}

}
