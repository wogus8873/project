package reserv.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;
import reserv.vo.WaitingVO;

public class RemoveWaiting implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String memId = req.getParameter("memId");
		String ownerId = req.getParameter("ownerId");
		
		WaitingVO vo = new WaitingVO();
		
		vo.setMemId(memId);
		vo.setOwnerId(ownerId);
		
		ReservService service = new ReservServiceImplMybatis();
		int r = service.removeWaiting(vo);
		
		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
