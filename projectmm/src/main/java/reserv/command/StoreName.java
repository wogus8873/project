package reserv.command;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;

public class StoreName implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String ownerId = req.getParameter("ownerId");
		
		System.out.println(ownerId);
		
		
		ReservService service = new ReservServiceImplMybatis();
		Map<String, Object> sr = service.storeName(ownerId);
		
		ObjectMapper mapper = new ObjectMapper();
		
		String json = "";
		
		try {
			json = mapper.writeValueAsString(sr);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
		return json + ".ajax";
	}

}
