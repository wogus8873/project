package reserv.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import common.Command;

public class ReservSuccess implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {		
		return "reserv/reservSuccess.tiles";
	}

}
