package reserv.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import reserv.service.ReservService;
import reserv.service.Impl.ReservServiceImplMybatis;
import reserv.vo.ReservMenuVO;

public class InsertReservMenu implements Command{

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		int orderNum = Integer.parseInt(req.getParameter("orderNum"));
		String menuId = req.getParameter("menuId");
		int quantity = Integer.parseInt(req.getParameter("quantity"));
		int menuSum = Integer.parseInt(req.getParameter("menuSum"));
		
		System.out.println(orderNum);
		System.out.println(menuId);
		System.out.println(quantity);
		System.out.println(menuSum);
		
		ReservMenuVO vo = new ReservMenuVO();
		
		vo.setOrderNum(orderNum);
		vo.setMenuId(menuId);
		vo.setQuantity(quantity);
		vo.setMenuSum(menuSum);
		
		ReservService service = new ReservServiceImplMybatis();
		int r = service.insertReservMenu(vo);
		System.out.println(r);
		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
		
		
	}

}
