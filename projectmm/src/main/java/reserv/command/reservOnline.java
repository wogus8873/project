package reserv.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;

public class reservOnline implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		return "reserv/reservOnline.tiles";
	}

}
