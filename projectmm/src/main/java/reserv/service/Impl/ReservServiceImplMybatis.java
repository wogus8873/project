package reserv.service.Impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.DataSource;
import reserv.mapper.ReservMapper;
import reserv.service.ReservService;
import reserv.vo.ReservMenuVO;
import reserv.vo.ReservVO;
import reserv.vo.WaitingVO;

public class ReservServiceImplMybatis implements ReservService {
	SqlSession session = DataSource.getInstance().openSession(true);
	ReservMapper mapper = session.getMapper(ReservMapper.class);
	
	@Override
	public int submitReserv(ReservVO vo) {
		return mapper.submitReserv(vo);
	}

	@Override
	public Map<String, Object> searchReserv(ReservVO vo) {
		return mapper.searchReserv(vo);
	}

	@Override
	public int insertReservMenu(ReservMenuVO vo) {
		return mapper.insertReservMenu(vo);
	}

	@Override
	public Map<String, Object> storeName(String ownerId) {
		return mapper.storeName(ownerId);
	}

	@Override
	public int insertWaiting(WaitingVO vo) {
		return mapper.insertWaiting(vo);
	}

	@Override
	public Map<String, Object> searchWaiting(WaitingVO vo) {
		return mapper.searchWaiting(vo);
	}


	@Override
	public List<ReservVO> reservList(String memId) {
		return mapper.reservList(memId);
	}

	@Override
	public List<ReservVO> memMenuList(ReservVO vo) {
		return mapper.memMenuList(vo);
	}

	@Override
	public Map<String, Object> rownumWaiting(ReservVO vo) {
		return mapper.rownumWaiting(vo);
	}

	@Override
	public List<Map<String, Object>> waitingList(String memId) {
		return mapper.waitingList(memId);
	}

	@Override
	public int removeWaiting(WaitingVO vo) {
		return mapper.removeWaiting(vo);
	}
	


	
}
