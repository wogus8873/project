package reserv.service;

import java.util.List;
import java.util.Map;

import reserv.vo.ReservMenuVO;
import reserv.vo.ReservVO;
import reserv.vo.WaitingVO;

public interface ReservService {

	public int submitReserv(ReservVO vo);

	public Map<String, Object> searchReserv(ReservVO vo);
	
	public int insertReservMenu(ReservMenuVO vo);
	
	public Map<String, Object> storeName(String ownerId);
	
	public int insertWaiting(WaitingVO vo);

	public Map<String, Object> searchWaiting(WaitingVO vo);
	
	public Map<String, Object> rownumWaiting(ReservVO vo);

	public List<ReservVO> reservList(String memId);

	public List<ReservVO> memMenuList(ReservVO vo);

	public List<Map<String, Object>> waitingList(String memId);

	public int removeWaiting(WaitingVO vo);

}
