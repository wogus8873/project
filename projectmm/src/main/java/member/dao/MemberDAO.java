package member.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import common.DAO;
import member.vo.MemberVO;
import store.vo.StoreVO;



public class MemberDAO {
	Connection conn;
	ResultSet rs;
	PreparedStatement psmt;

	// 싱글톤
	private static MemberDAO instance = new MemberDAO();

	public MemberDAO() {
	}

	public static MemberDAO getInstance() {
		return instance;
	}

	public void close() {
		try {
			if (rs != null) {

				rs.close();
			}
			if (conn != null) {
				conn.close();
			}
			if (psmt != null) {
				psmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	//로그인///
	public MemberVO loginCheck(String id, String pw) {
		/* 회원테이블			member
		회원 ID		mem_id		varchar2(30)	primary key
		패스워드		mem_pw		varchar2(30)	not null
		이름			mem_name	varchar2(30)	not null
		연락처		mem_phone	varchar2(30)	not null
		카드번호		credit_num	varchar2(30)	not null
		누적결제건수	pay_count	number(5)		default 0
		등급			grade		varchar2(30)	default 'B'
		멤버이미지		mem_img		varchar2()	
		  
		 * */
		
		
		String sql = "select * from member where mem_id=? and mem_pw=?";
		conn= DAO.getConn();
			
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1,id);
			psmt.setString(2, pw);
			rs = psmt.executeQuery();
			if(rs.next()) {
				MemberVO vo = new MemberVO();
				vo.setMemId(rs.getString("mem_id"));
				vo.setMemPw(rs.getString("mem_pw"));
				vo.setMemName(rs.getString("mem_name"));
				vo.setMemPhone(rs.getString("mem_phone"));
				vo.setCreditNum(rs.getString("credit_num"));
				vo.setPayCount(rs.getInt("pay_count"));
				vo.setGrade(rs.getString("grade"));
				vo.setMemImg(rs.getString("mem_img"));
				return vo;
				
			}
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			close();
		}
		
		return null;
	}	
	
	/*
	 * 
	BIZ_NUM        NOT NULL VARCHAR2(30)   
	OWNER_ID       NOT NULL VARCHAR2(30)   
	OWNER_PW       NOT NULL VARCHAR2(30)   
	OWNER_NAME     NOT NULL VARCHAR2(30)   
	OWNER_PHONE    NOT NULL VARCHAR2(30)   
	STORE_NAME     NOT NULL VARCHAR2(100)  
	CATEGORY       NOT NULL VARCHAR2(30)   
	STORE_LOCATION NOT NULL VARCHAR2(300)  
	STORE_TIME     NOT NULL VARCHAR2(100)  
	STORE_IMG      NOT NULL VARCHAR2(300)  
	GRADE                   VARCHAR2(1)    
	STORE_DESC              VARCHAR2(1000) 
	  */
	
	
	
	//점주 로그인
	public StoreVO StoreLoginCheck(String id, String pw) {
		conn= DAO.getConn();
		
		String sql = "select * from store where owner_id=? and owner_pw=?";
		
		try {
			
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, id);
			psmt.setString(2, pw);
			rs = psmt.executeQuery();
			if(rs.next()) {
				StoreVO vo= new StoreVO();
				vo.setBizNum(rs.getString("biz_num"));
				vo.setOwnerId(rs.getString("owner_id"));
				vo.setOwnerPw(rs.getString("owner_pw"));
				vo.setOwnerName(rs.getString("owner_name"));
				vo.setOwnerPhone(rs.getString("owner_phone"));
				vo.setStoreName(rs.getString("store_name"));
				vo.setCategory(rs.getString("category"));
				vo.setStoreLocation(rs.getString("store_location"));
				vo.setStoreTime(rs.getString("store_time"));
				vo.setStoreImg(rs.getString("store_img"));
				vo.setGrade(rs.getString("grade"));
				vo.setStoreDesc(rs.getString("store_desc"));
				
				return vo;
			}
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			close();
		}
		
		return null;
	}	
	
	
	
	
	//회원존재여부 멤버...
	public int UserCheck(String id) {
		conn= DAO.getConn();
		
		String sql = "SELECT COUNT(CASE WHEN mem_id=? THEN 1 END) \r\n"
				+ "FROM member";
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				
				return rs.getInt(1);
			}
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
		
		}
	/* 회원테이블			member
	회원 ID		mem_id		varchar2(30)	primary key
	패스워드		mem_pw		varchar2(30)	not null
	이름			mem_name	varchar2(30)	not null
	연락처		mem_phone	varchar2(30)	not null
	카드번호		credit_num	varchar2(30)	not null
	누적결제건수	pay_count	number(5)		default 0
	등급			grade		varchar2(30)	default 'B'
	멤버이미지		mem_img		varchar2()	
	  
	 * */
	
	//회원 존재 여부 점장(아이디중복체크)
	public int OwnerUserCheck(String userid) {
		conn= DAO.getConn();
		
		String sql = "SELECT COUNT(CASE WHEN owner_id=? THEN 1 END) \r\n"
				+ "FROM store";
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, userid);
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				
				return rs.getInt(1);
			}
			
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
		
		}
	
	
	
	public int updateMember(MemberVO vo) {
		String sql = "update member set mem_pw=?, mem_phone=?, mem_name=?, credit_num=?, mem_img=? where member_id=?";
		conn=DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getMemPw());
			psmt.setString(2, vo.getMemPhone());
			psmt.setString(3, vo.getMemName());
			psmt.setString(4, vo.getCreditNum());
			
			return psmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			close();
		}
		return 0; //////정상처리가 안되었을 경우.
		
	}
	
	//회원탈퇴
	public int withdrawal(String id) {
		System.out.println(id);
		String sql = "delete from member where mem_id = ?";
		conn = DAO.getConn();
		int r = 0;
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, id);
			r = psmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close();
		}
		return r;
	}
	
	
	public MemberVO findPwForm(String id) {
		System.out.println(id);
		String sql = "select * from member where mem_id =?";
		conn = DAO.getConn();
		
		
		
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				MemberVO vo = new MemberVO();
				
				vo.setMemPw(rs.getString("mem_pw"));
				
				
				return vo;
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close();
		}
		return null;
	}
		
	

	
	}

	
	
	




	


