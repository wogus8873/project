package member.service;

import java.util.List;
import java.util.Map;

import member.vo.MemberVO;
import store.vo.StoreVO;

public interface MemberService {

	//로그인 
	public MemberVO loginCheck(String id, String pw);
	
	//점주 로그인
	public StoreVO StoreLoginCheck(String id, String pw);
	
	//회원 존재 여부
	public int UserCheck(String id);
	
	//유저 회원가입
	public int SubmitMember(MemberVO vo);
	
	//유저 정보수정
	public int updateMember(MemberVO vo);
	
	//점장 유저체크(중복체크)
	public int OwnerUserCheck(String userid);

	
	public int updateMemMB(MemberVO vo);
	
	public MemberVO searchMem (String id);

	//관리자 회원목록
	public List<MemberVO> memberList();
	public List<Map<String,Object>> adminMemberList();

	//회원탈퇴
	public int withdrawal(String id);
	public int deleteMember(String memId);	
	public MemberVO findPwForm(String id);


	
}
