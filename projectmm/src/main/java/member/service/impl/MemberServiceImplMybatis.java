package member.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.DataSource;
import member.mapper.MemberMapper;
import member.service.MemberService;
import member.vo.MemberVO;
import store.vo.StoreVO;

public class MemberServiceImplMybatis implements MemberService {

	SqlSession session = DataSource.getInstance().openSession(true);
	MemberMapper mapper = session.getMapper(MemberMapper.class);
	
	@Override
	public MemberVO loginCheck(String id, String pw) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public StoreVO StoreLoginCheck(String id, String pw) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int UserCheck(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int SubmitMember(MemberVO vo) {
		// TODO Auto-generated method stub
		return mapper.SubmitMember(vo);
	}

	@Override
	public int updateMember(MemberVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int OwnerUserCheck(String userid) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateMemMB(MemberVO vo) {
		return mapper.updateMemMB(vo);
	}

	@Override
	public MemberVO searchMem(String id) {
		return mapper.searchMem(id);
	}

	public List<MemberVO> memberList() {
		// TODO Auto-generated method stub
		return mapper.memberList();
	}
	public int withdrawal(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Map<String, Object>> adminMemberList() {
		// TODO Auto-generated method stub
		return mapper.adminMemberList();
	}

	@Override
	public int deleteMember(String memId) {
		// TODO Auto-generated method stub
		return mapper.deleteMember(memId);
	}

	public MemberVO findPwForm(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
