package member.service.impl;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.DataSource;
import member.dao.MemberDAO;
import member.service.MemberService;
import member.vo.MemberVO;
import store.vo.StoreVO;

public class MemberServiceImpl implements MemberService {
	SqlSession session = DataSource.getInstance().openSession(true);
	MemberDAO dao = MemberDAO.getInstance();
	
	@Override
	public MemberVO loginCheck(String id, String pw) {
		return dao.loginCheck(id, pw);
		// TODO Auto-generated method stub
		
	}

	@Override
	public StoreVO StoreLoginCheck(String id, String pw) {
		// TODO Auto-generated method stub
		return dao.StoreLoginCheck(id, pw);
	}

	@Override
	public int UserCheck(String id) {
		// TODO Auto-generated method stub
		return dao.UserCheck(id);
	}

	@Override
	public int SubmitMember(MemberVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateMember(MemberVO vo) {
		// TODO Auto-generated method stub
		return dao.updateMember(vo);
	}

	@Override
	public int OwnerUserCheck(String userid) {
		// TODO Auto-generated method stub
		return dao.OwnerUserCheck(userid);
	}

	@Override
	public int updateMemMB(MemberVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public MemberVO searchMem(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<MemberVO> memberList() {
		// TODO Auto-generated method stub
		return null;
	}
	public int withdrawal(String id) {
		// TODO Auto-generated method stub
		return dao.withdrawal(id);
	}

	@Override

	public List<Map<String, Object>> adminMemberList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteMember(String memId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public MemberVO findPwForm(String id) {
		// TODO Auto-generated method stub
		return dao.findPwForm(id);
	}

	

	

}
