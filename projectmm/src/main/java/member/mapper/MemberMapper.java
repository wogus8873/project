package member.mapper;

import java.util.List;
import java.util.Map;

import member.vo.MemberVO;

public interface MemberMapper {
	
	public int SubmitMember(MemberVO vo);

	public List<MemberVO> memberList();
	public List<Map<String, Object>> adminMemberList();

	public int updateMemMB(MemberVO vo);
	
	public MemberVO searchMem(String id);
	
	public int deleteMember(String memId);

}
