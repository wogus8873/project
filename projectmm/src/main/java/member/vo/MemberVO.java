package member.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
 *
 회원테이블			member
회원 ID		mem_id		varchar2(30)	primary key
패스워드		mem_pw		varchar2(30)	not null
이름			mem_name	varchar2(30)	not null
연락처		mem_phone	varchar2(30)	not null
카드번호		credit_num	varchar2(30)	not null
누적결제건수	pay_count	number(5)		default 0
등급			grade		varchar2(30)	default 'B'
멤버이미지		mem_img		varchar2()	

 * */


@Setter
@Getter
@ToString
public class MemberVO {
	
	private String memId;
	private String memPw;
	private String memName;
	private String memPhone;
	private String creditNum;
	private int payCount;
	private String grade;
	private String memImg;
	

}
