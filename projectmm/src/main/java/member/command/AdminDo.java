package member.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import member.service.MemberService;
import member.service.impl.MemberServiceImplMybatis;
import member.vo.MemberVO;

public class AdminDo implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		MemberService service = new MemberServiceImplMybatis();
		
		List<MemberVO> list = service.memberList();
		req.setAttribute("memberList", list);
		return "member/admin.tiles";
	}

}
