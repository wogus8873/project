package member.command;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import member.dao.MemberDAO;
import member.service.MemberService;
import member.service.impl.MemberServiceImpl;

public class MemberIdCheckAction implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		MemberService service = new MemberServiceImpl();
		
		String id = req.getParameter("memId");
		
		int m = service.UserCheck(id);		
		int s = service.OwnerUserCheck(id);
		
		 
	    
		
	     String json = "";
		
		if(m==0 && s==0) {	
				json = "{\"retCode\":\"Success\"}";
	       }
		

		if(s!=0 || m!=0) {
			json = "{\"retCode\":\"Fail\"}";
	        
		}
			return json+".ajax";




  }
	
}	