package member.command;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import jjim.service.JjimService;
import jjim.service.impl.JjimServiceMybatis;

public class FavoriteStore implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		JjimService service = new JjimServiceMybatis();
		List<Map<String, Object>> list = service.listJjim();
		System.out.println(list.size());
		
		req.setAttribute("listJjim", list);
		
		return "member/favoriteStore.tiles";
	}

}
