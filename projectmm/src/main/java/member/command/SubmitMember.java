package member.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import common.ScriptUtils;
import member.service.MemberService;
import member.service.impl.MemberServiceImpl;
import member.service.impl.MemberServiceImplMybatis;
import member.vo.MemberVO;

public class SubmitMember implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		
				MemberVO vo = new MemberVO();
				
				String id= req.getParameter("userid");
				String pw= req.getParameter("userpw");
				String username = req.getParameter("username");
				String phonenum= req.getParameter("userphone");
				String creditnum= req.getParameter("creditnum");
				
				
				vo.setMemId(id);
				vo.setMemPw(pw);
				vo.setMemName(username);
				vo.setMemPhone(phonenum);
				vo.setCreditNum(creditnum);
				
				
				MemberService service = new MemberServiceImplMybatis();
				
				service.SubmitMember(vo);
				try {
					ScriptUtils.alertAndMovePage(resp,"회원가입 되셨습니다. 로그인 해주세요.", "login.do");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return "login.do";
				
	}

}
