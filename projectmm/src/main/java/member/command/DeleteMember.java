package member.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import member.service.MemberService;
import member.service.impl.MemberServiceImplMybatis;

public class DeleteMember implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		String memId = req.getParameter("memId");
		MemberService service = new MemberServiceImplMybatis();
		
		int r = service.deleteMember(memId);
		
		String json = "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		
		return json+".ajax";
	}

}
