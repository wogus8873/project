package member.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import common.Command;
import member.service.MemberService;
import member.service.impl.MemberServiceImpl;


public class Withdrawal implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = req.getSession(true);
		MemberService service = new MemberServiceImpl();
		
		
		String id = (String)session.getAttribute("id");
		
		System.out.println(id+ "test용 id");
		
		int r = service.withdrawal(id);
		
		if (r !=0 ) {
			System.out.println("회원삭제완료");
			
		}
		return "logout.do";
	}

}
