package member.command;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import common.Command;
import member.service.MemberService;
import member.service.impl.MemberServiceImplMybatis;
import member.vo.MemberVO;


public class UpdateMemberMB implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse resp) throws IOException {

	String savePath = request.getServletContext().getRealPath("/images/member"); //헬로우앱 제일 상위 경로에서 /upload 경로 지정
	MultipartRequest multi = new MultipartRequest(request, savePath, 1024*1024*10, "utf-8", new DefaultFileRenamePolicy() ); // 멀티파트ㅡ리퀘스트 생성	
		
		
		
	String memId = (String)request.getSession().getAttribute("id");
//	String memPw = request.getParameter("memPw");
//	String memPhone = request.getParameter("memPhone");
//	String creditNum = request.getParameter("creditNum");
	String memPw = multi.getParameter("memPw");
	String memPhone = multi.getParameter("memPhone");
	String creditNum = multi.getParameter("creditNum");
	String memImg = multi.getParameter("memImg");
	
	Enumeration<?> files = multi.getFileNames();   
	while(files.hasMoreElements()) {
		String file = (String) files.nextElement(); // 하나하나씩 불러옴
		System.out.println(file);
		memImg = multi.getFilesystemName(file); // getFilesystemName 메소드에 파일이름 매개값으로 넣어서
		// 똑같은 이름 있으면 리네임 정책에의해 이름 바껴서 가져옴
	}
	
	 
	MemberVO vo = new MemberVO();
	vo.setMemId(memId);
	vo.setMemPw(memPw);
	vo.setMemPhone(memPhone);
	vo.setCreditNum(creditNum);
	vo.setMemImg(memImg);

	System.out.println(vo);

	

	Map<String,Object> resultMap = new HashMap<>();
	MemberService service = new MemberServiceImplMybatis();
	int r = service.updateMemMB(vo);
	if(r > 0) {
		vo = service.searchMem(memId);
		resultMap.put("retCode", "Success");
		resultMap.put("memInfo", vo);
		HttpSession session = request.getSession(true);
		session.setAttribute("id", vo.getMemId());
		session.setAttribute("pw", vo.getMemPw());
		session.setAttribute("grade", vo.getGrade());
		session.setAttribute("name", vo.getMemName());
		session.setAttribute("img", vo.getMemImg());
		session.setAttribute("phone", vo.getMemPhone());
		session.setAttribute("credit", vo.getCreditNum());
		
	} else {
		resultMap.put("retCode", "Fail");
	}
	
	// json 포맷 생성.
	String json ="";
	ObjectMapper mapper = new ObjectMapper();
	try {
		json = mapper.writeValueAsString(resultMap);
	} catch (JsonProcessingException e) {
		e.printStackTrace();
	}
			
	return json + ".ajax";
	}

}
