package member.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import common.Command;
import common.ScriptUtils;
import member.service.MemberService;
import member.service.impl.MemberServiceImpl;
import member.vo.MemberVO;

import store.vo.StoreVO;

public class LoginCheck implements Command{

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		// TODO Auto-generated method stub
	
		HttpSession session = req.getSession(true);
		
		
		MemberService service = new MemberServiceImpl();
		String id = req.getParameter("id");
		String pw = req.getParameter("pw");
		
		
		int r = service.UserCheck(id);
		
		StoreVO svo = service.StoreLoginCheck(id, pw);
		
		MemberVO vo = service.loginCheck(id, pw);
		
		
		if(r==0) {
			
			
			if(svo != null) {
			
			session.setAttribute("ownerId",svo.getOwnerId());
			session.setAttribute("ownerPw", svo.getOwnerPw());
			session.setAttribute("ownerName", svo.getOwnerName());
			session.setAttribute("ownerPhone", svo.getOwnerPhone());
			session.setAttribute("storeName", svo.getStoreName());
			session.setAttribute("category", svo.getCategory());
			session.setAttribute("storeTime", svo.getStoreTime());
			session.setAttribute("storeLocation", svo.getStoreLocation());
			session.setAttribute("storeImg", svo.getStoreImg());
			session.setAttribute("ownerGrade", svo.getGrade());
			session.setAttribute("storeDesc", svo.getStoreDesc());
			
			
			
			System.out.println("점주로그인");
			System.out.println(r);
			System.out.println(svo.getStoreName());
			
			return "main.do";
			
			}else if(svo == null|| vo==null) {
				System.out.println("로그인 안됨");
				
				try {
					
					ScriptUtils.alertAndBackPage(resp,"아이디 , 비밀번호를 확인해주세요");
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return "member/login.tiles";
					
			}
				
		} else {
			
			if(vo!=null) {
				
			
			session.setAttribute("id", vo.getMemId());
			session.setAttribute("pw", vo.getMemPw());
			session.setAttribute("grade", vo.getGrade());
			session.setAttribute("name", vo.getMemName());
			session.setAttribute("img", vo.getMemImg());
			session.setAttribute("phone", vo.getMemPhone());
			session.setAttribute("credit", vo.getCreditNum());
			
			
			if(vo.getGrade().equals("M")) {
				System.out.println("관리자");
				
				
			}else if(vo.getGrade().equals("B")||vo.getGrade().equals("O")){
				System.out.println("일반회원");
				System.out.println(vo.getGrade());
				
			}
			
			}else if(vo==null) {
				
				
				try {
					ScriptUtils.alertAndBackPage(resp,"비밀번호를 확인해주세요");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			
			
		}
		return "main.do";
		
		

	}
	
	
}	
	
	
