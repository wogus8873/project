package member.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import common.Command;
import member.service.MemberService;
import member.service.impl.MemberServiceImpl;
import member.vo.MemberVO;

public class UpdateMemberAction implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
		
		MemberVO vo = new MemberVO();
		
		
		String id = (String)req.getSession().getAttribute("id");
		String grade = (String)req.getSession().getAttribute("grade");
		String credit = req.getParameter("credit");
		String pw = req.getParameter("pw");
		String name = req.getParameter("name");
		String phone = req.getParameter("phone");
		String img = req.getParameter("img");
		
		vo.setGrade(grade);
		vo.setCreditNum(credit);
		vo.setMemId(id);
		vo.setMemPw(pw);
		vo.setMemName(name);
		vo.setMemPhone(phone);
		vo.setMemImg(img);
		
		
		MemberService service = new MemberServiceImpl();

		service.updateMember(vo);
		
		
	    return null;

	}	

}
