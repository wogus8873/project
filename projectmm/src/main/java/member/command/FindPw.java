package member.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import common.ScriptUtils;
import member.service.MemberService;
import member.service.impl.MemberServiceImpl;
import member.vo.MemberVO;

public class FindPw implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		MemberService service = new MemberServiceImpl();
		
		String id = req.getParameter("id");	
		
		MemberVO vo = service.findPwForm(id);
		
		
		if(vo==null) {
			
			ScriptUtils.alertAndBackPage(resp,"존재하지 않는 아이디 입니다. 다시 입력해주세요.");
			
		}else {
			
			
			ScriptUtils.alertAndMovePage(resp,"회원님의 비밀번호는 \""+vo.getMemPw().replace(vo.getMemPw().substring(0,2),"**")+"\"입니다. 로그인 해주세요","login.do");
	
		
		}
		return "FindPwForm.do";
		
}

	
	
}	
