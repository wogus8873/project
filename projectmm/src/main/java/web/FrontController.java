package web;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import notice.command.DeleteQuestion;
import common.Command;
import common.MainDo;
import jjim.command.AddJjim;
import jjim.command.ListJjim;
import member.command.AdminDo;
import member.command.AdminMemberList;
import member.command.DeleteMember;
import member.command.FavoriteStore;
import member.command.FindPw;
import member.command.FindPwForm;
import member.command.IdCheckForm;
import member.command.Login;
import member.command.LoginCheck;
import menu.command.AddMenu;
import menu.command.CharData;
import menu.command.CharDo;
import menu.command.DeleteMenu;
import menu.command.MenuList;
import menu.command.SearchMenu;
import menu.command.ModMenu;

import member.command.Logout;
import member.command.MemberIdCheckAction;
import member.command.MemberProfile;
import member.command.OwnerIdCheckForm;
import member.command.SubmitMember;
import member.command.UpdateMember;
import member.command.UpdateMemberAction;
import member.command.UpdateMemberMB;
import member.command.Withdrawal;
import notice.command.AddAnswerAjax;
import notice.command.AddQuestion;
import notice.command.Calendar;
import notice.command.DeleteAnswer;
import notice.command.InsertQuestion;
import notice.command.ListQuestion;
import notice.command.ModQuestion;
import notice.command.searchQuestion;
import reserv.command.InsertReservMenu;
import reserv.command.InsertWaiting;
import reserv.command.MemMenuList;
import reserv.command.RemoveWaiting;
import reserv.command.ReservList;
import reserv.command.ReservProcess;
import reserv.command.ReservReport;
import reserv.command.ReservSuccess;
import reserv.command.RownumWaiting;
import reserv.command.SearchReserv;
import reserv.command.SearchWaiting;
import reserv.command.StoreName;
import reserv.command.StoreReservList;
import reserv.command.SubmitResev;
import reserv.command.WaitingDetail;
import reserv.command.WaitingList;
import reserv.command.WaitingPage;
import reserv.command.WaitingSuccess;
import reserv.command.reservOnline;
import reserv.command.waitingReport;
import reserv.command.ReservProcess;
import review.command.DelReview;
import review.command.InsertReview;
import review.command.ListReview;
import review.command.SearchMember;
import store.command.AdminStore;
import store.command.AdminStoreList;
import store.command.DeleteStore;
import store.command.ListReserv;
import store.command.ListStoreDo;
import store.command.ListStoreForm;
import store.command.ListTypeStoreForm;
import store.command.MapDo;
import store.command.NewStore;
import store.command.OwnerReservAllowMod;
import store.command.OwnerReservList;
import store.command.RandomListStore;
import store.command.SearchMapStoreDo;
import store.command.SearchStore;
import store.command.StoreProfile;
import store.command.SubmitStoreMember;
import store.command.TopStoreList;
import store.command.UpdateStore;
import store.command.UpdateStoreForm;
import store.command.UpdateStoreImg;
import owner.OwnerMyPage;

@WebServlet("*.do")
public class FrontController extends HttpServlet {
	HashMap<String , Command> map;
	
	public FrontController() {
		map = new HashMap<>();
	}
	
	@Override
	public void init() throws ServletException {
		//홈페이지
		map.put("/main.do", new MainDo());
		
		//가게
		map.put("/listStoreForm.do", new ListStoreForm());
		map.put("/listStore.do", new ListStoreDo());
		map.put("/searchMapStore.do", new SearchMapStoreDo());
		map.put("/listTypeStoreForm.do", new ListTypeStoreForm());
		map.put("/searchStore.do", new SearchStore());
		map.put("/newStore.do", new NewStore());
		map.put("/randomListStore.do",new RandomListStore());
		map.put("/updateStore.do", new UpdateStore());
		map.put("/updateStoreForm.do", new UpdateStoreForm());
		map.put("/updateStoreImg.do", new UpdateStoreImg());
		map.put("/storeProfile.do", new StoreProfile());
		map.put("/map.do", new MapDo());
		map.put("/deleteStore.do", new DeleteStore());
		map.put("/adminStoreList.do", new AdminStoreList());
		map.put("/topStoreList.do", new TopStoreList());
		
		
		
		//달력
		map.put("/calendar.do", new Calendar());

		//회원
		map.put("/login.do", new Login());//로그인, 회원가입 값 받는 페이지
		map.put("/loginCheck.do", new LoginCheck());//로그인 값 받기
		map.put("/logout.do", new Logout());//로그아웃
		map.put("/submitMember.do", new SubmitMember());// 회원가입
		map.put("/idCheckForm.do", new IdCheckForm());// 아이디 중복확인창
		map.put("/updateMember.do", new UpdateMember());//회원정보수정
		map.put("/updateMemberAction", new UpdateMemberAction());
		map.put("/memberIdCheckAction.do", new MemberIdCheckAction());
		map.put("/favoriteStore.do", new FavoriteStore());//찜 목록
		map.put("/updateMemberMB.do", new UpdateMemberMB());//회원정보수정마이바티스
		map.put("/searchMember.do", new SearchMember());
		map.put("/memberProfile.do", new MemberProfile());
		map.put("/withdrawal.do", new Withdrawal());//회원탈퇴
		map.put("/submitStoreMember.do", new SubmitStoreMember());

		map.put("/adminMemberList.do", new AdminMemberList());
		map.put("/deleteMember.do", new DeleteMember());

		map.put("/findPwForm.do", new FindPwForm());
		map.put("/findPw.do", new FindPw());
		map.put("/ownerIdCheckForm.do", new OwnerIdCheckForm());

		
		//점주
		map.put("/ownerMyPage.do", new OwnerMyPage());
		map.put("/reservList.do", new ReservList());
		map.put("/ownerReservList.do", new OwnerReservList());
		map.put("/ownerReservAllowMod.do", new OwnerReservAllowMod());
		map.put("/listReserv.do", new ListReserv());


		//메뉴
		map.put("/menuList.do", new MenuList());
		map.put("/addMenu.do", new AddMenu());
		map.put("/searchMenu.do", new SearchMenu());
		map.put("/deleteMenu.do", new DeleteMenu());
		map.put("/modMenu.do", new ModMenu());
		map.put("/char.do", new CharDo());
		map.put("/charData.do", new CharData());


		//건의사항
		map.put("/listQuestion.do", new ListQuestion());
		map.put("/insertQuestion.do", new InsertQuestion());
		map.put("/addQuestion.do", new AddQuestion());
		map.put("/searchQuestion.do", new searchQuestion());
		map.put("/deleteQuestion.do", new DeleteQuestion());
		map.put("/modQuestion.do", new ModQuestion());
		
		//건의사항 댓글
		map.put("/addAnswerAjax.do", new AddAnswerAjax());
		map.put("/deleteAnswer.do", new DeleteAnswer());
		
		
		//예약
		map.put("/reservOnline.do", new reservOnline());
		map.put("/reservProcess.do", new ReservProcess());
		map.put("/submitReserv.do", new SubmitResev());
		map.put("/insertReservMenu.do", new InsertReservMenu());
		map.put("/searchReserv.do", new SearchReserv());
		map.put("/reservSuccess.do", new ReservSuccess());
		map.put("/storeName.do", new StoreName());
		map.put("/reservReport.do", new ReservReport());
		map.put("/storeReservList.do", new StoreReservList());
		map.put("/memMenuList.do", new MemMenuList());
		
		
		//대기
		map.put("/waitingPage.do", new WaitingPage());
		map.put("/insertWaiting.do", new InsertWaiting());
		map.put("/searchWaiting.do", new SearchWaiting());
		map.put("/waitingSuccess.do", new WaitingSuccess());
		map.put("/rownumWaiting.do", new RownumWaiting());
		map.put("/waitingReport.do", new waitingReport());
		map.put("/waitingList.do", new WaitingList());
		map.put("/waitingDetail.do", new WaitingDetail());
		map.put("/removeWaiting.do", new RemoveWaiting());		
		//리뷰
		map.put("/insertReview.do", new InsertReview());
		map.put("/listReview.do", new ListReview());
		map.put("/delReview.do", new DelReview());

		
		//관리자
		map.put("/admin.do", new AdminDo());//마이페이지 첫화면
		map.put("/adminStore.do", new AdminStore());
		
		//찜
		map.put("/addjjim.do", new AddJjim());
		map.put("/listJjim.do", new ListJjim());
	}
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		String uri = req.getRequestURI();//HelloWeb/hello.do
		String context = req.getContextPath();//HelloWeb
		String page = uri.substring(context.length());
		System.out.println(">"+page);
		
		
		Command command = map.get(page);
		String viewPage = command.exec(req, resp); //main/main.tiles
		//.do 요청 들어오면 리다이렉트 경로 안 써도된다?
		//삭제하는경우 바로 갱신되게 하는게 리다이렉트
		if(viewPage.endsWith(".do")) {
			resp.sendRedirect(viewPage);
		//forward는 삭제하고 새로고침해야 갱신 차이점을 아시겠나요?
		}else if(viewPage.endsWith(".tiles")) {
			RequestDispatcher rd = req.getRequestDispatcher(viewPage); 
			rd.forward(req, resp);
		}else if(viewPage.endsWith(".ajax")) {//{"id:"hong","age":20}.ajax
			resp.setContentType("text/json;charset=utf-8");
			resp.getWriter().print(viewPage.substring(0,viewPage.length()-5)); //.ajax 는 필요없어서 빼는것 json형태의{"id:"hong","age":20} 만 출력
			
		}else{
			RequestDispatcher rd = req.getRequestDispatcher(viewPage); 
			rd.forward(req, resp);
		}
		
		
	  }
	}
