package common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class ScriptUtils {
		 
	    public static void init(HttpServletResponse resp) {
	        resp.setContentType("text/html; charset=euc-kr");
	        resp.setCharacterEncoding("euc-kr");
	    }
	 
	    public static void alert(HttpServletResponse resp, String alertText) throws IOException {
	        init(resp);
	        PrintWriter out = resp.getWriter();
	        out.println("<script>alert('" + alertText + "');</script> ");
	        out.flush();
	    }
	 
	    public static void alertAndMovePage(HttpServletResponse resp, String alertText, String nextPage)
	            throws IOException {
	        init(resp);
	        PrintWriter out = resp.getWriter();
	        out.println("<script>alert('" + alertText + "'); location.href='" + nextPage + "';</script> ");
	        out.flush();
	    }
	 
	    public static void alertAndBackPage(HttpServletResponse resp, String alertText) throws IOException {
	        init(resp);
	        PrintWriter out = resp.getWriter();
	        out.println("<script>alert('" + alertText + "'); history.go(-1);</script>");
	        out.flush();
	    }
	}


