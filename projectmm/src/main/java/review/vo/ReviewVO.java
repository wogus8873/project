package review.vo;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReviewVO {
	/*
	REVIEW_ID  NOT NULL NUMBER(6)      받아오기
	MEM_ID     NOT NULL VARCHAR2(30)   받아오기
	OWNER_ID   NOT NULL VARCHAR2(30)   받아오기
	REVIEW     NOT NULL VARCHAR2(1000) 보여주기
	RATING     NOT NULL NUMBER(5)      보여주기
	CREATED_AT          DATE           보여주기
	 */
	
	private int reviewId;
	private String memId;
	private String ownerId;
	private String review;
	private int rating;
	private Date createdAt;
}

