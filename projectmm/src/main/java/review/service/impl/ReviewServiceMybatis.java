package review.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import common.DataSource;
import review.mapper.ReviewMapper;
import review.service.ReviewService;
import review.vo.ReviewVO;

public class ReviewServiceMybatis implements ReviewService {
	
	SqlSession session = DataSource.getInstance().openSession(true);
	ReviewMapper mapper = session.getMapper(ReviewMapper.class);

	@Override
	public int insertReview(ReviewVO vo) {
		return mapper.insertReview(vo);
	}

	@Override
	public List<ReviewVO> listReview() {
		// TODO Auto-generated method stub
		return mapper.listReview();
	}

	@Override
	public int delReview(String memId) {
		// TODO Auto-generated method stub
		return mapper.delReview(memId);
	}



}
