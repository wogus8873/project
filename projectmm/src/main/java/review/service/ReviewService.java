package review.service;

import java.util.List;
import java.util.Map;

import review.vo.ReviewVO;

public interface ReviewService {
	
	// 리뷰 등록
	public int insertReview(ReviewVO vo);

	public List<ReviewVO> listReview();

	public int delReview(String memId);

	

}
