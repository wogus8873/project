package review.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Command;
import member.service.MemberService;
import member.service.impl.MemberServiceImplMybatis;
import member.vo.MemberVO;
import store.service.StoreService;
import store.service.impl.StoreServiceImplMybatis;
import store.vo.StoreVO;

public class SearchMember implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
String id = req.getParameter("id");
		

		MemberService service = new MemberServiceImplMybatis();
		MemberVO vo  = service.searchMem(id);
		String json = "";
		ObjectMapper mapper = new ObjectMapper();
		
		
		try {
			json = mapper.writeValueAsString(vo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json + ".ajax";

	}
}	
