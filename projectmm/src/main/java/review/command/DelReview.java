package review.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import review.service.ReviewService;
import review.service.impl.ReviewServiceMybatis;


public class DelReview implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String memId = req.getParameter("id");
		
		ReviewService service = new ReviewServiceMybatis();
		int r = service.delReview(memId);

		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
