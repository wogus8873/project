package review.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Command;
import review.service.ReviewService;
import review.service.impl.ReviewServiceMybatis;
import review.vo.ReviewVO;


public class InsertReview implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		/*
		REVIEW_ID  NOT NULL NUMBER(6)      받아오기
		MEM_ID     NOT NULL VARCHAR2(30)   받아오기
		OWNER_ID   NOT NULL VARCHAR2(30)   받아오기
		REVIEW     NOT NULL VARCHAR2(1000) 보여주기
		RATING     NOT NULL NUMBER(5)      보여주기
		CREATED_AT          DATE           보여주기
		 */
		
		String memid = req.getParameter("memid");
		String ownerid = req.getParameter("ownerid");
		String review = req.getParameter("review");
		String rating = req.getParameter("rating");

		req.setAttribute("memid", memid);
		req.setAttribute("ownerid", ownerid);
		req.setAttribute("review", review);
		req.setAttribute("rating", rating);
		
		ReviewVO vo = new ReviewVO();
		
		vo.setMemId(memid);
		vo.setOwnerId(ownerid);
		vo.setReview(review);
		vo.setRating(Integer.parseInt(rating));
		
		ReviewService service = new ReviewServiceMybatis();
		
		int r = service.insertReview(vo);
		
		String json = "";
		if (r > 0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
		
	}

}
