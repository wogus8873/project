package review.mapper;

import java.util.List;
import java.util.Map;

import review.vo.ReviewVO;

public interface ReviewMapper {

	public int insertReview(ReviewVO vo);

	public List<ReviewVO> listReview();

	public int delReview(String memId);
	
	
}
